import numpy as np
import scipy as sc
import math as mt
import random as ra

## CINEMATICA
def Calculo_Cinematica(x,y,phi,q1,q2,q3):
     
     #DISTANCIAS DE LOS ESLABONES
     l1= 0;
     l2= 0.275;
     l3= 0.425;
     l4= 0;
     a=0.175;
     altura=0.375;

     #CINEMATICA DIRECTA 
     hx=x+a*mt.cos(phi)+l2*mt.cos(q1+phi)*mt.cos(q2)+l3*mt.cos(q1+phi)*mt.cos(q2+q3);
     hy=y+a*mt.sin(phi)+l2*mt.sin(q1+phi)*mt.cos(q2)+l3*mt.sin(q1+phi)*mt.cos(q2+q3);
     hz=altura+l1+l2*mt.sin(q2)+l3*mt.sin(q2+q3);
  
     Cinematica = [hx,hy,hz]

     return (Cinematica)


## ROBOT FISICO
def AKASHA_DINAMICA(vref,v,q):

     v=np.array(v)
     q=np.array(q)
     vref=np.array(vref)
     ts=0.1
     
     # VELOCIDADES
     us = v[0]
     ws = v[1]
     q1ps = v[2]
     q2ps = v[3]
     q3ps = v[4]

     # ESTADOS DE LA PLATAFORMA

     ths = q[1]
     q1s = q[2]
     q2s = q[3]
     q3s = q[4]

     #PARAEMTROS DINAMICOS DEL AKASHA
     #error=(ra.random())*0.01
     error=1
     m=30*error;r=0.15;R=0.25;a=0.195;
     m2=3*error;m3=4*error;
     l2=0.275;l3=0.375;g=9.81;Ra=4.6;
     Kp=4;Kd=0.63;Ka=584;Kb=0.05;
     Kpt=4;Kpr=2;Kdt=0.63;Kdr=0.28;
     Kpa=584;Kpb=0.05;Rpa=4.6;
   
     C1=Kdt/Kpt;
     C2=Rpa*r*(m+m2+m3)/(2*Kpa*Kpt);
     C3=Rpa*m2*r/(2*Kpa*Kpt);
     C4=Rpa*m3*r/(2*Kpa*Kpt);
     C5=Rpa*m2*r/(2*Kpa*Kpr*R);
     C6=Rpa*m3*r/(2*Kpa*Kpr*R);
     C7=Kdr/Kpr;
     C8=Rpa*r*(m+m2+m3)/(2*Kpa*Kpr*R);
    
     C9=Ra*m2/(Ka*Kp);
     C10=Ra*m3/(Ka*Kp);
     C11=Kd/(Kp);
     C12=Kpb/(Kpt*r)+.95;
     C13=Kpb*R/(Kpr*r)+.95;
     C14=Kb/(Kp)+.95;
     C15=Ra*g*m2/(Ka*Kp);
     C16=Ra*g*m3/(Ka*Kp);

     # MATRIZ DE INERCIA
     M11 = C1+C2;
     M12 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M13 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M14 = -l2*C3*mt.sin(q2s)*mt.cos(q1s)-C4*(l2*mt.sin(q2s)*mt.cos(q1s)+l3*mt.sin(q2s+q3s)*mt.cos(q1s));
     M15 = -C4*l3*mt.sin(q2s+q3s)*mt.cos(q1s);
     M21 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M22 = (C7+C8*(1+2*a**2)+C5*l2*mt.cos(q2s)*(l2*mt.cos(q2s)+2*a*mt.cos(q1s)+a*mt.cos(ths+q1s)*mt.cos(ths)+a*mt.sin(ths+q1s)*mt.sin(ths))+
           C6*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)+2*a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+2*a*l2*mt.cos(q2s)*mt.cos(q1s)
           +a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+a*l2*mt.cos(q2s)*mt.cos(q1s)));
     M23 = (C5*(l2**2*mt.cos(q2s)**2+a*l2*mt.cos(q1s)*mt.cos(q2s))+
           C6*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+a*l2*mt.cos(q2s)*mt.cos(q1s)+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)));
     M24 = -C5*a*l2*mt.sin(q2s)*mt.sin(q1s)-C6*(a*l2*mt.sin(q2s)*mt.sin(q1s)+a*l3*mt.sin(q2s+q3s)*mt.sin(q1s));
     M25 = -C6*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s);
     M31 = -l2*C9*mt.cos(q2s)*mt.sin(q1s)-C10*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M32 = (C9*(l2**2*mt.cos(q2s)**2+2*a*l2*mt.cos(q1s)*mt.cos(q2s))+
           C10*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+2*a*l2*mt.cos(q2s)*mt.cos(q1s)+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)));
     M33 = C11+C9*l2**2*mt.cos(q2s)**2+C10*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s));
     M34 = 0; M35 = 0;
     M41 = -l2*C9*mt.sin(q2s)*mt.cos(q1s)-C10*(l2*mt.sin(q2s)*mt.cos(q1s)+l3*mt.sin(q2s+q3s)*mt.cos(q1s));
     M42 = -C9*2*a*l2*mt.sin(q2s)*mt.sin(q1s)-C10*(2*a*l2*mt.sin(q2s)*mt.sin(q1s)+2*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s));
     M43 = 0;
     M44 = C11+C9*l2**2+C10*(l2**2+l3**2+2*l2*l3*mt.cos(q3s));
     M45 = C10*l3*(l3+l2*mt.cos(q3s));
     M51 = -C10*l3*mt.sin(q2s+q3s)*mt.cos(q1s);
     M52 = -C10*2*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s);
     M53 = 0;
     M54=C10*l3*(l3+l2*mt.cos(q3s));
     M55 = C11+C10*l3**2;

     M =[[M11,M12,M13,M14,M15],
         [M21,M22,M23,M24,M25], 
         [M31,M32,M33,M34,M35],
         [M41,M42,M43,M44,M45], 
         [M51,M52,M53,M54,M55]]

     M=np.matrix(M)

     # MATRIZ DE FUERZAS CENTRIPETAS Y CORIOLIS
     Cs11 = C12;
     Cs12 = (-2*a*C2*ws+C3*(l2*mt.sin(q1s)*mt.sin(q2s)*q2ps-l2*mt.cos(q1s)*mt.cos(q2s)*ws-l2*mt.cos(q1s)*mt.cos(q2s)*q1ps)+C4*(l3*mt.sin(q1s)*mt.sin(q2s+q3s)*q3ps+
            (l3*mt.sin(q1s)*mt.sin(q2s+q3s)+l2*mt.sin(q1s)*mt.sin(q2s))*q2ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*q1ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws))
     Cs13 = (C3*(l2*mt.sin(q1s)*mt.sin(q2s)*q2ps+l2*mt.cos(q1s)*mt.cos(q2s)*q1ps-l2*mt.cos(q1s)*mt.cos(q2s)*ws)+C4*(l3*mt.sin(q1s)*mt.sin(q2s+q3s)*q3ps+
            (l3*mt.sin(q1s)*mt.sin(q2s+q3s)+l2*mt.sin(q1s)*mt.sin(q2s))*q2ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*q1ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws))
     Cs14 = (C3*(l2*mt.sin(q1s)*mt.sin(q2s)*ws+l2*mt.sin(q1s)*mt.sin(q2s)*q1ps-l2*mt.cos(q1s)*mt.cos(q2s)*q2ps)+C4*((l2*mt.sin(q1s)*mt.sin(q2s)+l3*mt.sin(q2s+q3s)*mt.sin(q1s))*ws+
            (l2*mt.sin(q1s)*mt.sin(q2s)+ l3*mt.sin(q2s+q3s)*mt.sin(q1s))*q1ps-(l3*mt.cos(q1s)*mt.cos(q2s+q3s)+l2*mt.cos(q1s)*mt.cos(q2s))*q2ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q3ps))
     Cs15 = (C4*(l3*mt.sin(q2s+q3s)*mt.sin(q1s)*ws+
            l3*mt.sin(q2s+q3s)*mt.sin(q1s)*q1ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q2ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q3ps))
     
     Cs21 = (C8*ws+(C5*(l2*mt.cos(q1s)*mt.cos(ths)*mt.cos(2*q1s+q2s)+2*l2*mt.sin(q1s+ths)*mt.cos(q2s)*mt.sin(ths))+
            C6*(l2*mt.cos(q2s)*mt.cos(ths)*mt.cos(q1s-q2s)+l3*mt.cos(q2s+q3s)*mt.cos(ths)*mt.cos(q1s-q2s)+l3*mt.cos(q2s+q3s)*mt.sin(ths+q1s)*mt.sin(ths)))*ws)
     Cs22 = (C12+C5*(a*l2*mt.cos(q2s)*mt.sin(q1s)*(ws+q1ps)+(l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*q2ps)+
            C6*((a*l2*mt.cos(q2s)*mt.sin(q1s)+a*l3*mt.cos(q2s+q3s)*mt.sin(q1s))*(ws+q1ps)+(a*l2*mt.cos(q1s)*mt.sin(q2s)
            +l2**2*mt.cos(q2s)*mt.sin(q2s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s))*(q2ps+q3ps)))
     Cs23 = (-C5*(a*l2*mt.cos(q2s)*mt.sin(q1s)*(ws+q1ps)+(l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*q2ps)-
            C6*((a*l2*mt.cos(q2s)*mt.sin(q1s)+a*l3*mt.cos(q2s+q3s)*mt.sin(q1s))*(ws+q1ps)+(a*l2*mt.cos(q1s)*mt.sin(q2s)
            +l2**2*mt.cos(q2s)*mt.sin(q2s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s))*(q2ps+q3ps)))
     Cs24 = (-C5*((l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s)*(ws+q1ps)+a*l2*mt.sin(q1s)*mt.cos(q2s))*(q2ps+q3ps))-
            C6*((l2**2*mt.cos(q2s)*mt.sin(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s)+
            l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*(ws+q1ps)+(a*l2*mt.sin(q1s)*mt.cos(q2s)+a*l3*mt.sin(q1s)*mt.cos(q2s+q3s))*(q2ps+q3ps)))
     Cs25 = (-C6*((l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s)+l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)))*(ws+q1ps)+
            (a*l3*mt.sin(q1s)*mt.cos(q2s+q3s))*(q2ps+q3ps))
    
     Cs31 = C9*l2*mt.cos(q2s)*mt.cos(q1s)*ws+C10*(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws;
     Cs32 = (C9*(-l2**2/2*mt.sin(2*q2s)*q2ps+(a*l2*mt.sin(q1s+q2s)+a*l2*mt.sin(q1s-q2s))*ws)+
            C10*((a*l2*mt.sin(q1s+q2s)+a*l2*mt.sin(q1s-q2s)-a*l3/4*mt.sin(q2s-q1s+q3s)+3*a*l3/4*mt.sin(q1s-q2s-q3s)+a*l3*mt.sin(q1s+q2s+q3s))*ws-
            (l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*q2ps-(l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3/2*mt.sin(q3s)+l2*l3/2*mt.sin(2*q2s+q3s))*q3ps))
     Cs33 = (C14-C9*l2**2/2*mt.sin(2*q2s)*q2ps-
            C10*((l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.sin(q2s+q3s)*mt.cos(q2s+q3s))*q3ps))
     Cs34 = C9*l2**2/2*mt.sin(2*q2s)*(ws+q1ps)+ C10*((l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s)))*(ws+q1ps)
     Cs35 = C10*((l3*mt.sin(q2s+q3s)*mt.cos(q2s+q3s)+l2*mt.sin(q2s+q3s)*mt.cos(q2s)))*(ws+q1ps)
    
     Cs41 = -C9*l2*mt.sin(q1s)*mt.sin(q2s)*ws-C10*(l2*mt.sin(q1s)*mt.sin(q2s)+l3*mt.sin(q1s)*mt.sin(q2s+q3s))*ws
     Cs42 = (C9*(l2**2/2*mt.sin(2*q2s)*(ws+q1ps)+(a*l2*mt.sin(q1s+q2s)-a*l2*mt.sin(q1s-q2s))*ws)+
            C10*((a*l2*mt.sin(q1s+q2s)-a*l2*mt.sin(q1s-q2s)+a*l3/4*mt.sin(q2s-q1s+q3s)-3*a*l3/4*mt.sin(q1s-q2s-q3s)+a*l3*mt.sin(q1s+q2s+q3s))*ws+
            (l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*(ws+q1ps)))
     Cs43 = (C9*(l2**2/2*mt.sin(2*q2s)*(ws+q1ps))+
            C10*(l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*(ws+q1ps))
     Cs44 = C14-C10*l2*l3*mt.sin(q3s)*q3ps;
     Cs45 = -C10*l2*l3*mt.sin(q3s)*(q2ps+q3ps);
    
     Cs51 = -C10*l3*mt.sin(q2s+q3s)*mt.sin(q1s)*ws;
     Cs52 = C10*l3*mt.sin(q2s+q3s)*((2*a*mt.cos(q1s)+l2*mt.cos(q2s)+l3*mt.cos(q2s+q3s))*ws+(a*mt.cos(q1s)+l3*mt.cos(q2s+q3s))*q1ps);
     Cs53 = C10*l3*mt.sin(q2s+q3s)*(l3*mt.cos(q2s+q3s)+l2*mt.cos(q2s))*(ws+q1ps);
     Cs54 = C10*l2*l3*mt.sin(q3s)*q2ps;
     Cs55 = C14;

     C = [[Cs11,Cs12,Cs13,Cs14,Cs15],
          [Cs21,Cs22,Cs23,Cs24,Cs25], 
          [Cs31,Cs32,Cs33,Cs34,Cs35],
          [Cs41,Cs42,Cs43,Cs44,Cs45], 
          [Cs51,Cs52,Cs53,Cs54,Cs55]]

     C=np.matrix(C)
     
     # VECTOR GRAVITACIONAL   
     G4 = C15*l2*mt.cos(q2s)+C16*(l3*mt.cos(q2s+q3s)+l2*mt.cos(q2s));
     G5 = C16*l3*mt.cos(q2s+q3s);
     G = np.array([0,0,0,G4,G5]).reshape(5,1)

     vref=vref.reshape(5,1)
     v=v.reshape(5,1)
     q=q.reshape(5,1)

     vp=(np.linalg.inv(M))*(vref-C*v-G)
     v=v+((np.array(vp)*ts).reshape(5,1))

     us = v[0,0]
     ws = v[1,0]
     q1ps = v[2,0]
     q2ps = v[3,0]
     q3ps = v[4,0]

     v_1 = [us,ws,q1ps,q2ps,q3ps]

     q = q+v*ts

     ths = q[1,0] 
     q1s = q[2,0]
     q2s = q[3,0]
     q3s = q[4,0]

     q_1 = [0,ths,q1s,q2s,q3s]

     Dinamica = [us,ws,q1ps,q2ps,q3ps,0,ths,q1s,q2s,q3s]

     return (Dinamica)


## COMPENSACION DINAMICA FUNCION

def COMPENSACION_DINAMICA_3GDL(vrefp,vref_e,v,q):

     vrefp=np.array(vrefp)
     vref_e=np.array(vref_e)
     v=np.array(v)
     q=np.array(q)
     
     us = v[0]
     ws = v[1]
     q1ps = v[2]
     q2ps = v[3]
     q3ps = v[4]
     
     # ESTADOS DE LA PLATAFORMA MOVIL Y BRAZO ROBOTICO
     ths = q[1];
     q1s = q[2];
     q2s = q[3];
     q3s = q[4];

     # PARAMETROS DINAMICOS DEL AKASHA
     m=30;r=0.15;R=0.25;a=0.195
     m2=3;m3=4
     l2=0.275;l3=0.375;g=9.81;Ra=4.6
     Kp=4;Kd=0.63;Ka=584;Kb=0.05
     Kpt=4;Kpr=2;Kdt=0.63;Kdr=0.28
     Kpa=584;Kpb=0.05;Rpa=4.6
   
     C1=Kdt/Kpt
     C2=Rpa*r*(m+m2+m3)/(2*Kpa*Kpt)
     C3=Rpa*m2*r/(2*Kpa*Kpt)
     C4=Rpa*m3*r/(2*Kpa*Kpt)
     C5=Rpa*m2*r/(2*Kpa*Kpr*R)
     C6=Rpa*m3*r/(2*Kpa*Kpr*R)
     C7=Kdr/Kpr
     C8=Rpa*r*(m+m2+m3)/(2*Kpa*Kpr*R)
    
     C9=Ra*m2/(Ka*Kp)
     C10=Ra*m3/(Ka*Kp)
     C11=Kd/(Kp)
     C12=Kpb/(Kpt*r)+.95
     C13=Kpb*R/(Kpr*r)+.95
     C14=Kb/(Kp)+.95
     C15=Ra*g*m2/(Ka*Kp)
     C16=Ra*g*m3/(Ka*Kp)
     
     # MATRIZ DE INERCIA
     M11 = C1+C2;
     M12 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M13 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M14 = -l2*C3*mt.sin(q2s)*mt.cos(q1s)-C4*(l2*mt.sin(q2s)*mt.cos(q1s)+l3*mt.sin(q2s+q3s)*mt.cos(q1s));
     M15 = -C4*l3*mt.sin(q2s+q3s)*mt.cos(q1s);
     M21 = -l2*C3*mt.cos(q2s)*mt.sin(q1s)-C4*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M22 = (C7+C8*(1+2*a**2)+C5*l2*mt.cos(q2s)*(l2*mt.cos(q2s)+2*a*mt.cos(q1s)+a*mt.cos(ths+q1s)*mt.cos(ths)+a*mt.sin(ths+q1s)*mt.sin(ths))+
           C6*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)+2*a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+2*a*l2*mt.cos(q2s)*mt.cos(q1s)
           +a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+a*l2*mt.cos(q2s)*mt.cos(q1s)));
     M23 = (C5*(l2**2*mt.cos(q2s)**2+a*l2*mt.cos(q1s)*mt.cos(q2s))+
           C6*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+a*l2*mt.cos(q2s)*mt.cos(q1s)+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)));
     M24 = -C5*a*l2*mt.sin(q2s)*mt.sin(q1s)-C6*(a*l2*mt.sin(q2s)*mt.sin(q1s)+a*l3*mt.sin(q2s+q3s)*mt.sin(q1s));
     M25 = -C6*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s);
     M31 = -l2*C9*mt.cos(q2s)*mt.sin(q1s)-C10*(l2*mt.cos(q2s)*mt.sin(q1s)+l3*mt.cos(q2s+q3s)*mt.sin(q1s));
     M32 = (C9*(l2**2*mt.cos(q2s)**2+2*a*l2*mt.cos(q1s)*mt.cos(q2s))+
           C10*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*a*l3*mt.cos(q2s+q3s)*mt.cos(q1s)+2*a*l2*mt.cos(q2s)*mt.cos(q1s)+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s)));
     M33 = C11+C9*l2**2*mt.cos(q2s)**2+C10*(l3**2*mt.cos(q2s+q3s)**2+l2**2*mt.cos(q2s)**2+2*l2*l3*mt.cos(q2s+q3s)*mt.cos(q2s));
     M34 = 0; M35 = 0;
     M41 = -l2*C9*mt.sin(q2s)*mt.cos(q1s)-C10*(l2*mt.sin(q2s)*mt.cos(q1s)+l3*mt.sin(q2s+q3s)*mt.cos(q1s));
     M42 = -C9*2*a*l2*mt.sin(q2s)*mt.sin(q1s)-C10*(2*a*l2*mt.sin(q2s)*mt.sin(q1s)+2*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s));
     M43 = 0;
     M44 = C11+C9*l2**2+C10*(l2**2+l3**2+2*l2*l3*mt.cos(q3s));
     M45 = C10*l3*(l3+l2*mt.cos(q3s));
     M51 = -C10*l3*mt.sin(q2s+q3s)*mt.cos(q1s);
     M52 = -C10*2*a*l3*mt.sin(q2s+q3s)*mt.sin(q1s);
     M53 = 0;
     M54=C10*l3*(l3+l2*mt.cos(q3s));
     M55 = C11+C10*l3**2;

     M =[[M11,M12,M13,M14,M15],
         [M21,M22,M23,M24,M25], 
         [M31,M32,M33,M34,M35],
         [M41,M42,M43,M44,M45], 
         [M51,M52,M53,M54,M55]]

     M=np.matrix(M)

     # MATRIZ GANANCIA  
     K = [[-1,0,0,0,0],
          [0,-1,0,0,0], 
          [0,0,-1,0,0],
          [0,0,0,-1,0], 
          [0,0,0,0,-1]]

     K=np.matrix(K)

     # MATRIZ DE FUERZAS CENTRIPETAS Y CORIOLIS
     Cs11 = C12;
     Cs12 = (-2*a*C2*ws+C3*(l2*mt.sin(q1s)*mt.sin(q2s)*q2ps-l2*mt.cos(q1s)*mt.cos(q2s)*ws-l2*mt.cos(q1s)*mt.cos(q2s)*q1ps)+C4*(l3*mt.sin(q1s)*mt.sin(q2s+q3s)*q3ps+
            (l3*mt.sin(q1s)*mt.sin(q2s+q3s)+l2*mt.sin(q1s)*mt.sin(q2s))*q2ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*q1ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws))
     Cs13 = (C3*(l2*mt.sin(q1s)*mt.sin(q2s)*q2ps+l2*mt.cos(q1s)*mt.cos(q2s)*q1ps-l2*mt.cos(q1s)*mt.cos(q2s)*ws)+C4*(l3*mt.sin(q1s)*mt.sin(q2s+q3s)*q3ps+
            (l3*mt.sin(q1s)*mt.sin(q2s+q3s)+l2*mt.sin(q1s)*mt.sin(q2s))*q2ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*q1ps-(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws))
     Cs14 = (C3*(l2*mt.sin(q1s)*mt.sin(q2s)*ws+l2*mt.sin(q1s)*mt.sin(q2s)*q1ps-l2*mt.cos(q1s)*mt.cos(q2s)*q2ps)+C4*((l2*mt.sin(q1s)*mt.sin(q2s)+l3*mt.sin(q2s+q3s)*mt.sin(q1s))*ws+
            (l2*mt.sin(q1s)*mt.sin(q2s)+ l3*mt.sin(q2s+q3s)*mt.sin(q1s))*q1ps-(l3*mt.cos(q1s)*mt.cos(q2s+q3s)+l2*mt.cos(q1s)*mt.cos(q2s))*q2ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q3ps))
     Cs15 = (C4*(l3*mt.sin(q2s+q3s)*mt.sin(q1s)*ws+
            l3*mt.sin(q2s+q3s)*mt.sin(q1s)*q1ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q2ps-l3*mt.cos(q1s)*mt.cos(q2s+q3s)*q3ps))
     
     Cs21 = (C8*ws+(C5*(l2*mt.cos(q1s)*mt.cos(ths)*mt.cos(2*q1s+q2s)+2*l2*mt.sin(q1s+ths)*mt.cos(q2s)*mt.sin(ths))+
            C6*(l2*mt.cos(q2s)*mt.cos(ths)*mt.cos(q1s-q2s)+l3*mt.cos(q2s+q3s)*mt.cos(ths)*mt.cos(q1s-q2s)+l3*mt.cos(q2s+q3s)*mt.sin(ths+q1s)*mt.sin(ths)))*ws)
     Cs22 = (C12+C5*(a*l2*mt.cos(q2s)*mt.sin(q1s)*(ws+q1ps)+(l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*q2ps)+
            C6*((a*l2*mt.cos(q2s)*mt.sin(q1s)+a*l3*mt.cos(q2s+q3s)*mt.sin(q1s))*(ws+q1ps)+(a*l2*mt.cos(q1s)*mt.sin(q2s)
            +l2**2*mt.cos(q2s)*mt.sin(q2s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s))*(q2ps+q3ps)))
     Cs23 = (-C5*(a*l2*mt.cos(q2s)*mt.sin(q1s)*(ws+q1ps)+(l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*q2ps)-
            C6*((a*l2*mt.cos(q2s)*mt.sin(q1s)+a*l3*mt.cos(q2s+q3s)*mt.sin(q1s))*(ws+q1ps)+(a*l2*mt.cos(q1s)*mt.sin(q2s)
            +l2**2*mt.cos(q2s)*mt.sin(q2s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s))*(q2ps+q3ps)))
     Cs24 = (-C5*((l2**2*mt.cos(q2s)*mt.sin(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s)*(ws+q1ps)+a*l2*mt.sin(q1s)*mt.cos(q2s))*(q2ps+q3ps))-
            C6*((l2**2*mt.cos(q2s)*mt.sin(q2s)+l3**2*mt.cos(q2s+q3s)*mt.sin(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s)+l2*l3*mt.cos(q2s+q3s)*mt.sin(q2s)+
            l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+a*l2*mt.cos(q1s)*mt.sin(q2s))*(ws+q1ps)+(a*l2*mt.sin(q1s)*mt.cos(q2s)+a*l3*mt.sin(q1s)*mt.cos(q2s+q3s))*(q2ps+q3ps)))
     Cs25 = (-C6*((l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s+q3s)+a*l3*mt.sin(q2s+q3s)*mt.cos(q1s)+l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)))*(ws+q1ps)+
            (a*l3*mt.sin(q1s)*mt.cos(q2s+q3s))*(q2ps+q3ps))
    
     Cs31 = C9*l2*mt.cos(q2s)*mt.cos(q1s)*ws+C10*(l2*mt.cos(q1s)*mt.cos(q2s)+l3*mt.cos(q2s+q3s)*mt.cos(q1s))*ws;
     Cs32 = (C9*(-l2**2/2*mt.sin(2*q2s)*q2ps+(a*l2*mt.sin(q1s+q2s)+a*l2*mt.sin(q1s-q2s))*ws)+
            C10*((a*l2*mt.sin(q1s+q2s)+a*l2*mt.sin(q1s-q2s)-a*l3/4*mt.sin(q2s-q1s+q3s)+3*a*l3/4*mt.sin(q1s-q2s-q3s)+a*l3*mt.sin(q1s+q2s+q3s))*ws-
            (l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*q2ps-(l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3/2*mt.sin(q3s)+l2*l3/2*mt.sin(2*q2s+q3s))*q3ps))
     Cs33 = (C14-C9*l2**2/2*mt.sin(2*q2s)*q2ps-
            C10*((l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*q2ps+(l2*l3*mt.sin(q2s+q3s)*mt.cos(q2s)+l3**2*mt.sin(q2s+q3s)*mt.cos(q2s+q3s))*q3ps))
     Cs34 = C9*l2**2/2*mt.sin(2*q2s)*(ws+q1ps)+ C10*((l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s)))*(ws+q1ps)
     Cs35 = C10*((l3*mt.sin(q2s+q3s)*mt.cos(q2s+q3s)+l2*mt.sin(q2s+q3s)*mt.cos(q2s)))*(ws+q1ps)
    
     Cs41 = -C9*l2*mt.sin(q1s)*mt.sin(q2s)*ws-C10*(l2*mt.sin(q1s)*mt.sin(q2s)+l3*mt.sin(q1s)*mt.sin(q2s+q3s))*ws
     Cs42 = (C9*(l2**2/2*mt.sin(2*q2s)*(ws+q1ps)+(a*l2*mt.sin(q1s+q2s)-a*l2*mt.sin(q1s-q2s))*ws)+
            C10*((a*l2*mt.sin(q1s+q2s)-a*l2*mt.sin(q1s-q2s)+a*l3/4*mt.sin(q2s-q1s+q3s)-3*a*l3/4*mt.sin(q1s-q2s-q3s)+a*l3*mt.sin(q1s+q2s+q3s))*ws+
            (l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*(ws+q1ps)))
     Cs43 = (C9*(l2**2/2*mt.sin(2*q2s)*(ws+q1ps))+
            C10*(l2**2/2*mt.sin(2*q2s)+l3**2/2*mt.sin(2*q2s+2*q3s)+l2*l3*mt.sin(2*q2s+q3s))*(ws+q1ps))
     Cs44 = C14-C10*l2*l3*mt.sin(q3s)*q3ps;
     Cs45 = -C10*l2*l3*mt.sin(q3s)*(q2ps+q3ps);
    
     Cs51 = -C10*l3*mt.sin(q2s+q3s)*mt.sin(q1s)*ws;
     Cs52 = C10*l3*mt.sin(q2s+q3s)*((2*a*mt.cos(q1s)+l2*mt.cos(q2s)+l3*mt.cos(q2s+q3s))*ws+(a*mt.cos(q1s)+l3*mt.cos(q2s+q3s))*q1ps);
     Cs53 = C10*l3*mt.sin(q2s+q3s)*(l3*mt.cos(q2s+q3s)+l2*mt.cos(q2s))*(ws+q1ps);
     Cs54 = C10*l2*l3*mt.sin(q3s)*q2ps;
     Cs55 = C14;

     C = [[Cs11,Cs12,Cs13,Cs14,Cs15],
          [Cs21,Cs22,Cs23,Cs24,Cs25], 
          [Cs31,Cs32,Cs33,Cs34,Cs35],
          [Cs41,Cs42,Cs43,Cs44,Cs45], 
          [Cs51,Cs52,Cs53,Cs54,Cs55]]

     C=np.matrix(C)
     
     # VECTOR GRAVITACIONAL   
     G4 = C15*l2*mt.cos(q2s)+C16*(l3*mt.cos(q2s+q3s)+l2*mt.cos(q2s));
     G5 = C16*l3*mt.cos(q2s+q3s);
     G = np.array([0,0,0,G4,G5]).reshape(5,1)

     vrefp=vrefp.reshape(5,1)
     vref_e=vref_e.reshape(5,1)
     v=v.reshape(5,1)

     vref= M*(vrefp+K*vref_e)+C*v+G;

     us = vref[0,0];
     ws = vref[1,0];
     q1ps = vref[2,0];
     q2ps = vref[3,0];
     q3ps = vref[4,0];

     v_1 = [us,ws,q1ps,q2ps,q3ps]
     
     return (v_1)


## MODELO PRINCIPAL

def MODELO(hdp,hd,h,phi,q,vuw,qp,hxy):
    ts=0.1
    
    #CONVERSION A VECTORES
    hdp =np.array(hdp)
    hd  =np.array(hd)
    h   =np.array(h)
    q   =np.array(q)
    vuw =np.array(vuw)
    qp  =np.array(qp)
    hxy =np.array(hxy)

    x=hxy[0]
    y=hxy[1]

    
    #MEDIDAS DEL ROBOT
    l1= 0;
    l2= 0.275;
    l3= 0.425;
    l4= 0;
    a=0.175;
    altura=0.375;

    #CALCULO DEL ERROR
    he=h-hd;
    he=he.reshape(3,1)

    #VALORES DE LOS ESLABONES
    q1=q[0]
    q2=q[1]
    q3=q[2]

    #JACOBIANA
    j11=mt.cos(phi)
    j12=-a*mt.sin(phi)-l2*mt.cos(q2)*mt.sin(q1+phi)-l3*mt.cos(q2+q3)*mt.sin(q1+phi)
    j13=-l2*mt.cos(q2)*mt.sin(q1+phi)-l3*mt.cos(q2+q3)*mt.sin(q1+phi)
    j14=-l2*mt.cos(q1+phi)*mt.sin(q2)-l3*mt.cos(q1+phi)*mt.sin(q2+q3)
    j15=-l3*mt.cos(q1+phi)*mt.sin(q2+q3)
   
    j21=mt.sin(phi);
    j22=a*mt.cos(phi)+l2*mt.cos(q2)*mt.cos(q1+phi)+l3*mt.cos(q2+q3)*mt.cos(q1+phi)
    j23=l2*mt.cos(q2)*mt.cos(q1+phi)+l3*mt.cos(q2+q3)*mt.cos(q1+phi)
    j24=-l2*mt.sin(q1+phi)*mt.sin(q2)-l3*mt.sin(q1+phi)*mt.sin(q2+q3)
    j25=-l3*mt.sin(q1+phi)*mt.sin(q2+q3)
    
    j31=0
    j32=0
    j33=0
    j34=l2*mt.cos(q2)+l3*mt.cos(q2+q3)
    j35=l3*mt.cos(q2+q3)
    
    J=[[j11,j12,j13,j14,j15],
       [j21,j22,j23,j24,j25],
       [j31,j32,j33,j34,j35]]

    J=np.matrix(J)

    #MATRIZ DE GANANCIA
    K=[[-1,0,0],
       [0,-1,0],
       [0,0,-1]]
    
    K=np.matrix(K)

    #VALORES PUNTO
    hdp=hdp.reshape(3,1)

    #LEY DE CONTROL SEGUIMIENTO TRAYECTORIA
    qref=(np.linalg.pinv(J))*(hdp+(K*he))

    #VELOCIDADES CINEMATICAS O VELOCIDADES DESEADAS PARA EL BLOQUE DE COMPENSACIÓN DINÁMICO
    uref_c=qref[0,0]
    wref_c=qref[1,0]
    q1pref_c=qref[2,0]
    q2pref_c=qref[3,0]
    q3pref_c=qref[4,0]

    # VELOCIDADES DEL CARRO
    u=vuw[0]
    w=vuw[1]

    # VELOCIDADES DEL BRAZO
    q1p=qp[0]
    q2p=qp[1]
    q3p=qp[2]

    #ERRORES DE VELOCIDAD VREF
    ue=u-uref_c;
    we=w-wref_c;
    q1pe=q1p-q1pref_c;
    q2pe=q2p-q2pref_c;
    q3pe=q3p-q3pref_c;

    #VECTOR DE ERRORES
    vref_e=[ue,we,q1pe,q2pe,q3pe]

    #DERIVADA
    vrefp_u=np.array([uref_c,uref_c]);
    vrefp_w=np.array([wref_c,wref_c]);
    vrefp_q1p=np.array([q1pref_c,q1pref_c]);
    vrefp_q2p=np.array([q2pref_c,q2pref_c]);
    vrefp_q3p=np.array([q3pref_c,q3pref_c]);

    vrefp_u=np.diff(vrefp_u)/ts;
    vrefp_w=np.diff(vrefp_w)/ts;
    vrefp_q1p=np.diff(vrefp_q1p)/ts;
    vrefp_q2p=np.diff(vrefp_q2p)/ts;
    vrefp_q3p=np.diff(vrefp_q3p)/ts;

    #VECTOR DERIVADAC
    vrefp=[vrefp_u[0],vrefp_w[0],vrefp_q1p[0],vrefp_q2p[0],vrefp_q3p[0]]
    
    #VELOCIDADES
    v=[uref_c,wref_c,q1pref_c,q2pref_c,q3pref_c]

    #ANGULOS
    q=[0,phi,q1,q2,q3]

    Dinamica=COMPENSACION_DINAMICA_3GDL(vrefp,vref_e,v,q)
    
    #VELOCIDADES NUEVAS DE PLATAFOMRA Y BRAZO  
    uref = Dinamica[0];
    wref = Dinamica[1];
    q1pref = Dinamica[2];
    q2pref = Dinamica[3];
    q3pref = Dinamica[4];
    
    vref =[uref,wref,q1pref,q2pref,q3pref]

    akasha = AKASHA_DINAMICA(vref,v,q)
    
    akasha =np.array(akasha)

    # VELOCIDADES DEL ROBOT
    u_1=akasha[0]
    w_1=akasha[1]
    q1p_1=akasha[2]
    q2p_1=akasha[3]
    q3p_1=akasha[4]
   
    # POSICIONES DEL ROBOT  
    phi_1=akasha[6]
    q1_1=akasha[7]
    q2_1=akasha[8]
    q3_1=akasha[9]

    #PLATAFORMA
    xp=u*mt.cos(phi)-a*w*mt.sin(phi);
    yp=u*mt.sin(phi)+a*w*mt.cos(phi);
   
    x_1=ts*xp+x;
    y_1=ts*yp+y;

    # CINMEATICA
    Cinematica = Calculo_Cinematica(x_1,y_1,phi_1,q1_1,q2_1,q3_1)
    Cinematica=np.array(Cinematica)
    hx_1=Cinematica[0]
    hy_1=Cinematica[1]
    hz_1=Cinematica[2]

    vf=([hx_1,hy_1,hz_1,q1_1,q2_1,q3_1,x_1,y_1,phi_1,u_1,w_1,q1p_1,q2p_1,q3p_1,
         ue,we,q1pe,q2pe,q3pe])
    
    return (vf)


    
