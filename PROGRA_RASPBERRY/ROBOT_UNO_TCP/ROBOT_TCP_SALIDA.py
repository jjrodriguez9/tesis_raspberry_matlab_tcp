import time
import socket
import DINAMICA_ROBOT as ROBOT

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect(("192.168.1.11",5005))

ts=0.1

mensaje=s.recv(64)
k=mensaje.decode().rstrip("\n")
M=k.split(",")

# VALORES INICIALES
x  =float(M[0])
y  =float(M[1])
phi=float(M[2])
q1 =float(M[3])
q2 =float(M[4])
q3 =float(M[5])

#CINEMATICA
Cinematica=ROBOT.Calculo_Cinematica(x,y,phi,q1,q2,q3)
hx=Cinematica[0]
hy=Cinematica[1]
hz=Cinematica[2]

# ENVIO DE VALORES INICIALES
R=str([hx,hy,hz,q1,q2,q3,x,y,phi,0,0,0,0,0,0,0])

R = R.encode()
s.send(R)

MATLAB=True

u=0
w=0
q1p=0
q2p=0
q3p=0

ts=0.1

while MATLAB:

    tic=time.time()
    
    mensaje=s.recv(64)
    k=mensaje.decode().rstrip("\n")
    M=k.split(",")


    hxd=float(M[0])
    hyd=float(M[1])
    hzd=float(M[2])

    hxdp=float(M[3])
    hydp=float(M[4])
    hzdp=float(M[5])
    
    hdp =[hxdp,hydp,hzdp]
    hd  =[hxd,hyd,hzd]
    
    h   =[hx,hy,hz]
    q   =[q1,q2,q3]
    vuw =[u,w]
    qp  =[q1p,q2p,q3p]
    xy  =[x,y]
    
    MANIPULADOR=ROBOT.MODELO(hdp,hd,h,phi,q,vuw,qp,xy)

    hx =MANIPULADOR[0]
    hy =MANIPULADOR[1]
    hz =MANIPULADOR[2]
    q1 =MANIPULADOR[3]
    q2 =MANIPULADOR[4]
    q3 =MANIPULADOR[5]
    x  =MANIPULADOR[6]
    y  =MANIPULADOR[7]
    phi=MANIPULADOR[8]
    u  =MANIPULADOR[9]
    w  =MANIPULADOR[10]
    q1p=MANIPULADOR[11]
    q2p=MANIPULADOR[12]
    q3p=MANIPULADOR[13]

    #ERRORES ROBOT DOS
    ue  =MANIPULADOR[14]
    we  =MANIPULADOR[15]
    q1pe=MANIPULADOR[16]
    q2pe=MANIPULADOR[17]
    q3pe=MANIPULADOR[18]

    R=str([hx,hy,hz,q1,q2,q3,x,y,phi,u,w,ue,we,q1pe,q2pe,q3pe])
 
    
    R = R.encode()
    s.send(R)
    
    while((time.time()-tic)<=ts):
        pass
     
    if(float(M[0])>=200):
        MATLAB=False
        
s.close()




