clear 
close all
clc
warning off

%% TIEMPOS 
ts=0.1; tf=100;
t=0:ts:tf;

%% HABILITAR CLIENTE UNO Y DOS TCP(ROBOTS)
Client_uno=true;
Client_dos=true;

Open_tcp(Client_uno,Client_dos);
global senddata senddata2
disp("TCP CONECT")
%% CREAR MEMORIAS PARA UNITY 
Crear_Memorias;

%% CONDICIONES DE FORMA
% xd=0.5+0.2*t;
% yd=0.3*sin(0.4*t);
% zd=0.5+0.1*sin(0.4*t);
% 
% xdp=0.2*ones(1,length(t));
% ydp=0.3*0.4*cos(0.4*t);
% zdp=0.1*0.4*cos(0.4*t);

% %% TRAYECTORIA DESEADA
% xd=1.5*sin(0.4*t);
% yd=1.5*sin(0.4*t);
% zd=0.7*ones(1,length(t));
% %% DERIVADAS DE TARYECTORIA DESEADA
% xdp=-1.5*0.4*cos(0.4*t);
% ydp=-1.5*0.4*cos(0.4*t);
% zdp=zeros(1,length(t));

% xd = 0.8*sin(0.1*t)+0.5;                xdp =0.8*0.1*cos(0.1*t);
% yd = 0.8*sin(0.1*t)+0.5;                ydp =0.8*0.1*cos(0.1*t);
% zd = 0.4*sin(0.2*t)+0.5;                zdp =0.4*0.2*cos(0.2*t);

%% CONDICIONES INICIALES DE FORMA
dd=3*ones(1,length(t));
alphad=80*pi/180*ones(1,length(t));
% alphad=atan2(ydp,xdp);
betad=0*pi/180*ones(1,length(t)); 

%% DERIVADAS DE FORMA
ddp=0*ones(1,length(t));
alphadp=0*pi/180*ones(1,length(t));
betadp=0*pi/180*ones(1,length(t));

%% POSICIONES INICIALES MANIPULADOR UNO
q11(1)=0*(pi/180);     
q21(1)=-45*(pi/180);    
q31(1)=-30*(pi/180); 
hx1(1)=1;  
hy1(1)=4;  
phi1(1)=30*(pi/180);
UR1=[hx1(1) hy1(1) phi1(1) q11(1) q21(1) q31(1) 0 0]';

%% POSICIONES INICIALES MANIPULADOR DOS
q12(1)=0*(pi/180);      
q22(1)=45*(pi/180);    
q32(1)=30*(pi/180); 
hx2(1)=-2;  
hy2(1)=2;  
phi2(1)=20*(pi/180);
UR2=[hx2(1) hy2(1) phi2(1) q12(1) q22(1) q32(1) 0 0]';

%% ENVIO DE DATOS A TCP
senddata=strcat(num2str(hx1(1)),',',num2str(hy1(1)),',',num2str(phi1(1)),','...
                    ,num2str(q11(1)),',',num2str(q21(1)),',',num2str(q31(1)));
                
senddata2=strcat(num2str(hx2(1)),',',num2str(hy2(1)),',',num2str(phi2(1)),','...
                    ,num2str(q12(1)),',',num2str(q22(1)),',',num2str(q32(1)));
                
ROBOTS=Tcp_Envio(Client_uno,Client_dos);

%% DATOS INICIALES DEL ROBOT1
x1(1)=ROBOTS(1);
y1(1)=ROBOTS(2);
z1(1)=ROBOTS(3);

%% DATOS INICIALES DEL ROBOT2
x2(1)=ROBOTS(17);
y2(1)=ROBOTS(18);
z2(1)=ROBOTS(19);

%% CALCULO DE PRIMER PUNTO
[x(1) y(1) z(1) d(1) alpha(1) beta(1)]= N(x2(1),y2(1),z2(1),x1(1),y1(1),z1(1));

GB=[x1(1) y1(1) z1(1) x2(1) y2(1) z2(1) alpha(1) beta(1) d(1)]';
%%
TD=[xd(1) yd(1) zd(1) x(1) y(1) z(1)]';

k=1;
ENABLE=0;
calllib('smClient64','setInt','ENABLE',1,ENABLE)
Errores=[0 0 0 0 0 0]';

while ENABLE==0
ENABLE=calllib('smClient64','getInt','ENABLE',0);
Enviar_Unity(UR1,UR2,GB,TD,Errores);
end


disp('INICO')

for k=1:length(t)
    tic

%% CALCULO DEL ERROR
[ne,xe(k),ye(k),ze(k),de(k),alphae(k),betae(k)]= ERROR(xd(k),yd(k),zd(k),dd(k),alphad(k),betad(k),x(k),y(k),z(k),d(k),alpha(k),beta(k));

%% VECTOR DE VELOCIDADES DEL PRIMER PUNTO
np=[xdp(k) ydp(k) zdp(k) ddp(k) alphadp(k) betadp(k)]';

%% JACOBIANA DEL PUNTO
J= JACOBIAN(x2(k),y2(k),z2(k),x1(k),y1(k),z1(k));

%% LEY DE CONTROL DEL PRIMER PUNTO
[x2p(k),y2p(k),z2p(k),x1p(k),y1p(k),z1p(k)]= LEY(J,np,ne);

%% VALORES DESEADOS ROBOTS INTEGRACION HACIA ATRAS
if k==1
x1d(k)=x1(k);
y1d(k)=y1(k);
z1d(k)=z1(k);
x2d(k)=x2(k);
y2d(k)=y2(k);
z2d(k)=z2(k);    
else
x1d(k)=ts*(x1p(k)+0.01*rand(1))+x1d(k-1);
y1d(k)=ts*(y1p(k)+0.01*rand(1))+y1d(k-1);
z1d(k)=ts*(z1p(k)+0.01*rand(1))+z1d(k-1);

x2d(k)=ts*(x2p(k)+0.01*rand(1))+x2d(k-1);
y2d(k)=ts*(y2p(k)+0.01*rand(1))+y2d(k-1);
z2d(k)=ts*(z2p(k)+0.01*rand(1))+z2d(k-1);
end

%% ENVIAR VALORES DE ROBOT UNO TCP
senddata=strcat(num2str(x1d(k)),',',num2str(y1d(k)),',',num2str(z1d(k)),','...
                    ,num2str(x1p(k)),',',num2str(y1p(k)),',',num2str(z1p(k)));
                
%% ENVIAR VALORES DE ROBOT DOS TCP
senddata2=strcat(num2str(x2d(k)),',',num2str(y2d(k)),',',num2str(z2d(k)),','...
                    ,num2str(x2p(k)),',',num2str(y2p(k)),',',num2str(z2p(k)));

%% RECIVIR DATOS DE LOS ROBOTS TCP
ROBOTS=Tcp_Envio(Client_uno,Client_dos);

%% DATOS DE ROBOT UNO
x1(k+1)=ROBOTS(1);
y1(k+1)=ROBOTS(2);
z1(k+1)=ROBOTS(3);

%% VELOCIDADES DE ESLABONES
q11(k)=ROBOTS(4);
q21(k)=ROBOTS(5);
q31(k)=ROBOTS(6);
hx1(k)=ROBOTS(7);  
hy1(k)=ROBOTS(8);  
phi1(k)=ROBOTS(9);
u1(k)=ROBOTS(10);
w1(k)=ROBOTS(11);

%% ERRORES DE VELOCIDADES
ue1(k)  =ROBOTS(12);
we1(k)  =ROBOTS(13);
q1pe1(k)=ROBOTS(14);
q2pe1(k)=ROBOTS(15);
q3pe1(k)=ROBOTS(16);

%% ENVIO DE DATOS ROBPT UNO UNITY 
UR1=[hx1(k) hy1(k) phi1(k) q11(k) q21(k) q31(k) u1(k) w1(k)]';

%% DATOS DE ROBOT DOS
x2(k+1)=ROBOTS(17);
y2(k+1)=ROBOTS(18);
z2(k+1)=ROBOTS(19);

%% VELOCIDADES
q12(k)=ROBOTS(20);
q22(k)=ROBOTS(21);
q32(k)=ROBOTS(22);
hx2(k)=ROBOTS(23);  
hy2(k)=ROBOTS(24);  
phi2(k)=ROBOTS(25);
u2(k)=ROBOTS(26);
w2(k)=ROBOTS(27);
%% ERRORRES DE VELOCIDADES
ue2(k)  =ROBOTS(28);
we2(k)  =ROBOTS(29);
q1pe2(k)=ROBOTS(30);
q2pe2(k)=ROBOTS(31);
q3pe2(k)=ROBOTS(32);

%% CALCULO DE PROXIMOS VALORES
[x(k+1) y(k+1) z(k+1) d(k+1) alpha(k+1) beta(k+1)]= N(x2(k+1),y2(k+1),z2(k+1),x1(k+1),y1(k+1),z1(k+1));
%% ENVIO DE DATOS ROBOT DOS UNITY
UR2=[hx2(k) hy2(k) phi2(k) q12(k) q22(k) q32(k) u2(k) w2(k)]';

%% ENVIO DE TRAYECTORIA DE CADA ROBOT UNOTY
GB=[x1(k) y1(k) z1(k) x2(k) y2(k) z2(k) alpha(k) beta(k) d(k)]';

%% ENVIO DE TRAYECTORIA DESEADA UNITY
TD=[xd(k) yd(k) zd(k) x(k+1) y(k+1) z(k+1) ]';

%% ENVIO DE ERRORES
Errores=[xe(k) ye(k) ze(k) de(k) alphae(k) betae(k)]';

%% ENVIO DE DATOS A UNITY (DLL)
Enviar_Unity(UR1,UR2,GB,TD,Errores);



while (toc<ts)  
end

dt(k)=toc;

disp('Simulando.......')
end

Cerrar_Tcp(Client_uno,Client_dos);
calllib('smClient64','freeMemories')

disp('CLOSE')

figure(1)
subplot(3,1,1);plot(t,xe,'r','LineWidth',2);grid on;legend('Error X');xlabel('Time[s]');ylabel('[m]');
subplot(3,1,2);plot(t,ye,'b','LineWidth',2);grid on;legend('Error Y');xlabel('Time[s]');ylabel('[m]');
subplot(3,1,3);plot(t,ze,'y','LineWidth',2);grid on;legend('Error Z');xlabel('Time[s]');ylabel('[m]');
grid on
%% ERRORES DE ORIENTACION
figure(2)
subplot(3,1,1);plot(t,de,'r','LineWidth',2);grid on;legend('Error d');xlabel('Time[s]');ylabel('[m]');
subplot(3,1,2);plot(t,alphae,'b','LineWidth',2);grid on;legend('Error alpha');xlabel('Time[s]');ylabel('[m]');
subplot(3,1,3);plot(t,betae,'y','LineWidth',2);grid on;legend('Error beta');xlabel('Time[s]');ylabel('[m]');

%% TIEMPO DE MUESTREO
figure(3)
plot(t,dt,'g','LineWidth',2);grid on;legend('Tiempo Muestreo');xlabel('Time[s]');ylabel('[ms]');

figure(4)
subplot(2,1,1);plot(t,q1pe1,'r','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[rad/s]');hold on
plot(t,q2pe1,'c','LineWidth',2)
plot(t,q3pe1,'y','LineWidth',2)
plot(t,we1,'g','LineWidth',2);legend('Error q1p','Error q2p','Error q3p','Erorrr w')
subplot(2,1,2);plot(t,q1pe2,'r','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[rad/s]');hold on
plot(t,q2pe2,'c','LineWidth',2)
plot(t,q3pe2,'y','LineWidth',2)
plot(t,we2,'g','LineWidth',2);legend('Error q1p','Error q2p','Error q3p','Erorrr w')

figure(5)
subplot(2,1,1);plot(t,ue1,'r','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[m/s]')
legend('Errror u')
subplot(2,1,2);plot(t,ue2,'g','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[m/s]');hold on
legend('Errror u')
