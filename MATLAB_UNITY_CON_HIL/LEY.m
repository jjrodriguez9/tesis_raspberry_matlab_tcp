function [x1p,y1p,z1p,x2p,y2p,z2p]= LEY(J,np,ne)

%% SEGUIMIENTO DE TRAYECTORIA

%% MATRIZ DE GANANCIA
  K1=[1.8 1.1 1.8 1.2 .5 .8];
  K2=[.5 .5 .9 .1 .1 .1];
%   K1=[.6 .6 .6 .7 .01 1];
%   K2=[.04 .04 .04 .4 .5 .5];
  M= diag(K1);
  E= diag(K2);
 
%% LEY DE CONTROL
Vcontrol=inv(J)*(np+M*tanh(E*ne));

%% VALORES DEL CONTROLADOR
x1p=Vcontrol(1);
y1p=Vcontrol(2);
z1p=Vcontrol(3);
x2p=Vcontrol(4);
y2p=Vcontrol(5);
z2p=Vcontrol(6);

end