function ROBOTS=Tcp_Envio(Client_uno,Client_dos)

global t1 t2
global senddata senddata2


if(Client_uno)
        fprintf(t1,'%s',senddata);    
        flushoutput (t1);
        while ~t1.BytesAvailable
        end
    in=fread(t1,t1.BytesAvailable); 
    in= char(in);
    str = convertCharsToStrings(in);
    str=erase(str,["[","]"]);
    ROBOT1=double(split(str,","));
    flushinput (t1); 
else
    ROBOT1=0;
end
    
if(Client_dos)
        fprintf(t2,'%s',senddata2);    
        flushoutput (t2);
        while ~t2.BytesAvailable
        end
    in2=fread(t2,t2.BytesAvailable); 
    in2= char(in2);
    str2 = convertCharsToStrings(in2);
    str2=erase(str2,["[","]"]);
    ROBOT2=double(split(str2,","));
    flushinput (t2); 
else
    ROBOT2=0;
end

ROBOTS=[ROBOT1;ROBOT2];
end