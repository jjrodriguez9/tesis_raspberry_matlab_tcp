function [ne,xe,ye,ze,de,alphae,betae]= ERROR(xd,yd,zd,dd,alphad,betad,x,y,z,d,alpha,beta)

%% ERRORES
xe=xd-x;
ye=yd-y;
ze=zd-z;
%% VECTOR ERROR DE POSICION
pe=[xe ye ze];
%% ERROR DE FORMA
de=dd-d;
alphae=alphad-alpha;
% [alphae] = ANGULO(alphaei);
betae=betad-beta;
%% VECTOR ERROR DE FORMA
se=[de alphae betae];
%% VECTOR ERROR CONTROL
ne=[pe se]';
end