function Crear_Memorias
warning off;
%% IMPORTAR MEMORIA COMPARTIDA
loadlibrary('smClient64.dll','./smClient.h');

%% ABRIR MEMORIA COMPARTIDA
calllib('smClient64','createMemory','ENABLE',1,1);   
calllib('smClient64','createMemory','ROBOT_ONE',7,2);
calllib('smClient64','createMemory','ROBOT_TWO',7,2);
calllib('smClient64','createMemory','BARRA',8,2);
calllib('smClient64','createMemory','DESEADO',6,2);
calllib('smClient64','createMemory','ERRORES',6,2);

calllib('smClient64','openMemory','BARRA',2);
calllib('smClient64','openMemory','ROBOT_ONE',2);
calllib('smClient64','openMemory','ROBOT_TWO',2);
calllib('smClient64','openMemory','DESEADO',2);
calllib('smClient64','openMemory','ENABLE',1);
calllib('smClient64','openMemory','ERRORES',1);

for i=0:8
   calllib('smClient64','setFloat','BARRA',i,0); 
   calllib('smClient64','setFloat','ROBOT_ONE',i,0); 
   calllib('smClient64','setFloat','ROBOT_TWO',i,0);
   calllib('smClient64','setFloat','ERRORES',i,0);
   calllib('smClient64','setFloat','DESEADO',i,0); 
end


end