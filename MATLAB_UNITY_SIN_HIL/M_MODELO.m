
function MANIPULADOR = M_MODELO(hxdp,hydp,hzdp,hxd,hyd,hzd,hx,hy,hz,phi,q1,q2,q3,u,w,ts,q1p,q2p,q3p,x,y)

   %% MEDIDAS
l1= 0;
l2= 0.275;
l3= 0.425;
l4= 0;
a=0.175;
altura=0.375;

   %% ERRORES DE CONTROL
    hxe=hx-hxd;
    hye=hy-hyd;
    hze=hz-hzd;
    
    he=[hxe hye hze]';
    
   %% JACCOBIANO
    j11=cos(phi);
    j12=-a*sin(phi)-l2*cos(q2)*sin(q1+phi)-l3*cos(q2+q3)*sin(q1+phi);
    j13=-l2*cos(q2)*sin(q1+phi)-l3*cos(q2+q3)*sin(q1+phi);
    j14=-l2*cos(q1+phi)*sin(q2)-l3*cos(q1+phi)*sin(q2+q3);
    j15=-l3*cos(q1+phi)*sin(q2+q3);
   
    j21=sin(phi);
    j22=a*cos(phi)+l2*cos(q2)*cos(q1+phi)+l3*cos(q2+q3)*cos(q1+phi);
    j23=l2*cos(q2)*cos(q1+phi)+l3*cos(q2+q3)*cos(q1+phi);
    j24=-l2*sin(q1+phi)*sin(q2)-l3*sin(q1+phi)*sin(q2+q3);
    j25=-l3*sin(q1+phi)*sin(q2+q3);
    
    j31=0;
    j32=0;
    j33=0;
    j34=l2*cos(q2)+l3*cos(q2+q3);
    j35=l3*cos(q2+q3);
      
    J=[j11 j12 j13 j14 j15;
       j21 j22 j23 j24 j25;
       j31 j32 j33 j34 j35]; 
   
   %% MATRIZ DE GANANCIA
   K=[ -1 0  0; 
       0 -1  0;
       0  0 -1];
   
   hdp=[hxdp hydp hzdp]';
   
   %% LEY DE CONTROL SEGUIMIENTO DE TRAYECTORIA 
   qref=pinv(J)*(hdp+K*tanh(he));
   
   %% VELOCIDADES CINEMATICAS O VELOCIDADES DESEADAS PARA EL BLOQUE DE COMPENSACIÓN DINÁMICO
   uref_c=qref(1);
   wref_c=qref(2);
   q1pref_c=qref(3);
   q2pref_c=qref(4);
   q3pref_c=qref(5);

   %% ERRORES DE VELOCIDAD VREF
    ue=u-uref_c;
    we=w-wref_c;
    q1pe=q1p-q1pref_c;
    q2pe=q2p-q2pref_c;
    q3pe=q3p-q3pref_c;
    
    vref_e=[ue we q1pe q2pe q3pe]';
    
   %% DERIVADAS DE LAS VREF DESEADAS
    vrefp_u=diff([uref_c uref_c(end)])/ts;
    vrefp_w=diff([wref_c wref_c(end)])/ts;
    vrefp_q1p=diff([q1pref_c q1pref_c(end)])/ts;
    vrefp_q2p=diff([q2pref_c q2pref_c(end)])/ts;
    vrefp_q3p=diff([q3pref_c q3pref_c(end)])/ts;
    
    vrefp=[vrefp_u vrefp_w vrefp_q1p vrefp_q2p vrefp_q3p]';
    
   %% BLOQUE DE COMPENSACIÓN DINÁMICA
    v=[uref_c wref_c q1pref_c q2pref_c q3pref_c]';
    q=[0 phi q1 q2 q3]';

    Dinamica=COMPENSACION_DINAMICA_3GDL(vrefp,vref_e,v,q,ts);
    
   %% VELOCIDADES NUEVAS DE PLATAFOMRA Y BRAZO  
    uref = Dinamica(1);
    wref = Dinamica(2);
    q1pref = Dinamica(3);
    q2pref = Dinamica(4);
    q3pref = Dinamica(5);
    
    vref =[uref wref q1pref q2pref q3pref]';

    
%      vref=[qref(1) we q1pe q2pe q3pe]'

   %% MODELO DEL ROBOT AKASHA
   akasha = AKASHA_DINAMICA(vref,v,q,ts);

   %% VELOCIDADES DEL ROBOT
   u_1=akasha(1);
   w_1=akasha(2);
   q1p_1=akasha(3);
   q2p_1=akasha(4);
   q3p_1=akasha(5);
   
   %% POSICIONES DEL ROBOT  
   phi_1=akasha(7);
   q1_1=akasha(8);
   q2_1=akasha(9);
   q3_1=akasha(10);
   
   %% PLATAFORMA
   xp=u*cos(phi)-a*w*sin(phi);
   yp=u*sin(phi)+a*w*cos(phi);
   
   x_1=ts*xp+x;
   y_1=ts*yp+y;

  %% CINMEATICA
Cinematica = Calculo_Cinematica(x_1,y_1,phi_1,q1_1,q2_1,q3_1);
hx_1=Cinematica(1);
hy_1=Cinematica(2);
hz_1=Cinematica(3);
errores=[vref_e]';


  %% SALIDA DE VALORES
vf=[hx_1 hy_1 hz_1 q1_1 q2_1 q3_1 x_1 y_1 phi_1 u_1 w_1 q1p_1 q2p_1 q3p_1 errores he']';
MANIPULADOR=[vf];

end

