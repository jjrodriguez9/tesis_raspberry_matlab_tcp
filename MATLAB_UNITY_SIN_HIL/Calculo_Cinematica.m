function Cinematica = Calculo_Cinematica(x,y,phi,q1,q2,q3)
%% DISTANCIAS DE LOS ESLABONES
l1= 0;
l2= 0.275;
l3= 0.425;
l4= 0;
%l4=0.15;
a=0.175;
altura=0.375;

%% CINEMATICA DIRECTA 
hx=x+a*cos(phi)+l2*cos(q1+phi)*cos(q2)+l3*cos(q1+phi)*cos(q2+q3);
hy=y+a*sin(phi)+l2*sin(q1+phi)*cos(q2)+l3*sin(q1+phi)*cos(q2+q3);
hz=altura+l1+l2*sin(q2)+l3*sin(q2+q3);

C= [hx hy hz ]';  
Cinematica = [C];
end