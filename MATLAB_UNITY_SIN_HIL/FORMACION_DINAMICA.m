%% FORMACION 
close all; clear all; clc;warning off;
ts=0.1; tf=100;
t=[0:ts:tf];
l1= 0;
l2= 0.275;
l3= 0.425;
l4= 0;
a=0.175;
altura=0.375;

%% TRAYECTORIA DESEADA
% xd=1.5*sin(0.4*t);
% yd=1.5*sin(0.4*t);
% zd=0.7*ones(1,length(t));
%% DERIVADAS DE TARYECTORIA DESEADA
% xdp=-1.5*0.4*cos(0.4*t);
% ydp=-1.5*0.4*cos(0.4*t);
% zdp=zeros(1,length(t));
% xd=0.5+0.2*t;
% yd=0.3*sin(0.4*t);
% zd=0.5+0.1*sin(0.4*t);
% 
% xdp=0.2*ones(1,length(t));
% ydp=0.3*0.4*cos(0.4*t);
% zdp=0.1*0.4*cos(0.4*t);
xd = 0.8*sin(0.1*t)+0.5;                xdp =0.8*0.1*cos(0.1*t);
yd = 0.8*sin(0.1*t)+0.5;                ydp =0.8*0.1*cos(0.1*t);
zd = 0.4*sin(0.2*t)+0.5;                zdp =0.4*0.2*cos(0.2*t);

%% CONDICIONES INICIALES DE FORMA
dd=[];alphad=[];betad=[];

dt=4*ones(1,length(t));
alphat=70*pi/180*ones(1,length(t));
% alphad=atan2(ydp,xdp);
betat=1*pi/180*ones(1,length(t)); 

%% DERIVADAS DE FORMA
ddp=0*ones(1,length(t));
alphadp=0*pi/180*ones(1,length(t));
betadp=0*pi/180*ones(1,length(t));

dd=[dt(1),dt(2),dt];
alphad=[-20*pi/180,alphat(2),alphat];
betad=[0*pi/180,betat(2),betat];
%% POSICIONES INICIALES MANIPULADOR UNO
q11(1)=20*(pi/180);     
q21(1)=5*(pi/180);    
q31(1)=-30*(pi/180); 
hx1(1)=3;  
hy1(1)=3;  
phi1(1)=-50*(pi/180);
UR1=[hx1(1) hy1(1) phi1(1) q11(1) q21(1) q31(1) 0 0]';

%% POSICIONES INICIALES MANIPULADOR DOS
q12(1)=0*(pi/180);      
q22(1)=-10*(pi/180);    
q32(1)=30*(pi/180); 
hx2(1)=2;  
hy2(1)=2;  
phi2(1)=-70*(pi/180);
UR2=[hx2(1) hy2(1) phi2(1) q12(1) q22(1) q32(1) 0 0]';
%% VEL ROBOT UNO
u1(1) = 0;
w1(1) = 0;
q1p1(1) = 0;
q2p1(1) = 0;
q3p1(1) = 0;

%% VEL ROBOT DOS
u2(1) = 0;
w2(1) = 0;
q1p2(1) = 0;
q2p2(1) = 0;
q3p2(1) = 0;
%% CALCULO DE PUNTOS INCIALES ROBOT UNO
Cinematica1=Calculo_Cinematica(hx1,hy1,phi1,q11,q21,q31);
x1(1)=Cinematica1(1);
y1(1)=Cinematica1(2);
z1(1)=Cinematica1(3);

%% CALCULO DE PUNTOS INCIALES ROBOT DOS
Cinematica2=Calculo_Cinematica(hx2,hy2,phi2,q12,q22,q32);
x2(1)=Cinematica2(1);
y2(1)=Cinematica2(2);
z2(1)=Cinematica2(3);

%% CALCULO DE PRIMER PUNTO
[x(1) y(1) z(1) d(1) alpha(1) beta(1)]= N(x2(1),y2(1),z2(1),x1(1),y1(1),z1(1));

%% VALORES INICIAL ROBOT UNO
x1d(1)=x1(1);
y1d(1)=y1(1);
z1d(1)=z1(1);
%% VALORES INCIAL ROBOT  DOS
x2d(1)=x2(1);
y2d(1)=y2(1);
z2d(1)=z2(1);
%%

Crear_Memorias;

GB=[x1(1) y1(1) z1(1) x2(1) y2(1) z2(1) alpha(1) beta(1) d(1)]';
%%
TD=[xd(1) yd(1) zd(1) x(1) y(1) z(1)]';


j=1;
k=1;

ENABLE=0;
calllib('smClient64','setInt','ENABLE',1,ENABLE)
Errores=[0 0 0 0 0 0]';

disp('INICO')
vdx=[];
vdy=[];
vdz=[];

vdx=[ 2.1,xd(1)];
vdy=[-4.2,yd(1)];
vdz=[0.01,zd(1)];
% vdx=[vdx,xd];
% vdy=[vdy,yd];
% vdz=[vdz,zd];
% vdx=[ 2.1,-2.1,-2.1];
% vdy=[-4.2,4.2,4.2];
% vdz=[0.01,0.7,0.01];

vdx=[vdx,xd];
vdy=[vdy,yd];
vdz=[vdz,zd];

% posiciones=length(vdx);

posiciones=2;

while ENABLE==0
i=1;

%% CALCULO DE LA GRAFICVA PARA UNITY
b=1/2;
rx1=xd(i)+b*(dt(i)*cos(betat(i))*cos(alphat(i)));
ry1=yd(i)+b*(dt(i)*cos(betat(i))*sin(alphat(i)));
rz1=zd(i)+b*(dt(i)*sin(betat(i)));

rx2=xd(i)-b*(dt(i)*cos(betat(i))*cos(alphat(i)));
ry2=yd(i)-b*(dt(i)*cos(betat(i))*sin(alphat(i)));
rz2=zd(i)-b*(dt(i)*sin(betat(i)));

ENABLE=calllib('smClient64','getInt','ENABLE',0);
GB=[rx1 ry1 rz1 rx2 ry2 rz2 alphad(i) betad(i) dd(i)]';
TD=[xd(i) yd(i) zd(i) vdx(i) vdy(i) vdz(i) ]';
Enviar_Unity(UR1,UR2,GB,TD,Errores);
err=0.01;



end
vac=0;
control="co";


pause(5);
%% CONTROL PARA IR POR LA BARRA
disp('POSICION INICIAL')

while true
tic

if(i<=posiciones)
[ne,xe(j),ye(j),ze(j),de(j),alphae(j),betae(j)]= ERROR(vdx(i),vdy(i),vdz(i),dd(i),alphad(i),betad(i),x(j),y(j),z(j),d(j),alpha(j),beta(j));
else
[ne,xe(j),ye(j),ze(j),de(j),alphae(j),betae(j)]= ERROR(vdx(k),vdy(k),vdz(k),dd(k),alphad(k),betad(k),x(j),y(j),z(j),d(j),alpha(j),beta(j));    
end

if(control=="home")
[ne,xe(j),ye(j),ze(j),de(j),alphae(j),betae(j)]= ERROR(-vdx(1),-vdy(1),vdz(1),dd(1),0*pi/180,0*pi/180,x(j),y(j),z(j),d(j),alpha(j),beta(j));    
end

%% JACOBIANA DEL PUNTO
J= JACOBIAN(x2(j),y2(j),z2(j),x1(j),y1(j),z1(j));

if (i<=posiciones)
%% VECTOR DE VELOCIDADES DEL PRIMER PUNTO
np=[0 0 0 0 0 0]';
%% LEY DE CONTROL DEL PRIMER PUNTO
[x2p(j),y2p(j),z2p(j),x1p(j),y1p(j),z1p(j)]= LEY_POSICION(J,np,ne);
else
%% LEY DE CONTROL DEL PRIMER PUNTO
%% VECTOR DE VELOCIDADES DEL PRIMER PUNTO
np=[xdp(k) ydp(k) zdp(k) ddp(k) alphadp(k) betadp(k)]';
[x2p(j),y2p(j),z2p(j),x1p(j),y1p(j),z1p(j)]= LEY(J,np,ne);  
end

if control=="home"
 np=[0 0 0 0 0 0]';
%% LEY DE CONTROL DEL PRIMER PUNTO
[x2p(j),y2p(j),z2p(j),x1p(j),y1p(j),z1p(j)]= LEY_POSICION(J,np,ne);   
end
%% VALORES DESEADOS ROBOTS
x1d(j+1)=ts*x1p(j)+x1d(j);
y1d(j+1)=ts*y1p(j)+y1d(j);
z1d(j+1)=ts*z1p(j)+z1d(j);

x2d(j+1)=ts*x2p(j)+x2d(j);
y2d(j+1)=ts*y2p(j)+y2d(j);
z2d(j+1)=ts*z2p(j)+z2d(j);
%% ENVIAR VALORES DE ROBOT UNO
% ROBOT1 = MODELO(x1p(k),y1p(k),z1p(k),x1d(k+1),y1d(k+1),z1d(k+1),x1(k),y1(k),z1(k),phi1(k),q11(k),q21(k),q31(k),ts,hx1(k),hy1(k));
ROBOT1 = M_MODELO(x1p(j),y1p(j),z1p(j),x1d(j),y1d(j),z1d(j),x1(j),y1(j),z1(j),phi1(j),q11(j),q21(j),q31(j),u1(j),w1(j),ts,q1p1(j),q2p1(j),q3p1(j),hx1(j),hy1(j));
x1(j+1)  =ROBOT1(1);
y1(j+1)  =ROBOT1(2);
z1(j+1)  =ROBOT1(3);
q11(j+1) =ROBOT1(4);
q21(j+1) =ROBOT1(5);
q31(j+1) =ROBOT1(6);
hx1(j+1) =ROBOT1(7);
hy1(j+1) =ROBOT1(8);
phi1(j+1)=ROBOT1(9);
u1(j+1)  =ROBOT1(10);
w1(j+1)  =ROBOT1(11);
q1p1(j+1)=ROBOT1(12);
q2p1(j+1)=ROBOT1(13);
q3p1(j+1)=ROBOT1(14);
%% ERRORES ROBOT UNO
u1e(j)=ROBOT1(15);
w1e(j)=ROBOT1(16);
q1p1e(j)=ROBOT1(17);
q2p1e(j)=ROBOT1(18);
q3p1e(j)=ROBOT1(19);

cr1x(j)=ROBOT1(20);
cr1y(j)=ROBOT1(21);
cr1z(j)=ROBOT1(22);


%% ENVIO DE DATOS ROBPT UNO UNITY 
UR1=[hx1(j) hy1(j) phi1(j) q11(j) q21(j) q31(j) u1(j) w1(j)]';

%% ENVIAR VALORES DE ROBOT DOS
ROBOT2 = M_MODELO(x2p(j),y2p(j),z2p(j),x2d(j),y2d(j),z2d(j),x2(j),y2(j),z2(j),phi2(j),q12(j),q22(j),q32(j),u2(j),w2(j),ts,q1p2(j),q2p2(j),q3p2(j),hx2(j),hy2(j));
x2(j+1)=ROBOT2(1);
y2(j+1)=ROBOT2(2);
z2(j+1)=ROBOT2(3);
q12(j+1)=ROBOT2(4);
q22(j+1)=ROBOT2(5);
q32(j+1)=ROBOT2(6);
hx2(j+1)=ROBOT2(7);
hy2(j+1)=ROBOT2(8);
phi2(j+1)=ROBOT2(9);
u2(j+1)=ROBOT2(10);
w2(j+1)=ROBOT2(11);
q1p2(j+1)=ROBOT2(12);
q2p2(j+1)=ROBOT2(13);
q3p2(j+1)=ROBOT2(14);
%% ERRORES ROBOT DOS
u2e(j)=ROBOT2(15);
w2e(j)=ROBOT2(16);
q1p2e(j)=ROBOT2(17);
q2p2e(j)=ROBOT2(18);
q3p2e(j)=ROBOT2(19);

cr2x(j)=ROBOT2(20);
cr2y(j)=ROBOT2(21);
cr2z(j)=ROBOT2(22);
%% PROXIMOS VALORES 
[x(j+1) y(j+1) z(j+1) d(j+1) alpha(j+1) beta(j+1)]= N(x2(j+1),y2(j+1),z2(j+1),x1(j+1),y1(j),z1(j+1));

UR2=[hx2(j) hy2(j) phi2(j) q12(j) q22(j) q32(j) u2(j) w2(j)]';

if(i==1)
GB=[rx1 ry1 rz1 rx2 ry2 rz2 alphad(i) betad(i) dd(i)]';
TD=[xd(j) yd(j) zd(j) vdx(i) vdy(i) vdz(i) ]';
end

if(i==2&&i<=posiciones)
GB=[rx1 ry1 rz1 rx2 ry2 rz2 alpha(j) beta(j) d(j)]';
TD=[xd(j) yd(j) zd(j) x(j+1) y(j+1) z(j+1) ]';
end

if(i>posiciones && not(control=="home"))
GB=[x1(j) y1(j) z1(j) x2(j) y2(j) z2(j) alpha(j) beta(j) d(j)]';
TD=[xd(j) yd(j) zd(j) x(j+1) y(j+1) z(j+1) ]';   
end

if (control=="home")
GB=[x1(length(t)) y1(length(t)) z1(length(t)) x2(length(t)) y2(length(t)) z2(length(t)) alpha(j) beta(j) d(j)]';
TD=[xd(length(t)) yd(length(t)) zd(length(t)) x(j+1) y(j+1) z(j+1) ]';    
end
%% ENVIO DE ERRORES
Errores=[xe(j) ye(j) ze(j) de(j) alphae(j) betae(j)]';

%% ENVIO DE DATOS A UNITY (DLL)
Enviar_Unity(UR1,UR2,GB,TD,Errores);

while (toc<ts)  
end

dt(j)=toc;

if(i<=posiciones) 
if (norm(ne)<err)
%     pause(3);
    i=i+1;
    k=k+1;

end
end

if(j==length(t))
    control="home";
    disp('regresop')
    vac=vac+1;
end
if (j>length(t))
    control="home";
    if (norm(ne)<err)
       break 
    end
end

if(i>posiciones)
    k=k+1;
end

j=j+1;
disp('Simulando......')
end 
  
pause(1);
calllib('smClient64','freeMemories')
disp('CLOSE')

t=0:ts:(j*ts)-ts;

figure('Name','ERRORES DE POSICION COOPERATIVO');
subplot(3,1,1);plot(t,xe,'r','LineWidth',2);grid on;legend('Error X');xlabel('Time[s]');ylabel('[m]');title('ERRORES DE POSICION X')
subplot(3,1,2);plot(t,ye,'b','LineWidth',2);grid on;legend('Error Y');xlabel('Time[s]');ylabel('[m]');title('ERRORES DE POSICION Y')
subplot(3,1,3);plot(t,ze,'y','LineWidth',2);grid on;legend('Error Z');xlabel('Time[s]');ylabel('[m]');title('ERRORES DE POSICION Z')
grid on
%% ERRORES DE ORIENTACION
figure('Name','ERRORES DE FORMA COOPERATIVO');
subplot(3,1,1);plot(t,de,'r','LineWidth',2);grid on;legend('Error Distancia');xlabel('Time[s]');ylabel('[m]');title('ERRORES DE FORMA Distancia')
subplot(3,1,2);plot(t,alphae,'b','LineWidth',2);grid on;legend('Error Alpha');xlabel('Time[s]');ylabel('[rad]');title('ERRORES DE FORMA Alpha')
subplot(3,1,3);plot(t,betae,'y','LineWidth',2);grid on;legend('Error Beta');xlabel('Time[s]');ylabel('[rad]');title('ERRORES DE FORMA Beta')

%% ERRORES DE ROBOT UNO DINAMICA
figure('Name','ERRORES DE ROBOT UNO DINAMICA');
subplot(3,1,1);plot(t,u1e,'r','LineWidth',2);legend('Error u');title('Error Velocidad Lineal');grid on;xlabel('Time[s]');ylabel('[m/s]')
subplot(3,1,2);plot(t,w1e,'b','LineWidth',2);legend('Error w');title('Error Velocidad Angular');grid on;xlabel('Time[s]');ylabel('[rad/s]')
subplot(3,1,3);plot(t,q1p1e,'g','LineWidth',2); hold on;plot(t,q2p1e,'y','LineWidth',2);plot(t,q3p1e,'c','LineWidth',2);
legend('Error q1p','Error q2p','Error q3p');title('Error de Velocidades Brazo');grid on;xlabel('Time[s]');ylabel('[rad/s]')

%% ERRORES DE ROBOT DOS DINAMICA
figure('Name','ERRORES DE ROBOT DOS DINAMICA');
subplot(3,1,1);plot(t,u2e,'r','LineWidth',2);legend('u_Error');title('Error Velocidad Lineal');grid on;xlabel('Time[s]');ylabel('[m/s]')
subplot(3,1,2);plot(t,w2e,'b','LineWidth',2);legend('w_Error');title('Error Velocidad Angular');grid on;xlabel('Time[s]');ylabel('[rad/s]')
subplot(3,1,3);plot(t,q1p2e,'g','LineWidth',2); hold on;plot(t,q2p2e,'y','LineWidth',2);plot(t,q3p2e,'c','LineWidth',2);
legend('Error q1p','Error q2p','Error q3p');title('Error de Velocidades Brazo');grid on;xlabel('Time[s]');ylabel('[rad/s]')

figure('Name','ERRORES DE CINEMATICA');
subplot(2,1,1);plot(t,cr1x,'r','LineWidth',2);hold on ;plot(t,cr1y,'b','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[m]')
plot(t,cr1z,'g','LineWidth',2);legend('Error x','Error y','Error z');title('Errores de Extremo Operativo Robot Uno')

subplot(2,1,2);plot(t,cr2x,'r','LineWidth',2);hold on ;plot(t,cr2y,'b','LineWidth',2);grid on;xlabel('Time[s]');ylabel('[m]')
plot(t,cr2z,'g','LineWidth',2);legend('Error x','Error y','Error z');title('Errores de Extremo Operativo Robot Dos')
% 
