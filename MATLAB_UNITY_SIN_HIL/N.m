
function [x y z d alpha beta]= N(x1,y1,z1,x2,y2,z2)
%% CALCULO DE VALORES DE POSICION
x=1/2*(x2+x1);
y=1/2*(y2+y1);
z=1/2*(z2+z1);
%% CALCULO DE VALORES DE FORMA
d=sqrt((x2-x1).^2+(y2-y1).^2+(z2-z1).^2);
alpha=atan2((y2-y1),(x2-x1));
beta=atan2((z2-z1),(x2-x1));

end