%% ************************************************************************
%% *****************SEGUIMIENTO DE TRAYECTORIA*****************************
%% ************************************************************************
clear all;  close all;  clc; warning off;

ts=0.1;  tfin=300;
t=[0.1:ts:tfin];
%%
%1) Librerias Memorias compartidas
loadlibrary('smClient64.dll','./smClient.h')
calllib('smClient64','createMemory','MemoriaONE',7,2)
calllib('smClient64','createMemory','MemoriaTWO',7,2)
calllib('smClient64','createMemory','ENABLE',1,1)
calllib('smClient64','openMemory','ENABLE',1)
calllib('smClient64','openMemory','MemoriaONE',2)
calllib('smClient64','openMemory','MemoriaTWO',2)

%2) Inicializa memorias
for i=0:7
    calllib('smClient64','setFloat','MemoriaONE',i,0)
    calllib('smClient64','setFloat','MemoriaTWO',i,0)
end
%%
% Par�mtros del robot
  a  = 0.175;            
  h  = 0.375;
  l2 = 0.275;            
  l3 = 0.275;          
  l4 = 0.15;

% Condiciones iniciales
  x(1) = 0; % PLATAFORMA M�VIL
  y(1) = 0;
  th(1)= 0*pi/180; 
  q1(1)= 0*pi/180; %BRAZO
  q2(1)= 90*pi/180;
  q3(1)=-90*pi/180;
  q4(1)=-90*pi/180;

% Trayectoria senoidal
 

% a)Silla de montar         
       hxd = 3.5*cos(0.05*t)+1.75;    hxd_p = -3.5*0.05*sin(0.05*t);      
       hyd = 3.5*sin(0.05*t)+1.75;    hyd_p =  3.5*0.05*cos(0.05*t);     
       hzd = 0.3*sin(0.1*t)+0.6;     hzd_p =  0.3*0.1*cos(0.1*t); 

% Posici�n inicial del extremo operativo   
    hx(1) = x(1)+a*cos(th(1))+cos(q1(1)+th(1))*(l2*cos(q2(1))+l3*cos(q2(1)+q3(1))+l4*cos(q2(1)+q3(1)+q4(1)));
    hy(1) = y(1)+a*sin(th(1))+sin(q1(1)+th(1))*(l2*cos(q2(1))+l3*cos(q2(1)+q3(1))+l4*cos(q2(1)+q3(1)+q4(1)));
    hz(1) = h+l2*sin(q2(1))+l3*sin(q2(1)+q3(1))+l4*sin(q2(1)+q3(1)+q4(1));
    
%************************************************************************** 
%***************************ANIMACI�N****************************************
%************************************************************************** 
close all; paso=1; fig=figure;
axis equal; 
%a) Par�metros del cuadro de animaci�n
    set(fig,'position',[10 60 980 600]);
    cameratoolbar
    view(40,40); 
    
%b) Dimenciones del Robot
    DimensionesMovil();
    DimensionesManipulador(a,h);

%c) Dibujo del Robot    
    G1=Movil3D(x(1),y(1),th(1));
    G2=Manipulador3D(x(1),y(1),th(1),q1(1),q2(1),q3(1),q4(1));
    G3=plot3(hx(1),hy(1),hz(1),'b','linewidth',2);hold on,grid on   
    G4=plot3(hxd(1),hyd(1),hzd(1),'r','linewidth',2);
    
% ESPERAR QUE SE INICIE LA SIMULACION EN UNITY
 enable=calllib('smClient64','getInt','ENABLE',0); 
 while (enable == 0)
 enable=calllib('smClient64','getInt','ENABLE',0);     
 disp("Wainting Unity3D...: " + enable);
 pause(1);
 end

for k=1:length(t)
    tic
 %Envia posiciones deseadas (solo para gr�ficar)
    calllib('smClient64','setFloat','MemoriaONE',7,hxd(k));
    calllib('smClient64','setFloat','MemoriaONE',8,hyd(k));
    calllib('smClient64','setFloat','MemoriaONE',9,hzd(k));
 %a)Errores de control
    hxe(k)= hxd(k)-hx(k);
    hye(k)= hyd(k)-hy(k);
    hze(k)= hzd(k)-hz(k);
    he = [hxe(k) hye(k) hze(k)]';
     
 %b)Matriz Jacobiana
    j11 = cos(th(k));
    j12 = -sin(q1(k)+th(k))*(l3*cos(q2(k)+q3(k))+l2*cos(q2(k))+l4*cos(q2(k)+q3(k)+q4(k)))-a*sin(th(k));
    j13 = -sin(q1(k)+th(k))*(l3*cos(q2(k)+q3(k))+l2*cos(q2(k))+l4*cos(q2(k)+q3(k)+q4(k)));
    j14 = -cos(q1(k)+th(k))*(l3*sin(q2(k)+q3(k))+l2*sin(q2(k))+l4*sin(q2(k)+q3(k)+q4(k)));
    j15 = -cos(q1(k)+th(k))*(l3*sin(q2(k)+q3(k))+l4*sin(q2(k)+q3(k)+q4(k)));
    j16 = -l4*sin(q2(k)+q3(k)+q4(k))*cos(q1(k)+th(k));
        
    j21 = sin(th(k));
    j22 = cos(q1(k)+th(k))*(l3*cos(q2(k)+q3(k))+l2*cos(q2(k))+l4*cos(q2(k)+q3(k)+q4(k)))+a*cos(th(k));
    j23 = cos(q1(k)+th(k))*(l3*cos(q2(k)+q3(k))+l2*cos(q2(k))+l4*cos(q2(k)+q3(k)+q4(k)));
    j24 = -sin(q1(k)+th(k))*(l3*sin(q2(k)+q3(k))+l2*sin(q2(k))+l4*sin(q2(k)+q3(k)+q4(k)));
    j25 = -sin(q1(k)+th(k))*(l3*sin(q2(k)+q3(k))+l4*sin(q2(k)+q3(k)+q4(k)));
    j26 = -l4*sin(q2(k)+q3(k)+q4(k))*sin(q1(k)+th(k));
        
    j31 = 0;
    j32 = 0;
    j33 = 0;
    j34 = l3*cos(q2(k)+q3(k))+l2*cos(q2(k))+l4*cos(q2(k)+q3(k)+q4(k));
    j35 = l3*cos(q2(k)+q3(k))+l4*cos(q2(k)+q3(k)+q4(k));
    j36 = l4*cos(q2(k)+q3(k)+q4(k));
    
    J=[j11 j12 j13 j14 j15 j16; j21 j22 j23 j24 j25 j26; j31 j32 j33 j34 j35 j36];
    
%c)Matriz de Ganancia
    W = [1 0 0; 0 1 0; 0 0 1]';
    
%d)Vector Nulo
    q1dm = 0*pi/180; q2dm = 90*pi/180; q3dm = -90*pi/180; q4dm = 0*pi/180;
    n = [0;...
         0;...
         q1dm-q1(k);...
         q2dm-q2(k);...
         q3dm-q3(k);...
         q4dm-q4(k)];
    
%e)Ley de Control
    hdp = [hxd_p(k) hyd_p(k) hzd_p(k)]';   
    Vref = pinv(J)*(hdp+W*tanh(0.5*he))+(eye(6)-pinv(J)*J)*n;
    u(k) = Vref(1);   
    w(k) = Vref(2);     
    q1_p(k)= Vref(3);    
    q2_p(k)= Vref(4);   
    q3_p(k)= Vref(5);   
    q4_p(k)= Vref(6);   
    
%f)ROBOT PLATAFORMA M�VIL    
    xp=u(k)*cos(th(k))-a*w(k)*sin(th(k)); 
    yp=u(k)*sin(th(k))+a*w(k)*cos(th(k));
    
    x(k+1) = xp*ts+x(k);    calllib('smClient64','setFloat','MemoriaONE',0,x(k+1));
    y(k+1) = yp*ts+y(k);    calllib('smClient64','setFloat','MemoriaONE',1,y(k+1));
    th(k+1)= w(k)*ts+th(k); calllib('smClient64','setFloat','MemoriaONE',2,th(k+1));  
    
    q1(k+1)=q1(k)+q1_p(k)*ts; calllib('smClient64','setFloat','MemoriaONE',3,q1(k+1));
    q2(k+1)=q2(k)+q2_p(k)*ts; calllib('smClient64','setFloat','MemoriaONE',4,q2(k+1));
    q3(k+1)=q3(k)+q3_p(k)*ts; calllib('smClient64','setFloat','MemoriaONE',5,q3(k+1));  
    q4(k+1)=q4(k)+q4_p(k)*ts; calllib('smClient64','setFloat','MemoriaONE',6,q4(k+1));
    
    %)Posici�n del estremo operativo en K+1
%     hx(k+1) = x(k+1)+a*cos(th(k+1))+cos(q1(k+1)+th(k+1))*(l2*cos(q2(k+1))+l3*cos(q2(k+1)+q3(k+1))+l4*cos( q2(k+1)+q3(k+1)+q4(k+1)));
%     hy(k+1) = y(k+1)+a*sin(th(k+1))+sin(q1(k+1)+th(k+1))*(l2*cos(q2(k+1))+l3*cos(q2(k+1)+q3(k+1))+l4*cos(q2(k+1)+q3(k+1)+q4(k+1)));
%     hz(k+1) = h+l2*sin(q2(k+1))+l3*sin(q2(k+1)+q3(k+1))+l4*sin(q2(k+1)+q3(k+1)+q4(k+1)); 
    
    hx(k+1) = calllib('smClient64','getFloat','MemoriaTWO',0);
    hy(k+1) = calllib('smClient64','getFloat','MemoriaTWO',1);
    hz(k+1) = calllib('smClient64','getFloat','MemoriaTWO',2);
    
  
    %drawnow
    %delete(G1);
    %delete(G2);
    %delete(G3);
    %delete(G4);

    %G1 = Movil3D(x(k+1),y(k+1),th(k+1));
    %G2 = Manipulador3D(x(k+1),y(k+1),th(k+1),q1(k+1),q2(k+1),q3(k+1),q4(k+1));
    G3 = plot3(hx(1:k),hy(1:k),hz(1:k),'g','linewidth',2); 
    G4 = plot3(hxd(1:k),hyd(1:k),hzd(1:k),'r','linewidth',2); 

    while(toc<=ts)
    end
    
    dt(k) = toc;
    disp("ts: "+dt(k));
    pause(0);

end



    

for k = 1:80:length(t)
    
    xlabel('X[m]'), ylabel('Y[m]'), zlabel('Z[m]')
    
%     axis([-1 5 -1 4 0 1]);
end

%%************************************************************************** 
%***************************GRAFICAS****************************************
%************************************************************************** 
figure,plot(hxe,'r','linewidth',2),hold on,plot(hye,'g','linewidth',2),plot(hze,'b','linewidth',2),legend('errx','erry','errz'),grid on, title ('Errores de Control')
figure,plot(q1_p,'r','linewidth',2),hold on,plot(q2_p,'g','linewidth',2),plot(q3_p,'b','linewidth',2),plot(q4_p,'m','linewidth',2),legend('q1p','q2p','q3p','q4p'),grid on, , title ('Velocidades de Eslabones')
figure,plot(u,'r','linewidth',2),legend('u'), grid on, title ('Velocidad lineal')
figure,plot(w,'r','linewidth',2),legend('w'), grid on, title ('Velocidad angular')