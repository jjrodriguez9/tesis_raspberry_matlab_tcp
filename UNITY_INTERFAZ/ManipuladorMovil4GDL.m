%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
%XXXXXXXXXXXXXXXXXXXCONTROL DE TRAYECTORIA MANIPULADOR MOVILXXXXXXXXXXXXXXXXXXXXXXX
%XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX
close; clear all; clc;
ts=0.1; tf= 200;
t=[0:ts:tf];

%%CONDICIONES INICIALES  
q1(1)=0*(pi/180);      %%% cero
q2(1)=45*(pi/180);    %%% positivo
q3(1)=-30*(pi/180); %% negativo
q4(1) = -30*(pi/180);
x(1)=10;  %posicion inicial en x
y(1)=3;   % posicion inicial en y
phi(1)=-30*(pi/180);

%TRAYECTORIA DESEADAS
% hxd=1*sin(0.3*t);
% hyd=1*cos(0.3*t);
% hzd=0.004*t;
% 
% %DERIVADAS 
% hxdp=1*0.3*cos(0.3*t);
% hydp=-1*0.3*sin(0.3*t);
% hzdp=0.004*ones(1,length(t));

% Trayectoria de coraz�n
hxd= 14*(0.12.*sin(0.01.*t)-0.04.*sin(0.03.*t));
hyd= 14*(0.13.*cos(0.01.*t)-0.05.*cos(0.02.*t)-0.02.*cos(0.03.*t)-0.01.*cos(0.04.*t));
hzd= 0.00005.*t +0.6;

hxdp= 14*(0.12.*cos(0.01.*t)*0.03-0.04.*cos(0.03.*t)*0.03);
hydp= 14*(-0.13.*sin(0.01.*t)*0.01+0.05.*sin(0.02.*t)*0.02+0.02.*sin(0.03.*t)*0.03+0.01.*sin(0.04.*t)*0.04);
hzdp= 0.00005*ones(1,length(t));

%%%DISTANCIAS DE LOS ESLABONES
l1= 0;
l2= 0.275;
l3= 0.275;
l4= 0.2;
a=0.175;
altura=0.375;

%%%CINEMATICA DIRECTA 
hx(1) = x(1)+a*cos(th(1))+cos(q1(1)+th(1))*(l2*cos(q2(1))+l3*cos(q2(1)+q3(1))+l4*cos(q2(1)+q3(1)+q4(1)));
hy(1) = y(1)+a*sin(th(1))+sin(q1(1)+th(1))*(l2*cos(q2(1))+l3*cos(q2(1)+q3(1))+l4*cos(q2(1)+q3(1)+q4(1)));
hz(1) = ha+l2*sin(q2(1))+l3*sin(q2(1)+q3(1))+l4*sin(q2(1)+q3(1)+q4(1));

for k=1:length(t)
    tic
    %%%%ERROR DE CONTROL
    hxe(k)=hx(k)-hxd(k);
    hye(k)=hy(k)-hyd(k);
    hze(k)=hz(k)-hzd(k);
    
    he=[hxe(k) hye(k) hze(k)]';
    
    %%JACCOBIANO
    j11=cos(phi(k));
    j12=-a*sin(phi(k))-l2*cos(q2(k))*sin(q1(k)+phi(k))-l3*cos(q2(k)+q3(k))*sin(q1(k)+phi(k))-l4*sin(q1(k)+phi(k))*cos(q2(k)+q3(k)+q4(k));
    j13=-l2*cos(q2(k))*sin(q1(k)+phi(k))-l3*cos(q2(k)+q3(k))*sin(q1(k)+phi(k))-l4*sin(q1(k)+phi(k))*cos(q2(k)+q3(k)+q4(k));
    j14=-l2*cos(q1(k)+phi(k))*sin(q2(k))-l3*cos(q1(k)+phi(k))*sin(q2(k)+q3(k))-l4*cos(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
    j15=-l3*cos(q1(k)+phi(k))*sin(q2(k)+q3(k))-l4*cos(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
    j16=-l4*cos(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
   
    j21=sin(phi(k));
    j22=a*cos(phi(k))+l2*cos(q2(k))*cos(q1(k)+phi(k))+l3*cos(q2(k)+q3(k))*cos(q1(k)+phi(k))+l4*cos(q1(k)+phi(k))*cos(q2(k)+q3(k)+q4(k));
    j23=l2*cos(q2(k))*cos(q1(k)+phi(k))+l3*cos(q2(k)+q3(k))*cos(q1(k)+phi(k))+l4*cos(q1(k)+phi(k))*cos(q2(k)+q3(k)+q4(k));
    j24=-l2*sin(q1(k)+phi(k))*sin(q2(k))-l3*sin(q1(k)+phi(k))*sin(q2(k)+q3(k))-l4*sin(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
    j25=-l3*sin(q1(k)+phi(k))*sin(q2(k)+q3(k))-l4*sin(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
    j26=-l4*sin(q1(k)+phi(k))*sin(q2(k)+q3(k)+q4(k));
    
    j31=0;
    j32=0;
    j33=0;
    j34=l2*cos(q2(k))+l3*cos(q2(k)+q3(k))+l4*cos(q2(k)+q3(k)+q4(k));
    j35=l3*cos(q2(k)+q3(k))+l4*cos(q2(k)+q3(k)+q4(k));
    j36=l4*cos(q2(k)+q3(k)+q4(k));
      
    J=[j11 j12 j13 j14 j15 j16;
       j21 j22 j23 j24 j25 j26;
       j31 j32 j33 j34 j35 j36]; 
   
   %%MATRIZ DE GANANCIA
   K=[ -2 0  0; 
       0 -2  0;
       0  0 -2];
   
   hdp=[hxdp(k) hydp(k) hzdp(k)]';
   %%LEY DE CONTROL 
%  a2=J  
%  at=J'
%  b=J*at
% c=inv(b)
% % R=at*c
% qref1=at*c
% qref=pinv(J)
qref=pinv(J)*(hdp+K*tanh(he))
%l=hdp+K*tanh(he)


   u(k)=qref(1);
   w(k)=qref(2);
   q1p(k)=qref(3);
   q2p(k)=qref(4);
   q3p(k)=qref(5);
   q4p(k)=qref(6);
   
%    fprintf('u: %1d w: %1d q1: %1d q2: %1d q3: %1d q4: %1d\n',u(k),w(k),q1p(k),q2p(k),q3p(k), q4p(k))  
   %%Velocidades de la plataforma ( Robot diferencial)
   xp(k)=u(k)*cos(phi(k))-a*w(k)*sin(phi(k));
   yp(k)=u(k)*sin(phi(k))+a*w(k)*cos(phi(k));
   %%%Posiciones del la plataforma
   phi(k+1)=ts*w(k)+phi(k);
   x(k+1)=ts*xp(k)+x(k);
   y(k+1)=ts*yp(k)+y(k);
   
   %%%BRAZO
   q1(k+1)=ts*q1p(k)+q1(k);
   q2(k+1)=ts*q2p(k)+q2(k);
   q3(k+1)=ts*q3p(k)+q3(k);
   q4(k+1)=ts*q4p(k)+q4(k);
   
   %%%CINEMATICA DIRECTA 
    hx(k+1)=x(k+1)+l2*cos(q1(k+1)+phi(k+1))*cos(q2(k+1))+l3*cos(q1(k+1)+phi(k+1))*cos(q2(k+1)+q3(k+1))+l4*cos(q1(k+1)+phi(k+1))*cos(q2(k+1)+q3(k+1)+q4(k+1));
    hy(k+1)=y(k+1)+l2*sin(q1(k+1)+phi(k+1))*cos(q2(k+1))+l3*sin(q1(k+1)+phi(k+1))*cos(q2(k+1)+q3(k+1))+l4*sin(q1(k+1)+phi(k+1))*cos(q2(k+1)+q3(k+1)+q4(k+1));
    hz(k+1)=altura+l1+l2*sin(q2(k+1))+l3*sin(q2(k+1)+q3(k+1))+l4*sin(q2(k+1)+q3(k+1)+q4(k+1));
   toc
end 
%% Gr�ficas
figure
plot(t,hxe,'b'); hold on
plot(t,hye,'r'); hold on
plot(t,hze,'y'); grid
legend('Error de hxe','Error de hye','Error de hze');
xlabel('Tiempo [s]'); ylabel('[m]');
figure
plot3(hxd,hyd,hzd,'b'); hold on
plot3(hx,hy,hz,'r'); grid
% legend('Trayectria Deseada','Trayectoria Descrita');
% title('Trayectoria Deseada vs Descrita')
% xlabel('Posicion X [m]'); ylabel('Posicion Y [m]'); zlabel('Posicion Z [m]');
% title('Trayectoria Descrita');


%% ANIMACI�N 
figure
paso=30;
axis equal;
DimensionesManipulador(a,altura);
DimensionesMovil;
h1=Movil3D(x(1),y(1),phi(1)); hold on
h2=Manipulador3D(x(1),y(1),phi(1),q1(1),q2(1),q3(1),q4(1));
h3=plot3(hxd(1),hyd(1),hzd(1),'g'); hold on
h4=plot3(hx(1),hy(1),hz(1),'m'); hold on
%%
for i=1:paso:length(t)
    drawnow;
    delete(h1);
    delete(h2);
    delete(h3);
    delete(h4);
    h1=Movil3D(x(i),y(i),phi(i)); hold on
    h2=Manipulador3D(x(i),y(i),phi(i),q1(i),q2(i),q3(i),q4(i)); hold on
    h3=plot3(hxd(1:i),hyd(1:i),hzd(1:i),'g'); hold on
    h4=plot3(hx(1:i),hy(1:i),hz(1:i),'m'); hold on
end

