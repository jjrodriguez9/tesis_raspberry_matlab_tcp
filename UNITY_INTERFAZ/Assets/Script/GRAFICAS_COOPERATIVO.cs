﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MOL;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using System.Runtime.InteropServices;


public class GRAFICAS_COOPERATIVO : MonoBehaviour
{

    [SerializeField] private GameObject GR1;
    [SerializeField] private GameObject GR2;
    [SerializeField] private GameObject GB;
    [SerializeField] private GameObject GD;

    [SerializeField] private GameObject q1_eslabon;
    [SerializeField] private GameObject q2_eslabon;
    [SerializeField] private GameObject q3_eslabon;
    [SerializeField] private GameObject plataforma;

    [SerializeField] private GameObject q1_eslabon2;
    [SerializeField] private GameObject q2_eslabon2;
    [SerializeField] private GameObject q3_eslabon2;
    [SerializeField] private GameObject plataforma2;

    [SerializeField] private GameObject B;

    static int k;
    private float To;
    private float Time_sample;

    const string dllPath = "smClient64.dll";
    const string ROBOT_ONE = "ROBOT_ONE";
    const string ROBOT_TWO = "ROBOT_TWO";
    const string BARRA = "BARRA";
    const string DESEADO = "DESEADO";
    const string ENABLE = "ENABLE";
    const string ERRORES = "ERRORES";

    [DllImport(dllPath)]
    static extern int openMemory(String name, int type);

    [DllImport(dllPath)]
    static extern void setFloat(String memName, int position, float value);

    [DllImport(dllPath)]
    static extern void setInt(String memName, int position, int value);

    [DllImport(dllPath)]
    static extern void getInt(String memName, int position);

    [DllImport(dllPath)]
    static extern float getFloat(String memName, int position);

    private float[] xd = new float[9999];
    private float[] yd = new float[9999];
    private float[] zd = new float[9999];


    private float[] x = new float[9999];
    private float[] y = new float[9999];
    private float[] z = new float[9999];

    private float[] x1 = new float[9999];
    private float[] y1 = new float[9999];
    private float[] z1 = new float[9999];
    private float[] phi1 = new float[9999];

    private float[] q11 = new float[9999];
    private float[] q21 = new float[9999];
    private float[] q31 = new float[9999];

    static int de, i;
    float x2, y2, phi2;
    float q12, q22, q32;

    float hx1, hy1, hz1;
    float hx2, hy2, hz2;

    float bx, by, bz;
    float d, alpha, beta, distance;

    float u1, w1, u2, w2;

    void Start()
    {
        openMemory(ROBOT_ONE, 2);
        openMemory(ROBOT_TWO, 2);
        openMemory(BARRA, 2);
        openMemory(DESEADO, 2);
        openMemory(ERRORES, 2);
        openMemory(ENABLE, 1);

        Time_sample = 0.1f;

        k = 0;

    }

    private void Awake()
    {


        x1[k] = getFloat(ROBOT_ONE, 0);
        y1[k] = getFloat(ROBOT_ONE, 1);
        phi1[k] = getFloat(ROBOT_ONE, 2);

        q11[k] = getFloat(ROBOT_ONE, 3);
        q21[k] = getFloat(ROBOT_ONE, 4);
        q31[k] = getFloat(ROBOT_ONE, 5);
        u1 = getFloat(ROBOT_ONE, 6);
        w1 = getFloat(ROBOT_ONE, 7);

        x2 = getFloat(ROBOT_TWO, 0);
        y2 = getFloat(ROBOT_TWO, 1);
        phi2 = getFloat(ROBOT_TWO, 2);

        q12 = getFloat(ROBOT_TWO, 3);
        q22 = getFloat(ROBOT_TWO, 4);
        q32 = getFloat(ROBOT_TWO, 5);
        u2 = getFloat(ROBOT_TWO, 6);
        w2 = getFloat(ROBOT_TWO, 7);

        hx1 = getFloat(BARRA, 0);
        hy1 = getFloat(BARRA, 1);
        hz1 = getFloat(BARRA, 2);

        hx2 = getFloat(BARRA, 3);
        hy2 = getFloat(BARRA, 4);
        hz2 = getFloat(BARRA, 5);

        xd[k] = getFloat(DESEADO, 0);
        yd[k] = getFloat(DESEADO, 1);
        zd[k] = getFloat(DESEADO, 2);

        x[k] = getFloat(DESEADO, 3);
        y[k] = getFloat(DESEADO, 4);
        z[k] = getFloat(DESEADO, 5);

        bx = getFloat(DESEADO, 3);
        by = getFloat(DESEADO, 4);
        bz = getFloat(DESEADO, 5);

        alpha = getFloat(BARRA, 6);
        beta = getFloat(BARRA, 7);

        distance = getFloat(BARRA, 8);

        this.GROBOTS();
        this.BRAZO();
        this.PLATAFORMA();
        this.M_BARRA();


        To = 0;
        setInt(ENABLE, 0, 1);
    }

    void Update()
    {
        this.ANIMACION();

    }


    private void ANIMACION()
    {

        To += Time.deltaTime;

        if (To >= Time_sample)
        {

            x1[k] = getFloat(ROBOT_ONE, 0);
            y1[k] = getFloat(ROBOT_ONE, 1);
            phi1[k] = getFloat(ROBOT_ONE, 2);

            q11[k] = getFloat(ROBOT_ONE, 3);
            q21[k] = getFloat(ROBOT_ONE, 4);
            q31[k] = getFloat(ROBOT_ONE, 5);
            u1 = getFloat(ROBOT_ONE, 6);
            w1 = getFloat(ROBOT_ONE, 7);

            Setting.setting._Q11 = q11[k];
            Setting.setting._q21 = q21[k];
            Setting.setting._q31 = q31[k];
            Setting.setting._phi1 = phi1[k];
            Setting.setting._u1 = u1;
            Setting.setting._w1 = w1;



            x2 = getFloat(ROBOT_TWO, 0);
            y2 = getFloat(ROBOT_TWO, 1);
            phi2 = getFloat(ROBOT_TWO, 2);

            q12 = getFloat(ROBOT_TWO, 3);
            q22 = getFloat(ROBOT_TWO, 4);
            q32 = getFloat(ROBOT_TWO, 5);
            u2 = getFloat(ROBOT_TWO, 6);
            w2 = getFloat(ROBOT_TWO, 7);

            Setting.setting._q12 = q12;
            Setting.setting._q22 = q22;
            Setting.setting._q32 = q32;
            Setting.setting._phi2 = phi2;
            Setting.setting._u2 = u2;
            Setting.setting._w2 = w2;


            hx1 = getFloat(BARRA, 0);
            hy1 = getFloat(BARRA, 1);
            hz1 = getFloat(BARRA, 2);

            hx2 = getFloat(BARRA, 3);
            hy2 = getFloat(BARRA, 4);
            hz2 = getFloat(BARRA, 5);

            xd[k] = getFloat(DESEADO, 0);
            yd[k] = getFloat(DESEADO, 1);
            zd[k] = getFloat(DESEADO, 2);

            x[k] = getFloat(DESEADO, 3);
            y[k] = getFloat(DESEADO, 4);
            z[k] = getFloat(DESEADO, 5);

            bx = getFloat(DESEADO, 3);
            by = getFloat(DESEADO, 4);
            bz = getFloat(DESEADO, 5);

            alpha = getFloat(BARRA, 6);
            beta = getFloat(BARRA, 7);


            Setting.setting._hxe = getFloat(ERRORES, 0);
            Setting.setting._hye = getFloat(ERRORES, 1);
            Setting.setting._hze = getFloat(ERRORES, 2);

            Setting.setting._de = getFloat(ERRORES, 3);
            Setting.setting._alphae = getFloat(ERRORES, 4);
            Setting.setting._betae = getFloat(ERRORES, 5);

            distance = Setting.setting._distance = getFloat(BARRA, 8);

            this.GROBOTS();
            this.BRAZO();
            this.PLATAFORMA();

            this.M_BARRA();
            k += 1;
            To = 0;
        }
    }

    private void GROBOTS()
    {
        GR1.transform.position = new Vector3(-hy2, hz2, hx2);
        GR2.transform.position = new Vector3(-hy1, hz1, hx1);
        GB.transform.position = new Vector3(-y[k], z[k], x[k]);
        GD.transform.position = new Vector3(-yd[k], zd[k], xd[k]);
    }

    private void BRAZO()
    {

        q1_eslabon.transform.localRotation = Quaternion.Euler(0f, ((-q11[k] / 1) * Mathf.Rad2Deg), 0f); // Sentido por ley de la mano derecha
        q2_eslabon.transform.localRotation = Quaternion.Euler(((q21[k] / 1) * Mathf.Rad2Deg), 0f, 0f);
        q3_eslabon.transform.localRotation = Quaternion.Euler(((q31[k] / 1) * Mathf.Rad2Deg), 0f, 90f);

        q1_eslabon2.transform.localEulerAngles = new Vector3(0f, ((-q12 / 1) * Mathf.Rad2Deg), 0f); // Sentido por ley de la mano derecha
        q2_eslabon2.transform.localEulerAngles = new Vector3(((q22 / 1) * Mathf.Rad2Deg), 0f, 0f);
        q3_eslabon2.transform.localEulerAngles = new Vector3(((q32 / 1) * Mathf.Rad2Deg), 0f, 90f);

    }

    private void PLATAFORMA()
    {
        //float aux = y[k];
        //  Debug.Log(x1[k]);
        plataforma.transform.localPosition = new Vector3(-y1[k], 0f, x1[k]);
        plataforma.transform.localRotation = Quaternion.Euler(0f, (-phi1[k] / 1) * Mathf.Rad2Deg, 0f);

        plataforma2.transform.localPosition = new Vector3(-y2, 0f, x2);
        plataforma2.transform.localEulerAngles = new Vector3(0f, (-phi2 / 1) * Mathf.Rad2Deg, 0f);

    }
    private void M_BARRA()
    {
        B.transform.localScale = new Vector3(distance / 5f, 1f, 1f);

        B.transform.localPosition = new Vector3(-by, bz, bx);

        B.transform.localEulerAngles = new Vector3(0f, 90 - ((alpha) * Mathf.Rad2Deg), (beta) * Mathf.Rad2Deg);

    }





    void OnApplicationQuit()
    {

        setInt(ENABLE, 0, 0);

    }
}

