﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MOL;
using TMPro;
using UnityEngine.SceneManagement;

public class Trayectoria : MonoBehaviour
{
    public GameObject linerenderer;
    public GameObject pointer;
    public GameObject pointerBlue;
    public GameObject pointerGreen;
    public GameObject pointerRed;
    public GameObject pointerMagenta;
    public GameObject HolderPrefb;
    public GameObject holder;
    public GameObject xLineNumber;
    public Material bluemat;
    public Material greenmat;
    public Material redmat;
    public Material magentamat;
    public Text topValue;
    public GameObject graf_trayectoria_deseada;

    public List<GraphData> graphDataPlayer1 = new List<GraphData>();
    public List<GraphData> graphDataPlayer2 = new List<GraphData>();
    public List<GraphData> graphDataPlayer3 = new List<GraphData>();
    public List<GraphData> graphDataPlayer4 = new List<GraphData>();

    private GraphData gd;
    private float highestValue = 56;
    public Transform origin;
    public TextMesh player1name;
    public TextMesh player2name;
    public TextMesh player3name;

    private float lrWidth = 0.1f;
    private int dataGap = 0;
    private float[] dat1;
    private float[] dat2;
    private float[] dat3;
    private float[] dat4;

    /// <summary>
    public float tfin = 6f; //6
    float ts = 0.1f;
    private float[] t;
    private float[] u = new float[9999];
    private float[] w = new float[9999];
    private float[] q1 = new float[9999];
    private float[] q2 = new float[9999];
    private float[] q3 = new float[9999];
    private float[] q4 = new float[9999];

    private float[] hx = new float[9999];
    private float[] hy = new float[9999];
    private float[] hz = new float[9999];

    private float[] th = new float[9999];

    private float[] x = new float[9999];
    private float[] y = new float[9999];

    private float[] xp = new float[9999];
    private float[] yp = new float[9999];

    private float[,] qpref = new float[6, 1];
    private double[,] re = new double[3, 1];
    private float[,] red = new float[3, 1];

    float l1, l2, l3, l4, a, altura;

    private float[] hxe = new float[9999];
    private float[] hye = new float[9999];
    private float[] hze = new float[9999];
    private float[,] error = new float[3, 1];

    double j11, j12, j13, j14, j15, j16, j21, j22, j23, j24, j25, j26, j31, j32, j33, j34, j35, j36;

    private float[] q1p = new float[9999];
    private float[] q2p = new float[9999];
    private float[] q3p = new float[9999];
    private float[] q4p = new float[9999];

    private float[] hxd = new float[9999];
    private float[] hyd = new float[9999];
    private float[] hzd = new float[9999];

    private float[] hxd_p = new float[9999];
    private float[] hyd_p = new float[9999];
    private float[] hzd_p = new float[9999];
    public int k;

    /******************************************/
    public Transform carro, q1T, q2T, q3T, q4T,cuboDeseado,cuboReal;
    public int m = 0;
    public int valork;
    public float pausa;
    public float corrX, corrZ;
    public float corrXD,corrYD,corrZD;
    public float corrXR, corrYR, corrZR;
    Vector3 startPosition;
    public Animator ruedas;
    /******************************************/
    public TextMeshProUGUI xTxt, yTxt, thTxt, q1Txt, q2Txt, q3Txt, q4Txt;
    public GameObject etiqueta1, etiqueta2, etiqueta3,etiqueta4;

    public void Dibujar_Trayectoria()
    {
        t = new float[((int)tfin * 10) + 1];
        float tnew;
        //Condiciones Iniciales
        q1[0] = (0f * Mathf.PI) / 180f;
        q2[0] = (45f * Mathf.PI) / 180f;
        q3[0] = (-30f * Mathf.PI) / 180f;
        q4[0] = (-30f * Mathf.PI) / 180f;

        th[0] = (-30f * Mathf.PI) / 180f;
        x[0] = 0.5f;
        y[0] = 1f;

        dat1 = new float[t.Length];
        dat2 = new float[t.Length];
        dat3 = new float[t.Length];
        dat4 = new float[t.Length];
        //Eslabones
        l1 = 0f;
        l2 = 0.275f;
        l3 = 0.275f;
        l4 = 0.2f;
        a = 0.175f;
        altura = 0.375f;

        //Cinematica Directa
        hx[0] = x[0] + a * Mathf.Cos(th[0]) + l2 * Mathf.Cos(q2[0]) * Mathf.Cos(q1[0] + th[0]) + l3 * Mathf.Cos(q2[0] + q3[0]) * Mathf.Cos(q1[0] + th[0]) + l4 * Mathf.Cos(q2[0] + q3[0] + q4[0]) * Mathf.Cos(q1[0] + th[0]);
        hy[0] = y[0] + a * Mathf.Sin(th[0]) + l2 * Mathf.Cos(q2[0]) * Mathf.Sin(q1[0] + th[0]) + l3 * Mathf.Cos(q2[0] + q3[0]) * Mathf.Sin(q1[0] + th[0]) + l4 * Mathf.Cos(q2[0] + q3[0] + q4[0]) * Mathf.Sin(q1[0] + th[0]); ;
        hz[0] = altura + l1 + l2 * Mathf.Sin(q2[0]) + l3 * Mathf.Sin(q2[0] + q3[0]) + l4 * Mathf.Sin(q2[0] + q3[0] + q4[0]);

        for (int k = 0; k < t.Length; k++)
        {
            // tiempo nuevo en 0.1
            tnew = k / 1f; // 10f
            // *********** DEFINICIÓN DE TRAYECTORIA ***********
            
            // TRAYECTORIA DE CORAZON

            float hxd_n = 20 * (0.12f* Mathf.Sin(0.01f * tnew) - 0.04f * Mathf.Sin(0.03f* tnew));
            float hyd_n = 20* (0.13f* Mathf.Cos(0.01f * tnew) - 0.05f * Mathf.Cos(0.02f* tnew) - 0.02f * Mathf.Cos(0.03f* tnew) - 0.01f * Mathf.Cos(0.04f* tnew));
            float hzd_n = 0.000005f * tnew + 0.6f;

            float hxd_p_n = 20 * (0.12f* Mathf.Cos(0.01f * tnew) * 0.03f - 0.04f * Mathf.Cos(0.03f * tnew) * 0.03f);
            float hyd_p_n = 20 * (-0.13f* Mathf.Sin(0.01f* tnew) * 0.01f + 0.05f * Mathf.Sin(0.02f * tnew) * 0.02f + 0.02f * Mathf.Sin(0.03f* tnew) * 0.03f + 0.01f* Mathf.Sin(0.04f * tnew) * 0.04f);
            float hzd_p_n = 0.000005f;
            
            //Trayectoria Espiral
            //float hxd_n = 1.5f * Mathf.Sin(0.3f * tnew);
            //float hyd_n = 1.5f * Mathf.Cos(0.3f * tnew);
            //float hzd_n = 0.3f + 0.01f * tnew;

            //float hxd_p_n = 1.5f * 0.3f * Mathf.Cos(0.3f * tnew);  // derivada de x deseado
            //float hyd_p_n = -1.5f * 0.3f * Mathf.Sin(0.3f * tnew); // derivada de y deseado
            //float hzd_p_n = 0.01f;                            // derivada de y deseado

            hxd[k] = hxd_n;
            hyd[k] = hyd_n;
            hzd[k] = hzd_n;

            hxd_p[k] = hxd_p_n;                                
            hyd_p[k] = hyd_p_n;                               
            hzd_p[k] = hzd_p_n;

            //Errores
            hxe[k] = hx[k] - hxd[k-1];
            hye[k] = hy[k] - hyd[k-1];
            hze[k] = hz[k] - hzd[k-1];

            float[,] error = new float[3, 1] { { hxe[k] }, { hye[k] }, { hze[k] } };

            // Jacobina
            j11 = Mathf.Cos(th[k]);
            j12 = -a * Mathf.Sin(th[k]) - l2 * Mathf.Cos(q2[k]) * Mathf.Sin(q1[k] + th[k]) - l3 * Mathf.Cos(q2[k] + q3[k]) * Mathf.Sin(q1[k] + th[k]) - l4 * Mathf.Sin(q1[k] + th[k]) * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j13 = -l2 * Mathf.Cos(q2[k]) * Mathf.Sin(q1[k] + th[k]) - l3 * Mathf.Cos(q2[k] + q3[k]) * Mathf.Sin(q1[k] + th[k]) - l4 * Mathf.Sin(q1[k] + th[k]) * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j14 = -l2 * Mathf.Sin(q2[k]) * Mathf.Cos(q1[k] + th[k]) - l3 * Mathf.Sin(q2[k] + q3[k]) * Mathf.Cos(q1[k] + th[k]) - l4 * Mathf.Cos(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);
            j15 = -l3 * Mathf.Sin(q2[k] + q3[k]) * Mathf.Cos(q1[k] + th[k]) - l4 * Mathf.Cos(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);
            j16 = -l4 * Mathf.Cos(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);


            j21 = Mathf.Sin(th[k]);
            j22 = a * Mathf.Cos(th[k]) + l2 * Mathf.Cos(q2[k]) * Mathf.Cos(q1[k] + th[k]) + l3 * Mathf.Cos(q2[k] + q3[k]) * Mathf.Cos(q1[k] + th[k]) + l4 * Mathf.Cos(q1[k] + th[k]) * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j23 = l2 * Mathf.Cos(q2[k]) * Mathf.Cos(q1[k] + th[k]) + l3 * Mathf.Cos(q2[k] + q3[k]) * Mathf.Cos(q1[k] + th[k]) + l4 * Mathf.Cos(q1[k] + th[k]) * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j24 = -l2 * Mathf.Sin(q2[k]) * Mathf.Sin(q1[k] + th[k]) - l3 * Mathf.Sin(q2[k] + q3[k]) * Mathf.Sin(q1[k] + th[k]) - l4 * Mathf.Sin(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);
            j25 = -l3 * Mathf.Sin(q2[k] + q3[k]) * Mathf.Sin(q1[k] + th[k]) - l4 * Mathf.Sin(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);
            j26 = -l4 * Mathf.Sin(q1[k] + th[k]) * Mathf.Sin(q2[k] + q3[k] + q4[k]);

            j31 = 0f;
            j32 = 0f;
            j33 = 0f;
            j34 = l2 * Mathf.Cos(q2[k]) + l3 * Mathf.Cos(q2[k] + q3[k]) + l4 * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j35 = l3 * Mathf.Cos(q2[k] + q3[k]) + l4 * Mathf.Cos(q2[k] + q3[k] + q4[k]);
            j36 = l4 * Mathf.Cos(q2[k] + q3[k] + q4[k]);


            double[,] J = new double[3, 6] {    {j11, j12, j13, j14, j15, j16 },
                                                { j21, j22, j23, j24, j25, j26 },
                                                { j31, j32, j33, j34, j35, j36} };

            Matrix Mat = new Matrix(J);
            //Debug.LogFormat("J(3, 6) = \n{0}", Mat);

            //Transpuesta de J
            Matrix MatT = Mat.Transposition();

            string str = "";
            str += "Mat = \n";
            str += Mat.ToString();
            str += "\n";
            str += "Mat T (Transposed) = \n";
            str += MatT.ToString();

            //bDebug.LogFormat("{0}", str);
            //Debug.LogFormat("trasnp = \n{0}", MatT);

            //Multiplicar Matrices(J*Jtrsn)
            Matrix ansa = Mat * MatT;
            //Debug.LogFormat("Mat(3, 6) * MatT(6, 3)  = \n{0}", ansa);

            // Inversa ans
            Matrix inversa = ansa.Inverse();
            //Debug.LogFormat("Inverse of Matrix: \n{0}", inversa.ToString());

            // inversa*trasnp
            Matrix psedudo = MatT * inversa;
            //Debug.LogFormat("MatT(6, 3) * inversa(3, 3)  = \n{0}", psedudo);

            Matrix total = MatT * ((Mat * MatT).Inverse());
            //Debug.LogFormat("TOTAL = \n{0}", total);

            //Matriz de Ganancia*/
            double[,] K_ = new double[3, 3] { { -2f, 0f, 0f }, { 0f, -2f, 0f }, { 0f, 0f, -2f } };
            //Matrix G = new Matrix[K_];

            //Ley de Control  
            double a1 = (Mathf.Exp(hxe[k]) - Mathf.Exp(-hxe[k])) / (Mathf.Exp(hxe[k]) + Mathf.Exp(-hxe[k]));
            double b1 = (Mathf.Exp(hye[k]) - Mathf.Exp(-hye[k])) / (Mathf.Exp(hye[k]) + Mathf.Exp(-hye[k]));
            double c1 = (Mathf.Exp(hze[k]) - Mathf.Exp(-hze[k])) / (Mathf.Exp(hze[k]) + Mathf.Exp(-hze[k]));
            //double[,] error1 = new double[3, 1] { { a1 }, { b1 }, { c1 } };
            double[,] error1 = new double[3, 1] { { hxe[k] }, { hye[k] }, { hze[k] } };
            Matrix e = new Matrix(error1);

            //Matrix re = G * e;
            //Debug.LogFormat("K(3, 3) * ERROR1(3, 1)  = \n{0}", re);

            double[,] V = new double[3, 1] { { hxd_p[k] }, { hyd_p[k] }, { hzd_p[k] } };
            Matrix hdp = new Matrix(V);
            
            //Matrix red = hdp + re;
            // Debug.LogFormat("hPUNTO = \n{0}", red);

            //Matrix qpref = psedudo * (hdp + G*e);
            // Debug.LogFormat("qref = \n{0}", qpref);

            u[k] = (float)qpref[0, 0];
            w[k] = (float)qpref[1, 0];
            q1p[k] = (float)qpref[2, 0];
            q2p[k] = (float)qpref[3, 0];
            q3p[k] = (float)qpref[4, 0];
            q4p[k] = (float)qpref[5, 0];
            //print("u: " + u[k] + "w: " + w[k] + "q1p: " + q1p[k] + "q2p: " + q2p[k] + "q3p: " + q3p[k] + "q4p: " + q4p[k]);

            //Integrar a traves de Euler
            // ROBOT
            xp[k] = u[k] * Mathf.Cos(th[k]) - a * w[k] * Mathf.Sin(th[k]);
            yp[k] = u[k] * Mathf.Sin(th[k]) + a * w[k] * Mathf.Cos(th[k]);

            //PLATAFORMA
            th[k + 1] = ts * w[k] + th[k];
            x[k + 1] = ts * xp[k] + x[k];
            y[k + 1] = ts * yp[k] + y[k];

            // BRAZO
            q1[k + 1] = ts * q1p[k] + q1[k];
            q2[k + 1] = ts * q2p[k] + q2[k];
            q3[k + 1] = ts * q3p[k] + q3[k];
            q4[k + 1] = ts * q4p[k] + q4[k];
            //print("P: "  u[k + 1] + w[k + 1] + q1[k + 1] + " S: " + q2[k + 1] + " T: " + q3[k + 1]+ q4[k + 1]);

            //print("q1: " + q1[k]  + " q2: " + q2[k] + " q3: " + q3[k ] + " q4: " + q4[k ]);

            //Cinematica Directa
            hx[k + 1] = x[k + 1] + l2 * Mathf.Cos(q2[k + 1]) * Mathf.Cos(q1[k + 1] + th[k + 1]) + l3 * Mathf.Cos(q2[k + 1] + q3[k + 1]) * Mathf.Cos(q1[k + 1] + th[k + 1]) + l4 * Mathf.Cos(q2[k + 1] + q3[k + 1] + q4[k + 1]) * Mathf.Cos(q1[k + 1] + th[k + 1]);
            hy[k + 1] = y[k + 1] + l2 * Mathf.Cos(q2[k + 1]) * Mathf.Sin(q1[k + 1] + th[k + 1]) + l3 * Mathf.Cos(q2[k + 1] + q3[k + 1]) * Mathf.Sin(q1[k + 1] + th[k + 1]) + l4 * Mathf.Cos(q2[k + 1] + q3[k + 1] + q4[k + 1]) * Mathf.Sin(q1[k + 1] + th[k + 1]); ;
            hz[k + 1] = altura + l1 + l2 * Mathf.Sin(q2[k + 1]) + l3 * Mathf.Sin(q2[k + 1] + q3[k + 1]) + l4 * Mathf.Sin(q2[k + 1] + q3[k + 1] + q4[k + 1]);
            Grafico__trayectoria_deseada();

        }
                
        /////////////////
        //for (int i = 0; i < dat1.Length; i++)
        //{
        //    GraphData gd = new GraphData();
        //    gd.marbles = dat1[i];
        //    graphDataPlayer1.Add(gd);
        //}
        //for (int i = 0; i < dat2.Length; i++)
        //{
        //    GraphData gd2 = new GraphData();
        //    gd2.marbles = dat2[i];
        //    graphDataPlayer2.Add(gd2);
        //}
        //for (int i = 0; i < dat3.Length; i++)
        //{
        //    GraphData gd3 = new GraphData();
        //    gd3.marbles = dat3[i];
        //    graphDataPlayer3.Add(gd3);
        //}
        //// showing graph
        //ShowGraph();
        ////////
        //caso = 2;
        StartCoroutine(Mover_Deseado());
    }


    private void Grafico__trayectoria_deseada()
    {
        graf_trayectoria_deseada.transform.position = new Vector3(-hyd[k], hzd[k], hxd[k]); // Sentido por ley de la mano derecha
        if (!graf_trayectoria_deseada.activeInHierarchy)
        {
            graf_trayectoria_deseada.SetActive(true);
        }
    }

    IEnumerator Mover_Deseado()
    {
        for (int kk = 1; kk < t.Length; kk++)
        {
            cuboDeseado.transform.localPosition = new Vector3(hxd[kk] + corrXD, hzd[kk] + corrYD, hyd[kk] + corrZD) ;
            valork = kk;
            yield return new WaitForSeconds(0f);
        }
        StartCoroutine(Mover_Manipulador());
    }

    IEnumerator Mover_Manipulador()
    {

        for (int kk = 1; kk < t.Length; kk++)
        {
            ruedas.StopPlayback();
            q1T.transform.localEulerAngles = new Vector3(0f, ((-q1[kk] / 1)  * Mathf.Rad2Deg) - 180, 0f);
            q2T.transform.localEulerAngles = new Vector3(((-q2[kk] / 1)  * Mathf.Rad2Deg) + 90, 0f, 0f);
            q3T.transform.localEulerAngles = new Vector3(((-q3[kk] / 1)  * Mathf.Rad2Deg) + 90, 0f, 90f);
            q4T.transform.localEulerAngles = new Vector3(((-q4[kk] / 1)  * Mathf.Rad2Deg) - 180, 0f, 0f);

            carro.transform.localPosition = new Vector3(x[kk]  + corrXR, 0f, y[kk]  + corrZR);
            cuboReal.transform.localPosition = new Vector3(hx[kk] + corrXR, hz[kk] + corrYR, hy[kk] + corrZR);
            carro.transform.localEulerAngles = new  Vector3(0f,  (-th[kk] / 1) * Mathf.Rad2Deg + 90, 0f);

            xTxt.text = "X= " + x[kk].ToString("F4") + " m";
            yTxt.text = "Y= " + y[kk].ToString("F4") + " m";
            thTxt.text = "th= " + th[kk].ToString("F4") + " rad/s";
            q1Txt.text = "Q1= " + q1[kk].ToString("F4") + " rad/s";
            q2Txt.text = "Q2= " + q2[kk].ToString("F4") + " rad/s";
            q3Txt.text = "Q3= " + q3[kk].ToString("F4") + " rad/s";
            q4Txt.text = "Q4= " + q4[kk].ToString("F4") + " rad/s";


            //xTxt.text = "X= " + carro.transform.position.x.ToString("F2") + " m";
            //yTxt.text = "Y= " + carro.transform.position.z.ToString("F2") + " m";         
            //thTxt.text = "th= " + carro.transform.localEulerAngles.y.ToString("F2");
            //q1Txt.text = "Q1= " + q1T.transform.localEulerAngles.y.ToString("F2");
            //q2Txt.text = "Q2= " + q2T.transform.localEulerAngles.x.ToString("F2");
            //q3Txt.text = "Q3= " + q3T.transform.localEulerAngles.x.ToString("F2");
            //q4Txt.text = "Q4= " + q4T.transform.localEulerAngles.x.ToString("F2");

            //print("q1: " + q1[kk] + " q2: " + q2[kk] + " q3: " + q3[kk] + " q4: " + q4[kk]);

            valork = kk;
            yield return new WaitForSeconds(pausa);
        }
        //StopAllCoroutines();
        ruedas.StartPlayback();
        //caso = 3;
    }


    public void Borrar()
    {
        Application.LoadLevel(Application.loadedLevel);
        //SceneManager.LoadScene("Trayectoria_");

    }

    void Start()
    {
        startPosition = carro.transform.position;
        ruedas.StartPlayback();

        corrX = carro.transform.position.x;
        corrZ = carro.transform.position.z;

        corrXD = cuboDeseado.transform.position.x;
        corrYD = cuboDeseado.transform.position.y;
        corrZD = cuboDeseado.transform.position.z;

        corrXR = cuboReal.transform.position.x;
        corrYR = cuboReal.transform.position.y;
        corrZR = cuboReal.transform.position.z;

        etiqueta1.SetActive(false);
        etiqueta2.SetActive(false);
        etiqueta3.SetActive(false);

        Dibujar_Trayectoria();
    }
    public void Boton_Animar()
    {
        Dibujar_Trayectoria();
    }

    public void Dibujar_xyz()
    {
        etiqueta1.SetActive(true);
        for (int k = 0; k < t.Length; k++)
        {
            dat1[k] = (float)(29f + hxe[k] * 30f);
            dat2[k] = (float)(29f + hye[k] * 30f);
            dat3[k] = (float)(29f + hze[k] * 30f);
            dat4[k] = (float)(29f -20 * 30f);

        }
        ///////////////
        for (int i = 0; i < dat1.Length; i++)
        {
            GraphData gd = new GraphData();
            gd.marbles = dat1[i];
            graphDataPlayer1.Add(gd);
        }
        for (int i = 0; i < dat2.Length; i++)
        {
            GraphData gd2 = new GraphData();
            gd2.marbles = dat2[i];
            graphDataPlayer2.Add(gd2);
        }
        for (int i = 0; i < dat3.Length; i++)
        {
            GraphData gd3 = new GraphData();
            gd3.marbles = dat3[i];
            graphDataPlayer3.Add(gd3);
        }
        for (int i = 0; i < dat4.Length; i++)
        {
            GraphData gd4 = new GraphData();
            gd4.marbles = dat4[i];
            graphDataPlayer4.Add(gd4);
        }
        // showing graph
        ShowGraph();
    }

    public void Dibujar_u()
    {
        etiqueta2.SetActive(true);
        for (int k = 0; k < t.Length; k++)
        {
            dat1[k] = (float)(29f + u[k] * 30f);
            dat2[k] = (float)(29f -20 * 30f);
            dat3[k] = (float)(29f -20 * 30f);
            dat4[k] = (float)(29f -20 * 30f);

        }
        ///////////////
        for (int i = 0; i < dat1.Length; i++)
        {
            GraphData gd = new GraphData();
            gd.marbles = dat1[i];
            graphDataPlayer1.Add(gd);
        }
        for (int i = 0; i < dat2.Length; i++)
        {
            GraphData gd2 = new GraphData();
            gd2.marbles = dat2[i];
            graphDataPlayer2.Add(gd2);
        }
        for (int i = 0; i < dat3.Length; i++)
        {
            GraphData gd3 = new GraphData();
            gd3.marbles = dat3[i];
            graphDataPlayer3.Add(gd3);
        }
        for (int i = 0; i < dat4.Length; i++)
        {
            GraphData gd4 = new GraphData();
            gd4.marbles = dat4[i];
            graphDataPlayer4.Add(gd4);
        }
        // showing graph
        ShowGraph();
    }
    public void Dibujar_w()
    {
        etiqueta3.SetActive(true);
        for (int k = 0; k < t.Length; k++)
        {
            dat1[k] = (float)(29f - 20 * 30f);
            dat2[k] = (float)(29f + w[k] * 30f);
            dat3[k] = (float)(29f - 20 * 30f);
            dat4[k] = (float)(29f - 20 * 30f);

        }
        ///////////////
        for (int i = 0; i < dat1.Length; i++)
        {
            GraphData gd = new GraphData();
            gd.marbles = dat1[i];
            graphDataPlayer1.Add(gd);
        }
        for (int i = 0; i < dat2.Length; i++)
        {
            GraphData gd2 = new GraphData();
            gd2.marbles = dat2[i];
            graphDataPlayer2.Add(gd2);
        }
        for (int i = 0; i < dat3.Length; i++)
        {
            GraphData gd3 = new GraphData();
            gd3.marbles = dat3[i];
            graphDataPlayer3.Add(gd3);
        }
        for (int i = 0; i < dat4.Length; i++)
        {
            GraphData gd4 = new GraphData();
            gd4.marbles = dat4[i];
            graphDataPlayer4.Add(gd4);
        }
        // showing graph
        ShowGraph();
    }

    public void Dibujar_q1234()
    {
        etiqueta4.SetActive(true);
        for (int k = 0; k < t.Length; k++)
        {
            dat1[k] = (float)(29f + q1p[k] * 30f);
            dat2[k] = (float)(29f + q2p[k] * 30f);
            dat3[k] = (float)(29f + q3p[k] * 30f);
            dat4[k] = (float)(29f + q4p[k] * 30f);

        }
        ///////////////
        for (int i = 0; i < dat1.Length; i++)
        {
            GraphData gd = new GraphData();
            gd.marbles = dat1[i];
            graphDataPlayer1.Add(gd);
        }
        for (int i = 0; i < dat2.Length; i++)
        {
            GraphData gd2 = new GraphData();
            gd2.marbles = dat2[i];
            graphDataPlayer2.Add(gd2);
        }
        for (int i = 0; i < dat3.Length; i++)
        {
            GraphData gd3 = new GraphData();
            gd3.marbles = dat3[i];
            graphDataPlayer3.Add(gd3);
        }
        for (int i = 0; i < dat4.Length; i++)
        {
            GraphData gd4 = new GraphData();
            gd4.marbles = dat4[i];
            graphDataPlayer4.Add(gd4);
        }
        // showing graph
        ShowGraph();
    }
    //public void Update()
    //{
    //    if (Input.GetKeyDown(KeyCode.N))
    //    {
    //        m++;
    //    }
    //    if (m == 1)
    //    {
    //        switch (caso)
    //        {
    //            case 1:
    //                Dibujar_Trayectoria();
    //                break;
    //            case 2:
    //                //StopCoroutine("Mover_Manipulador");
    //                StartCoroutine(Mover_Manipulador());
    //                break;
    //            case 3:
    //                //StopAllCoroutines();
    //                //StopCoroutine("Mover_Manipulador");
    //                break;
    //        }
    //    }
    //    if (m == 2)
    //    {
    //        m = 0;
    //        carro.transform.position = startPosition;
    //        caso = 1;
    //        valork = 0;

    //        StopAllCoroutines();
    //        //Destroy(holder);
    //        //ClearGraph();
    //        //Application.LoadLevel(Application.loadedLevel);
    //    }
    //}

    public void ShowData(GraphData[] gdlist, int playerNum, float gap)
    {

        // Adjusting value to fit in graph
        for (int i = 0; i < gdlist.Length; i++)
        {
            // since Y axis is from 0 to 7 we are dividing the marbles with the highestValue
            // so that we get a value less than or equals to 1 and than we can multiply that
            // number with Y axis range to fit in graph. 
            // e.g. marbles = 90, highest = 90 so 90/90 = 1 and than 1*7 = 7 so for 90, Y = 7

            gdlist[i].marbles = (gdlist[i].marbles / highestValue) * 7;
        }
        if (playerNum == 1)
            StartCoroutine(BarGraphBlue(gdlist, gap));
        if (playerNum == 2)
            StartCoroutine(BarGraphGreen(gdlist, gap));
        if (playerNum == 3)
            StartCoroutine(BarGraphRed(gdlist, gap));
        else if (playerNum == 4)
            StartCoroutine(BarGraphMagenta(gdlist, gap));
    }

    public void AddPlayer1Data(int numOfStones)
    {
        GraphData gd = new GraphData();
        gd.marbles = numOfStones;
        graphDataPlayer1.Add(gd);
    }
    public void AddPlayer2Data(int numOfStones)
    {
        GraphData gd = new GraphData();
        gd.marbles = numOfStones;
        graphDataPlayer2.Add(gd);
    }
    public void AddPlayer3Data(int numOfStones)
    {
        GraphData gd = new GraphData();
        gd.marbles = numOfStones;
        graphDataPlayer3.Add(gd);
    }
    public void AddPlayer4Data(int numOfStones)
    {
        GraphData gd = new GraphData();
        gd.marbles = numOfStones;
        graphDataPlayer4.Add(gd);
    }

    public void ShowGraph()
    {

        ClearGraph();

        if (graphDataPlayer1.Count >= 1 && graphDataPlayer2.Count >= 1 && graphDataPlayer3.Count >= 1 && graphDataPlayer4.Count >= 1)
        {
            holder = Instantiate(HolderPrefb, Vector3.zero, Quaternion.identity) as GameObject;
            holder.name = "h3";

            GraphData[] gd1 = new GraphData[graphDataPlayer1.Count];
            GraphData[] gd2 = new GraphData[graphDataPlayer2.Count];
            GraphData[] gd3 = new GraphData[graphDataPlayer3.Count];
            GraphData[] gd4 = new GraphData[graphDataPlayer4.Count];
            for (int i = 0; i < graphDataPlayer1.Count; i++)
            {
                GraphData gd = new GraphData();
                gd.marbles = graphDataPlayer1[i].marbles;
                gd1[i] = gd;
            }
            for (int i = 0; i < graphDataPlayer2.Count; i++)
            {
                GraphData gd = new GraphData();
                gd.marbles = graphDataPlayer2[i].marbles;
                gd2[i] = gd;
            }
            for (int i = 0; i < graphDataPlayer3.Count; i++)
            {
                GraphData gd = new GraphData();
                gd.marbles = graphDataPlayer3[i].marbles;
                gd3[i] = gd;
            }
            for (int i = 0; i < graphDataPlayer4.Count; i++)
            {
                GraphData gd = new GraphData();
                gd.marbles = graphDataPlayer4[i].marbles;
                gd4[i] = gd;
            }

            dataGap = GetDataGap(graphDataPlayer3.Count);


            int dataCount = 0;
            int gapLength = 1;
            float gap = 1.0f;
            bool flag = false;

            while (dataCount < graphDataPlayer3.Count)
            {
                if (dataGap > 1)
                {

                    if ((dataCount + dataGap) == graphDataPlayer3.Count)
                    {

                        dataCount += dataGap - 1;
                        flag = true;
                    }
                    else if ((dataCount + dataGap) > graphDataPlayer3.Count && !flag)
                    {

                        dataCount = graphDataPlayer3.Count - 1;
                        flag = true;
                    }
                    else
                    {
                        dataCount += dataGap;
                        if (dataCount == (graphDataPlayer3.Count - 1))
                            flag = true;
                    }
                }
                else
                    dataCount += dataGap;

                gapLength++;
            }

            if (graphDataPlayer3.Count > 13)
            {
                if (graphDataPlayer3.Count < 40)
                    gap = 13.0f / graphDataPlayer3.Count;
                else if (graphDataPlayer3.Count >= 40)
                {
                    gap = 13.0f / gapLength;
                }
            }

            ShowData(gd1, 1, gap);
            ShowData(gd2, 2, gap);
            ShowData(gd3, 3, gap);
            ShowData(gd4, 4, gap);
        }
    }

    public void ClearGraph()
    {
        if (holder)
            Destroy(holder);
    }

    int GetDataGap(int dataCount)
    {
        int value = 1;
        int num = 0;
        while ((dataCount - (40 + num)) >= 0)
        {
            value += 1;
            num += 150;
        }
        return value;
    }


    IEnumerator BarGraphBlue(GraphData[] gd, float gap)
    {
        float xIncrement = gap;
        int dataCount = 0;
        bool flag = false;
        Vector3 startpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));//origin.position;//

        while (dataCount < gd.Length)
        {

            Vector3 endpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
            startpoint = new Vector3(startpoint.x, startpoint.y, origin.position.z);
            // pointer is an empty gameObject, i made a prefab of it and attach it in the inspector
            GameObject p = Instantiate(pointer, new Vector3(startpoint.x, startpoint.y, origin.position.z), Quaternion.identity) as GameObject;
            p.transform.parent = holder.transform;


            GameObject lineNumber = Instantiate(xLineNumber, new Vector3(origin.position.x + xIncrement, origin.position.y - 0.18f, origin.position.z), Quaternion.identity) as GameObject;
            lineNumber.transform.parent = holder.transform;
            lineNumber.GetComponent<TextMesh>().text = (dataCount + 1).ToString();


            // linerenderer is an empty gameObject with Line Renderer Component Attach to it, 
            // i made a prefab of it and attach it in the inspector
            GameObject lineObj = Instantiate(linerenderer, startpoint, Quaternion.identity) as GameObject;
            lineObj.transform.parent = holder.transform;
            lineObj.name = dataCount.ToString();

            LineRenderer lineRenderer = lineObj.GetComponent<LineRenderer>();

            lineRenderer.material = bluemat;
            lineRenderer.SetWidth(lrWidth, lrWidth);
            lineRenderer.SetVertexCount(3);


            //while (Vector3.Distance(p.transform.position, endpoint) > 0.2f)
            //{
            //    float step = 5 * Time.deltaTime;
            //    p.transform.position = Vector3.MoveTowards(p.transform.position, endpoint, step);
            //    lineRenderer.SetPosition(0, startpoint);
            //    lineRenderer.SetPosition(1, p.transform.position);

            //    yield return null;
            //}

            lineRenderer.SetPosition(0, startpoint);
            lineRenderer.SetPosition(1, endpoint);


            p.transform.position = endpoint;
            GameObject pointered = Instantiate(pointerBlue, endpoint, pointerBlue.transform.rotation) as GameObject;
            pointered.transform.parent = holder.transform;
            startpoint = endpoint;

            if (dataGap > 1)
            {
                if ((dataCount + dataGap) == gd.Length)
                {
                    dataCount += dataGap - 1;
                    flag = true;
                }
                else if ((dataCount + dataGap) > gd.Length && !flag)
                {
                    dataCount = gd.Length - 1;
                    flag = true;
                }
                else
                {
                    dataCount += dataGap;
                    if (dataCount == (gd.Length - 1))
                        flag = true;
                }
            }
            else
                dataCount += dataGap;

            xIncrement += gap;

            yield return null;

        }
    }

    IEnumerator BarGraphGreen(GraphData[] gd, float gap)
    {
        float xIncrement = gap;
        int dataCount = 0;
        bool flag = false;

        Vector3 startpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
        while (dataCount < gd.Length)
        {

            Vector3 endpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
            startpoint = new Vector3(startpoint.x, startpoint.y, origin.position.z);
            // pointer is an empty gameObject, i made a prefab of it and attach it in the inspector
            GameObject p = Instantiate(pointer, new Vector3(startpoint.x, startpoint.y, origin.position.z), Quaternion.identity) as GameObject;
            p.transform.parent = holder.transform;

            // linerenderer is an empty gameObject with Line Renderer Component Attach to it, 
            // i made a prefab of it and attach it in the inspector
            GameObject lineObj = Instantiate(linerenderer, startpoint, Quaternion.identity) as GameObject;
            lineObj.transform.parent = holder.transform;
            lineObj.name = dataCount.ToString();

            LineRenderer lineRenderer = lineObj.GetComponent<LineRenderer>();

            lineRenderer.material = greenmat;
            lineRenderer.SetWidth(lrWidth, lrWidth);
            lineRenderer.SetVertexCount(3);

            //while (Vector3.Distance(p.transform.position, endpoint) > 0.2f)
            //{
            //    float step = 5 * Time.deltaTime;
            //    p.transform.position = Vector3.MoveTowards(p.transform.position, endpoint, step);
            //    lineRenderer.SetPosition(0, startpoint);
            //    lineRenderer.SetPosition(1, p.transform.position);

            //    yield return null;
            //}

            lineRenderer.SetPosition(0, startpoint);
            lineRenderer.SetPosition(1, endpoint);


            p.transform.position = endpoint;
            GameObject pointerblue = Instantiate(pointerGreen, endpoint, pointerGreen.transform.rotation) as GameObject;
            pointerblue.transform.parent = holder.transform;
            startpoint = endpoint;

            if (dataGap > 1)
            {
                if ((dataCount + dataGap) == gd.Length)
                {
                    dataCount += dataGap - 1;
                    flag = true;
                }
                else if ((dataCount + dataGap) > gd.Length && !flag)
                {
                    dataCount = gd.Length - 1;
                    flag = true;
                }
                else
                {
                    dataCount += dataGap;
                    if (dataCount == (gd.Length - 1))
                        flag = true;
                }
            }
            else
                dataCount += dataGap;

            xIncrement += gap;

            yield return null;

        }
    }

    IEnumerator BarGraphRed(GraphData[] gd, float gap)
    {
        float xIncrement = gap;
        int dataCount = 0;
        bool flag = false;

        Vector3 startpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
        while (dataCount < gd.Length)
        {

            Vector3 endpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
            startpoint = new Vector3(startpoint.x, startpoint.y, origin.position.z);
            // pointer is an empty gameObject, i made a prefab of it and attach it in the inspector
            GameObject p = Instantiate(pointer, new Vector3(startpoint.x, startpoint.y, origin.position.z), Quaternion.identity) as GameObject;
            p.transform.parent = holder.transform;

            // linerenderer is an empty gameObject with Line Renderer Component Attach to it, 
            // i made a prefab of it and attach it in the inspector
            GameObject lineObj = Instantiate(linerenderer, startpoint, Quaternion.identity) as GameObject;
            lineObj.transform.parent = holder.transform;
            lineObj.name = dataCount.ToString();

            LineRenderer lineRenderer = lineObj.GetComponent<LineRenderer>();

            lineRenderer.material = redmat;
            lineRenderer.SetWidth(lrWidth, lrWidth);
            lineRenderer.SetVertexCount(3);

            //while (Vector3.Distance(p.transform.position, endpoint) > 0.2f)
            //{
            //    float step = 5 * Time.deltaTime;
            //    p.transform.position = Vector3.MoveTowards(p.transform.position, endpoint, step);
            //    lineRenderer.SetPosition(0, startpoint);
            //    lineRenderer.SetPosition(1, p.transform.position);

            //    yield return null;
            //}

            lineRenderer.SetPosition(0, startpoint);
            lineRenderer.SetPosition(1, endpoint);


            p.transform.position = endpoint;
            GameObject pointerblue = Instantiate(pointerRed, endpoint, pointerRed.transform.rotation) as GameObject;
            pointerblue.transform.parent = holder.transform;
            startpoint = endpoint;

            if (dataGap > 1)
            {
                if ((dataCount + dataGap) == gd.Length)
                {
                    dataCount += dataGap - 1;
                    flag = true;
                }
                else if ((dataCount + dataGap) > gd.Length && !flag)
                {
                    dataCount = gd.Length - 1;
                    flag = true;
                }
                else
                {
                    dataCount += dataGap;
                    if (dataCount == (gd.Length - 1))
                        flag = true;
                }
            }
            else
                dataCount += dataGap;

            xIncrement += gap;

            yield return null;

        }
    }

    IEnumerator BarGraphMagenta(GraphData[] gd, float gap)
    {
        float xIncrement = gap;
        int dataCount = 0;
        bool flag = false;

        Vector3 startpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
        while (dataCount < gd.Length)
        {

            Vector3 endpoint = new Vector3((origin.position.x + xIncrement), (origin.position.y + gd[dataCount].marbles), (origin.position.z));
            startpoint = new Vector3(startpoint.x, startpoint.y, origin.position.z);
            // pointer is an empty gameObject, i made a prefab of it and attach it in the inspector
            GameObject p = Instantiate(pointer, new Vector3(startpoint.x, startpoint.y, origin.position.z), Quaternion.identity) as GameObject;
            p.transform.parent = holder.transform;

            // linerenderer is an empty gameObject with Line Renderer Component Attach to it, 
            // i made a prefab of it and attach it in the inspector
            GameObject lineObj = Instantiate(linerenderer, startpoint, Quaternion.identity) as GameObject;
            lineObj.transform.parent = holder.transform;
            lineObj.name = dataCount.ToString();

            LineRenderer lineRenderer = lineObj.GetComponent<LineRenderer>();

            lineRenderer.material = magentamat;
            lineRenderer.SetWidth(lrWidth, lrWidth);
            lineRenderer.SetVertexCount(3);

            //while (Vector3.Distance(p.transform.position, endpoint) > 0.2f)
            //{
            //    float step = 5 * Time.deltaTime;
            //    p.transform.position = Vector3.MoveTowards(p.transform.position, endpoint, step);
            //    lineRenderer.SetPosition(0, startpoint);
            //    lineRenderer.SetPosition(1, p.transform.position);

            //    yield return null;
            //}

            lineRenderer.SetPosition(0, startpoint);
            lineRenderer.SetPosition(1, endpoint);


            p.transform.position = endpoint;
            GameObject pointerblue = Instantiate(pointerMagenta, endpoint, pointerMagenta.transform.rotation) as GameObject;
            pointerblue.transform.parent = holder.transform;
            startpoint = endpoint;

            if (dataGap > 1)
            {
                if ((dataCount + dataGap) == gd.Length)
                {
                    dataCount += dataGap - 1;
                    flag = true;
                }
                else if ((dataCount + dataGap) > gd.Length && !flag)
                {
                    dataCount = gd.Length - 1;
                    flag = true;
                }
                else
                {
                    dataCount += dataGap;
                    if (dataCount == (gd.Length - 1))
                        flag = true;
                }
            }
            else
                dataCount += dataGap;

            xIncrement += gap;

            yield return null;

        }
    }
    public class GraphData
    {
        public float marbles;
    }
}
