﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MOL;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using System.Runtime.InteropServices;

public class CONTROL_COOPERATIVO : MonoBehaviour
{
    [SerializeField] private ROBOT_ONE MANIPULADOR1 = new ROBOT_ONE();
    [SerializeField] private ROBOT_TWO MANIPULADOR2 = new ROBOT_TWO();

    [SerializeField] private GameObject GR1;
    [SerializeField] private GameObject GR2;

    static int k;
    private float To;
    private float Time_sample;

    const string dllPath = "smClient64.dll";
    const string UNITY = "UNITY";
    const string MATLAB = "MATLAB";
    const string ENABLE = "ENABLE";

    [DllImport(dllPath)]
    static extern int openMemory(String name, int type);

    [DllImport(dllPath)]
    static extern void setFloat(String memName, int position, float value);

    [DllImport(dllPath)]
    static extern void setInt(String memName, int position, int value);

    [DllImport(dllPath)]
    static extern float getFloat(String memName, int position);

    float x1p, y1p, z1p;
    float x1d, y1d, z1d;

    float x2p, y2p, z2p;
    float x2d, y2d, z2d;

    float hx1, hy1, hz1;
    float hx2, hy2, hz2;


    void Start()
    {
        openMemory(UNITY, 2);
        openMemory(MATLAB, 2);
        openMemory(ENABLE, 1);

        Time_sample = 0.1f;
        k = 0;

        x1p = getFloat(UNITY, 0);
        y1p = getFloat(UNITY, 1);
        z1p = getFloat(UNITY, 2);

        x1d = getFloat(UNITY, 3);
        y1d = getFloat(UNITY, 4);
        z1d = getFloat(UNITY, 5);

        x2p = getFloat(UNITY, 6);
        y2p = getFloat(UNITY, 7);
        z2p = getFloat(UNITY, 8);

        x2d = getFloat(UNITY, 9);
        y2d = getFloat(UNITY, 10);
        z2d = getFloat(UNITY, 11);

        Matrix ROBOT_UNO = GameObject.FindObjectOfType<ROBOT_ONE>().R_ONE(x1p, y1p, z1p, x1d, y1d, z1d, k);

        Matrix ROBOT_DOS = GameObject.FindObjectOfType<ROBOT_TWO>().R_TWO(x2p, y2p, z2p, x2d, y2d, z2d, k);
        hx1 = (float)ROBOT_UNO[0, 0];
        hy1 = (float)ROBOT_UNO[1, 0];
        hz1 = (float)ROBOT_UNO[2, 0];

        hx2 = (float)ROBOT_DOS[0, 0];
        hy2 = (float)ROBOT_DOS[1, 0];
        hz2 = (float)ROBOT_DOS[2, 0];

        setFloat(MATLAB, 0, hx1);
        setFloat(MATLAB, 1, hy1);
        setFloat(MATLAB, 2, hz1);

        setFloat(MATLAB, 3, hx2);
        setFloat(MATLAB, 4, hy2);
        setFloat(MATLAB, 5, hz2);

        To = 0;

        setInt(ENABLE, 0, 1);
    }


    void Update()
    {
        To += Time.deltaTime;
        if (To >= Time_sample)
        {
            x1p = getFloat(UNITY, 0);
            y1p = getFloat(UNITY, 1);
            z1p = getFloat(UNITY, 2);

            x1d = getFloat(UNITY, 3);
            y1d = getFloat(UNITY, 4);
            z1d = getFloat(UNITY, 5);

            x2p = getFloat(UNITY, 6);
            y2p = getFloat(UNITY, 7);
            z2p = getFloat(UNITY, 8);

            x2d = getFloat(UNITY, 9);
            y2d = getFloat(UNITY, 10);
            z2d = getFloat(UNITY, 11);

            Matrix ROBOT_UNO = GameObject.FindObjectOfType<ROBOT_ONE>().R_ONE(x1p, y1p, z1p, x1d, y1d, z1d, k);

            Matrix ROBOT_DOS = GameObject.FindObjectOfType<ROBOT_TWO>().R_TWO(x2p, y2p, z2p, x2d, y2d, z2d, k);
            hx1 = (float)ROBOT_UNO[0, 0];
            hy1 = (float)ROBOT_UNO[1, 0];
            hz1 = (float)ROBOT_UNO[2, 0];

            hx2 = (float)ROBOT_DOS[0, 0];
            hy2 = (float)ROBOT_DOS[1, 0];
            hz2 = (float)ROBOT_DOS[2, 0];

            setFloat(MATLAB, 0, hx1);
            setFloat(MATLAB, 1, hy1);
            setFloat(MATLAB, 2, hz1);

            setFloat(MATLAB, 3, hx2);
            setFloat(MATLAB, 4, hy2);
            setFloat(MATLAB, 5, hz2);

            this.G_R_ONE(hx1, hy1, hz1, hx2, hy2, hz2);

            k += 1;
            To = 0;
        }
    }


    private void G_R_ONE(float hx1m, float hy1m, float hz1m, float hx2m, float hy2m, float hz2m)
    {
        GR1.transform.position = new Vector3(-hy1m, hz1m, hx1m);
        GR2.transform.position = new Vector3(-hy2m, hz2m, hx2m);
    }


    void OnApplicationQuit()
    {

        setInt(ENABLE, 0, 0);

    }
}
