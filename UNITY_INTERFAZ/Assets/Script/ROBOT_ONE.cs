﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MOL;
using TMPro;
using UnityEngine.SceneManagement;

public class ROBOT_ONE : MonoBehaviour
{
    [SerializeField] private GameObject q1_eslabon;
    [SerializeField] private GameObject q2_eslabon;
    [SerializeField] private GameObject q3_eslabon;
    [SerializeField] private GameObject plataforma;

    float l1r = 0f;
    float l2r = 0.275f;
    float l3r = 0.425f;
    float l4r = 0f;
    float ar = 0.175f;
    float altura = 0.375f;


    public static int k;
    public static float ts = 0.1f;

    public static float[] hxe = new float[9999];
    public static float[] hye = new float[9999];
    public static float[] hze = new float[9999];
    private double[,] error_h = new double[3, 1];

    public static float[] hxdp = new float[9999];
    public static float[] hydp = new float[9999];
    public static float[] hzdp = new float[9999];
    private double[,] velocidades_trayectoria = new double[3, 1];

    public static float[] hxd = new float[9999];
    public static float[] hyd = new float[9999];
    public static float[] hzd = new float[9999];

    public static float[] q1 = new float[9999];
    public static float[] q2 = new float[9999];
    public static float[] q3 = new float[9999];

    public static float[] q1p = new float[9999];
    public static float[] q2p = new float[9999];
    public static float[] q3p = new float[9999];

    public static float[] u = new float[9999];
    public static float[] w = new float[9999];


    public static float[] uref_c = new float[9999];
    public static float[] wref_c = new float[9999];
    public static float[] q1pref_c = new float[9999];
    public static float[] q2pref_c = new float[9999];
    public static float[] q3pref_c = new float[9999];


    public static float[] ue = new float[9999];
    public static float[] we = new float[9999];
    public static float[] q1pe = new float[9999];
    public static float[] q2pe = new float[9999];
    public static float[] q3pe = new float[9999];


    public static float[] vrefp_u = new float[9999];
    public static float[] vrefp_w = new float[9999];
    public static float[] vrefp_q1p = new float[9999];
    public static float[] vrefp_q2p = new float[9999];
    public static float[] vrefp_q3p = new float[9999];

    public static float xp;
    public static float yp;


    public static float[] hx = new float[9999];
    public static float[] hy = new float[9999];
    public static float[] hz = new float[9999];

    public static float[] x = new float[9999];
    public static float[] y = new float[9999];
    public static float[] phi = new float[9999];

    private double[,] error_velocidades = new double[5, 1];
    private double[,] derivadas_velocidades = new double[5, 1];
    private double[,] eslabones = new double[5, 1];
    private double[,] velocidades = new double[5, 1];
    private double[,] velocidades_ref = new double[5, 1];


    public static float[] uref = new float[9999];
    public static float[] wref = new float[9999];
    public static float[] q1pref = new float[9999];
    public static float[] q2pref = new float[9999];
    public static float[] q3pref = new float[9999];

    public static float usa, wsa, q1psa, q2psa, q3psa, thsa, q1sa, q2sa, q3sa;

    public Matrix R_ONE(float hxdpm, float hydpm, float hzdpm, float hxdm, float hydm, float hzdm, int k)
    {
        Matrix R_ONE;

        hxd[k] = hxdm;
        hyd[k] = hydm;
        hzd[k] = hzdm;

        hxdp[k] = hxdpm;
        hydp[k] = hydpm;
        hzdp[k] = hzdpm;


        hxe[k] = hx[k] - hxd[k];
        hye[k] = hy[k] - hyd[k];
        hze[k] = hz[k] - hzd[k];

        double hxe_s = (Mathf.Exp(hxe[k]) - Mathf.Exp(-hxe[k])) / (Mathf.Exp(hxe[k]) + Mathf.Exp(-hxe[k]));
        double hye_s = (Mathf.Exp(hye[k]) - Mathf.Exp(-hye[k])) / (Mathf.Exp(hye[k]) + Mathf.Exp(-hye[k]));
        double hze_s = (Mathf.Exp(hze[k]) - Mathf.Exp(-hze[k])) / (Mathf.Exp(hze[k]) + Mathf.Exp(-hze[k]));

        double[,] h_s = new double[3, 1] { { hxe_s }, { hye_s }, { hze_s } };

        Matrix he = new Matrix(h_s);

        velocidades_trayectoria = new double[3, 1] { { hxdp[k] }, { hydp[k] }, { hzdp[k] } };
        Matrix hdp = new Matrix(velocidades_trayectoria);


        float j11 = Mathf.Cos(phi[k]);
        float j12 = -ar * Mathf.Sin(phi[k]) - l2r * Mathf.Cos(q2[k]) * Mathf.Sin(q1[k] + phi[k]) - l3r * Mathf.Cos(q2[k] + q3[k]) * Mathf.Sin(q1[k] + phi[k]);
        float j13 = -l2r * Mathf.Cos(q2[k]) * Mathf.Sin(q1[k] + phi[k]) - l3r * Mathf.Cos(q2[k] + q3[k]) * Mathf.Sin(q1[k] + phi[k]);
        float j14 = -l2r * Mathf.Cos(q1[k] + phi[k]) * Mathf.Sin(q2[k]) - l3r * Mathf.Cos(q1[k] + phi[k]) * Mathf.Cos(q2[k] + q3[k]);
        float j15 = -l3r * Mathf.Cos(q1[k] + phi[k]) * Mathf.Sin(q2[k] + q3[k]);

        float j21 = Mathf.Sin(phi[k]);
        float j22 = ar * Mathf.Cos(phi[k]) + l2r * Mathf.Cos(q2[k]) * Mathf.Cos(q1[k] + phi[k]) + l3r * Mathf.Cos(q2[k] + q3[k]) * Mathf.Cos(q1[k] + phi[k]);
        float j23 = l2r * Mathf.Cos(q2[k]) * Mathf.Cos(q1[k] + phi[k]) + l3r * Mathf.Cos(q2[k] + q3[k]) * Mathf.Cos(q1[k] + phi[k]);
        float j24 = -l2r * Mathf.Sin(q1[k] + phi[k]) * Mathf.Sin(q2[k]) - l3r * Mathf.Sin(q1[k] + phi[k]) * Mathf.Sin(q2[k] + q3[k]);
        float j25 = -l3r * Mathf.Sin(q1[k] + phi[k]) * Mathf.Sin(q2[k] + q3[k]);

        float j31 = 0f;
        float j32 = 0f;
        float j33 = 0f;
        float j34 = l2r * Mathf.Cos(q2[k]) + l3r * Mathf.Cos(q2[k] + q3[k]);
        float j35 = l3r * Mathf.Cos(q2[k] + q3[k]);

        double[,] J = new double[3, 5] {    {j11, j12, j13, j14, j15},
                                            {j21, j22, j23, j24, j25},
                                            {j31, j32, j33, j34, j35}};

        Matrix Jacobina = new Matrix(J);
        Matrix J_T = Jacobina.Transposition();
        Matrix J_J_T = Jacobina * J_T;
        Matrix INVERSA = J_J_T.Inverse();
        Matrix psedudo = J_T * INVERSA;


        double[,] G = new double[3, 3]{ {-1, 0,  0},
                                        {0, -1,  0},
                                        {0,  0, -1}};

        Matrix MG = new Matrix(G);

        // Matrix MG_he = MG * he;
        // Matrix Vel_MG_he = hdp + MG_he;

        Matrix qref = (psedudo * (hdp + MG * he));

        //Matrix qref = psedudo * Vel_MG_he;


        uref_c[k] = (float)qref[0, 0];
        wref_c[k] = (float)qref[1, 0];
        q1pref_c[k] = (float)qref[2, 0];
        q2pref_c[k] = (float)qref[3, 0];
        q3pref_c[k] = (float)qref[4, 0];

        velocidades = new double[5, 1] { { uref_c[k] }, { wref_c[k] }, { q1pref_c[k] }, { q2pref_c[k] }, { q3pref_c[k] } };
        Matrix v = new Matrix(velocidades);

        ue[k] = u[k] - uref_c[k];
        we[k] = w[k] - wref_c[k];
        q1pe[k] = q1p[k] - q1pref_c[k];
        q2pe[k] = q2p[k] - q2pref_c[k];
        q3pe[k] = q3p[k] - q3pref_c[k];

        error_velocidades = new double[5, 1] { { ue[k] }, { we[k] }, { q1pe[k] }, { q2pe[k] }, { q3pe[k] } };
        Matrix vref_e = new Matrix(error_velocidades);

        #region DERIVADA VELOCIDADES
        if (k == 0)
        {
            vrefp_u[k] = (uref_c[k]) / ts;
            vrefp_w[k] = (wref_c[k]) / ts;
            vrefp_q1p[k] = (q1pref_c[k]) / ts;
            vrefp_q2p[k] = (q2pref_c[k]) / ts;
            vrefp_q3p[k] = (q3pref_c[k]) / ts;
        }
        else
        {
            vrefp_u[k] = (uref_c[k] - uref_c[k - 1]) / ts;
            vrefp_w[k] = (wref_c[k] - wref_c[k - 1]) / ts;
            vrefp_q1p[k] = (q1pref_c[k] - q1pref_c[k - 1]) / ts;
            vrefp_q2p[k] = (q2pref_c[k] - q2pref_c[k - 1]) / ts;
            vrefp_q3p[k] = (q3pref_c[k] - q3pref_c[k - 1]) / ts;
        }

        derivadas_velocidades = new double[5, 1] { { vrefp_u[k] }, { vrefp_w[k] }, { vrefp_q1p[k] }, { vrefp_q2p[k] }, { vrefp_q3p[k] } };
        Matrix vrefp = new Matrix(derivadas_velocidades);
        #endregion

        eslabones = new double[5, 1] { { 0 }, { phi[k] }, { q1[k] }, { q2[k] }, { q3[k] } };
        Matrix q = new Matrix(eslabones);

        Matrix Dinamica = COMPENSACION_DINAMICA_3GDL(vrefp, vref_e, v, q);

        uref[k] = (float)Dinamica[0, 0];
        wref[k] = (float)Dinamica[1, 0];
        q1pref[k] = (float)Dinamica[2, 0];
        q2pref[k] = (float)Dinamica[3, 0];
        q3pref[k] = (float)Dinamica[4, 0];

        velocidades_ref = new double[5, 1] { { uref[k] }, { wref[k] }, { q1pref[k] }, { q2pref[k] }, { q3pref[k] } };
        Matrix vref = new Matrix(velocidades_ref);

        Matrix Akasha = AKASHA_DINAMICA(vref, v, q);

        u[k + 1] = (float)Akasha[0, 0];
        w[k + 1] = (float)Akasha[1, 0];
        q1p[k + 1] = (float)Akasha[2, 0];
        q2p[k + 1] = (float)Akasha[3, 0];
        q3p[k + 1] = (float)Akasha[4, 0];


        phi[k + 1] = (float)Akasha[6, 0];
        q1[k + 1] = (float)Akasha[7, 0];
        q2[k + 1] = (float)Akasha[8, 0];
        q3[k + 1] = (float)Akasha[9, 0];

        xp = u[k] * Mathf.Cos(phi[k]) - ar * w[k] * Mathf.Sin(phi[k]);
        yp = u[k] * Mathf.Sin(phi[k]) + ar * w[k] * Mathf.Cos(phi[k]);

        x[k + 1] = ts * xp + x[k];
        y[k + 1] = ts * yp + y[k];
        //   Debug.Log(y[k]);

        this.BRAZO(q1[k], q2[k], q3[k]);
        this.PLATAFORMA(y[k], x[k], phi[k]);

        Matrix Cinematica = Calculo_Cinematica(x[k + 1], y[k + 1], phi[k + 1], q1[k + 1], q2[k + 1], q3[k + 1]);
        hx[k + 1] = (float)Cinematica[0, 0];
        hy[k + 1] = (float)Cinematica[1, 0];
        hz[k + 1] = (float)Cinematica[2, 0];


        double[,] SR1 = new double[3, 1] { { hx[k + 1] }, { hy[k + 1] }, { hz[k + 1] } };
        Matrix ROBOT = new Matrix(SR1);

        R_ONE = ROBOT;

        return R_ONE;

    }

    private Matrix COMPENSACION_DINAMICA_3GDL(Matrix velp, Matrix vele, Matrix vel, Matrix esla)
    {
        Matrix COMPENSACION_DINAMICA_3GDL;

        float us = (float)vel[0, 0];
        float ws = (float)vel[1, 0];
        float q1ps = (float)vel[2, 0];
        float q2ps = (float)vel[3, 0];
        float q3ps = (float)vel[4, 0];

        float ths = (float)esla[1, 0];
        float q1s = (float)esla[2, 0];
        float q2s = (float)esla[3, 0];
        float q3s = (float)esla[4, 0];

        float m = 30f, r = 0.15f, R = 0.25f, a = 0.195f;
        float m2 = 3f, m3 = 4;
        float l2 = 0.275f, l3 = 0.375f, g = 9.81f, Ra = 4.6f;
        float Kp = 4f, Kd = 0.63f, Ka = 584f, Kb = 0.05f;
        float Kpt = 4f, Kpr = 2f, Kdt = 0.63f, Kdr = 0.28f;
        float Kpa = 584f, Kpb = 0.05f, Rpa = 4.6f;

        float C1 = Kdt / Kpt;
        float C2 = Rpa * r * (m + m2 + m3) / (2 * Kpa * Kpt);
        float C3 = Rpa * m2 * r / (2 * Kpa * Kpt);
        float C4 = Rpa * m3 * r / (2 * Kpa * Kpt);
        float C5 = Rpa * m2 * r / (2 * Kpa * Kpr * R);
        float C6 = Rpa * m3 * r / (2 * Kpa * Kpr * R);
        float C7 = Kdr / Kpr;
        float C8 = Rpa * r * (m + m2 + m3) / (2 * Kpa * Kpr * R);

        float C9 = Ra * m2 / (Ka * Kp);
        float C10 = Ra * m3 / (Ka * Kp);
        float C11 = Kd / (Kp);
        float C12 = Kpb / (Kpt * r) + 0.95f;
        float C13 = Kpb * R / (Kpr * r) + 0.95f;
        float C14 = Kb / (Kp) + 0.95f;
        float C15 = Ra * g * m2 / (Ka * Kp);
        float C16 = Ra * g * m3 / (Ka * Kp);


        float M11 = C1 + C2;
        float M12 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M13 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M14 = -l2 * C3 * Mathf.Sin(q2s) * Mathf.Cos(q1s) - C4 * (l2 * Mathf.Sin(q2s) * Mathf.Cos(q1s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s));
        float M15 = -C4 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s);
        float M21 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M22 = C7 + C8 * (1 + 2 * Mathf.Pow(a, 2)) + C5 * l2 * Mathf.Cos(q2s) * (l2 * Mathf.Cos(q2s) + 2 * a * Mathf.Cos(q1s) + a * Mathf.Cos(ths + q1s) * Mathf.Cos(ths) + a * Mathf.Sin(ths + q1s) * Mathf.Sin(ths)) + C6 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s) + 2 * a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + 2 * a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s));
        float M23 = C5 * (Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + a * l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) + C6 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M24 = -C5 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) - C6 * (a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s));
        float M25 = -C6 * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s);
        float M31 = -l2 * C9 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C10 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M32 = C9 * (Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * a * l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) + C10 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + 2 * a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M33 = C11 + C9 * Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + C10 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M34 = 0f;
        float M35 = 0f;
        float M41 = -l2 * C9 * Mathf.Sin(q2s) * Mathf.Cos(q1s) - C10 * (l2 * Mathf.Sin(q2s) * Mathf.Cos(q1s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s));
        float M42 = -C9 * 2 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) - C10 * (2 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) + 2 * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s));
        float M43 = 0f;
        float M44 = C11 + C9 * Mathf.Pow(l2, 2) + C10 * (Mathf.Pow(l2, 2) + Mathf.Pow(l3, 2) + 2f * l2 * l3 * Mathf.Cos(q3s));
        float M45 = C10 * l3 * (l3 + l2 * Mathf.Cos(q3s));
        float M51 = -C10 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s);
        float M52 = -C10 * 2f * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s);
        float M53 = 0;
        float M54 = C10 * l3 * (l3 + l2 * Mathf.Cos(q3s));
        float M55 = C11 + C10 * Mathf.Pow(l3, 2);


        double[,] MASA = new double[5, 5] {   {M11, M12, M13, M14, M15},
                                              {M21, M22, M23, M24, M25},
                                              {M31, M32, M33, M34, M35},
                                              {M41, M42, M43, M44, M45},
                                              {M51, M52, M53, M54, M55}};

        Matrix M = new Matrix(MASA);

        double[,] MGK = new double[5, 5] {    {-1,  0,  0,  0,  0},
                                              { 0, -1,  0,  0,  0},
                                              { 0,  0, -1,  0,  0},
                                              { 0,  0,  0, -1,  0},
                                              { 0,  0,  0,  0, -1}};

        Matrix GK = new Matrix(MGK);

        float Cs11 = C12;
        float Cs12 = -2 * a * C2 * ws + C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q2ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * ws - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q1ps) + C4 * (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) * q3ps + (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s)) * q2ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * q1ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws);
        float Cs13 = C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q2ps + l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q1ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * ws) + C4 * (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) * q3ps + (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s)) * q2ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * q1ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws);
        float Cs14 = C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * ws + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q1ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q2ps) + C4 * ((l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s)) * ws + (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s)) * q1ps - (l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) * q2ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q3ps);
        float Cs15 = C4 * (l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * ws + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * q1ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q2ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q3ps);

        float Cs21 = C8 * ws + (C5 * (l2 * Mathf.Cos(q1s) * Mathf.Cos(ths) * Mathf.Cos(2 * q1s + q2s) + 2 * l2 * Mathf.Sin(q1s + ths) * Mathf.Cos(q2s) * Mathf.Sin(ths)) + C6 * (l2 * Mathf.Cos(q2s) * Mathf.Cos(ths) * Mathf.Cos(q1s - q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(ths) * Mathf.Cos(q1s - q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(ths + q1s) * Mathf.Sin(ths))) * ws;
        float Cs22 = C12 + C5 * (a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) * (ws + q1ps) + (Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * q2ps) + C6 * ((a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s)) * (ws + q1ps) + (a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) + Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s)) * (q2ps + q3ps));
        float Cs23 = -C5 * (a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) * (ws + q1ps) + (Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * q2ps) - C6 * ((a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s)) * (ws + q1ps) + (a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) + Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s)) * (q2ps + q3ps));
        float Cs24 = -C5 * ((Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) * (ws + q1ps) + a * l2 * Mathf.Sin(q1s) * Mathf.Cos(q2s)) * (q2ps + q3ps)) - C6 * ((Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * (ws + q1ps) + (a * l2 * Mathf.Sin(q1s) * Mathf.Cos(q2s) + a * l3 * Mathf.Sin(q1s) * Mathf.Cos(q2s + q3s)) * (q2ps + q3ps));
        float Cs25 = -C6 * ((l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s) + l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s))) * (ws + q1ps) + (a * l3 * Mathf.Sin(q1s) * Mathf.Cos(q2s + q3s)) * (q2ps + q3ps);

        float Cs31 = C9 * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) * ws + C10 * (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws;
        float Cs32 = C9 * (-Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * q2ps + (a * l2 * Mathf.Sin(q1s + q2s) + a * l2 * Mathf.Sin(q1s - q2s)) * ws) + C10 * ((a * l2 * Mathf.Sin(q1s + q2s) + a * l2 * Mathf.Sin(q1s - q2s) - a * l3 / 4 * Mathf.Sin(q2s - q1s + q3s) + 3 * a * l3 / 4 * Mathf.Sin(q1s - q2s - q3s) + a * l3 * Mathf.Sin(q1s + q2s + q3s)) * ws - (Mathf.Pow(l2, 2) / 2 * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * q2ps - (Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 / 2 * Mathf.Sin(q3s) + l2 * l3 / 2 * Mathf.Sin(2 * q2s + q3s)) * q3ps);
        float Cs33 = C14 - C9 * Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * q2ps - C10 * ((Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s)) * q3ps);
        float Cs34 = C9 * Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps) + C10 * ((Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s))) * (ws + q1ps);
        float Cs35 = C10 * ((l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s) + l2 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s))) * (ws + q1ps);

        float Cs41 = -C9 * l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * ws - C10 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s)) * ws;
        float Cs42 = C9 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps) + (a * l2 * Mathf.Sin(q1s + q2s) - a * l2 * Mathf.Sin(q1s - q2s)) * ws) + C10 * ((a * l2 * Mathf.Sin(q1s + q2s) - a * l2 * Mathf.Sin(q1s - q2s) + a * l3 / 4 * Mathf.Sin(q2s - q1s + q3s) - 3 * a * l3 / 4 * Mathf.Sin(q1s - q2s - q3s) + a * l3 * Mathf.Sin(q1s + q2s + q3s)) * ws + (Mathf.Pow(l2, 2) / 2 * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * (ws + q1ps));
        float Cs43 = C9 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps)) + C10 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2f * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * (ws + q1ps);
        float Cs44 = C14 - C10 * l2 * l3 * Mathf.Sin(q3s) * q3ps;
        float Cs45 = -C10 * l2 * l3 * Mathf.Sin(q3s) * (q2ps + q3ps);

        float Cs51 = -C10 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * ws;
        float Cs52 = C10 * l3 * Mathf.Sin(q2s + q3s) * ((2f * a * Mathf.Cos(q1s) + l2 * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s)) * ws + (a * Mathf.Cos(q1s) + l3 * Mathf.Cos(q2s + q3s)) * q1ps);
        float Cs53 = C10 * l3 * Mathf.Sin(q2s + q3s) * (l3 * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q2s)) * (ws + q1ps);
        float Cs54 = C10 * l2 * l3 * Mathf.Sin(q3s) * q2ps;
        float Cs55 = C14;

        double[,] CENTIPETRA = new double[5, 5] {   {Cs11, Cs12, Cs13, Cs14, Cs15},
                                                    {Cs21, Cs22, Cs23, Cs24, Cs25},
                                                    {Cs31, Cs32, Cs33, Cs34, Cs35},
                                                    {Cs41, Cs42, Cs43, Cs44, Cs45},
                                                    {Cs51, Cs52, Cs53, Cs54, Cs55}};

        Matrix C = new Matrix(CENTIPETRA);

        float G4 = C15 * l2 * Mathf.Cos(q2s) + C16 * (l3 * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q2s));
        float G5 = C16 * l3 * Mathf.Cos(q2s + q3s);

        double[,] GRAVITACIONAL = new double[5, 1] { { 0 }, { 0 }, { 0 }, { G4 }, { G5 } };
        Matrix G = new Matrix(GRAVITACIONAL);

        COMPENSACION_DINAMICA_3GDL = M * (velp + GK * vele) + C * vel + G;

        return COMPENSACION_DINAMICA_3GDL;

    }


    private Matrix AKASHA_DINAMICA(Matrix velr, Matrix vel, Matrix esla)
    {
        Matrix AKASHA_DINAMICA;

        float us = (float)vel[0, 0];
        float ws = (float)vel[1, 0];
        float q1ps = (float)vel[2, 0];
        float q2ps = (float)vel[3, 0];
        float q3ps = (float)vel[4, 0];

        float ths = (float)esla[1, 0];
        float q1s = (float)esla[2, 0];
        float q2s = (float)esla[3, 0];
        float q3s = (float)esla[4, 0];

        //float error = Random.Range(0f, 1f) * 2f;
        float error = 1;

        float m = 30 * error, r = 0.15f, R = 0.25f, a = 0.195f;
        float m2 = 3 * error, m3 = 4f * error;
        float l2 = 0.275f, l3 = 0.375f, g = 9.81f, Ra = 4.6f;
        float Kp = 4f, Kd = 0.63f, Ka = 584f, Kb = 0.05f;
        float Kpt = 4f, Kpr = 2f, Kdt = 0.63f, Kdr = 0.28f;
        float Kpa = 584f, Kpb = 0.05f, Rpa = 4.6f;

        float C1 = Kdt / Kpt;
        float C2 = Rpa * r * (m + m2 + m3) / (2 * Kpa * Kpt);
        float C3 = Rpa * m2 * r / (2 * Kpa * Kpt);
        float C4 = Rpa * m3 * r / (2 * Kpa * Kpt);
        float C5 = Rpa * m2 * r / (2 * Kpa * Kpr * R);
        float C6 = Rpa * m3 * r / (2 * Kpa * Kpr * R);
        float C7 = Kdr / Kpr;
        float C8 = Rpa * r * (m + m2 + m3) / (2 * Kpa * Kpr * R);

        float C9 = Ra * m2 / (Ka * Kp);
        float C10 = Ra * m3 / (Ka * Kp);
        float C11 = Kd / (Kp);
        float C12 = Kpb / (Kpt * r) + 0.95f;
        float C13 = Kpb * R / (Kpr * r) + 0.95f;
        float C14 = Kb / (Kp) + 0.95f;
        float C15 = Ra * g * m2 / (Ka * Kp);
        float C16 = Ra * g * m3 / (Ka * Kp);


        float M11 = C1 + C2;
        float M12 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M13 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M14 = -l2 * C3 * Mathf.Sin(q2s) * Mathf.Cos(q1s) - C4 * (l2 * Mathf.Sin(q2s) * Mathf.Cos(q1s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s));
        float M15 = -C4 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s);
        float M21 = -l2 * C3 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C4 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M22 = C7 + C8 * (1 + 2 * Mathf.Pow(a, 2)) + C5 * l2 * Mathf.Cos(q2s) * (l2 * Mathf.Cos(q2s) + 2 * a * Mathf.Cos(q1s) + a * Mathf.Cos(ths + q1s) * Mathf.Cos(ths) + a * Mathf.Sin(ths + q1s) * Mathf.Sin(ths)) + C6 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s) + 2 * a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + 2 * a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s));
        float M23 = C5 * (Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + a * l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) + C6 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M24 = -C5 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) - C6 * (a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s));
        float M25 = -C6 * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s);
        float M31 = -l2 * C9 * Mathf.Cos(q2s) * Mathf.Sin(q1s) - C10 * (l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s));
        float M32 = C9 * (Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * a * l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) + C10 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s) + 2 * a * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M33 = C11 + C9 * Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + C10 * (Mathf.Pow(l3, 2) * Mathf.Pow(Mathf.Cos(q2s + q3s), 2) + Mathf.Pow(l2, 2) * Mathf.Pow(Mathf.Cos(q2s), 2) + 2 * l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q2s));
        float M34 = 0f;
        float M35 = 0f;
        float M41 = -l2 * C9 * Mathf.Sin(q2s) * Mathf.Cos(q1s) - C10 * (l2 * Mathf.Sin(q2s) * Mathf.Cos(q1s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s));
        float M42 = -C9 * 2 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) - C10 * (2 * a * l2 * Mathf.Sin(q2s) * Mathf.Sin(q1s) + 2 * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s));
        float M43 = 0f;
        float M44 = C11 + C9 * Mathf.Pow(l2, 2) + C10 * (Mathf.Pow(l2, 2) + Mathf.Pow(l3, 2) + 2f * l2 * l3 * Mathf.Cos(q3s));
        float M45 = C10 * l3 * (l3 + l2 * Mathf.Cos(q3s));
        float M51 = -C10 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s);
        float M52 = -C10 * 2f * a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s);
        float M53 = 0;
        float M54 = C10 * l3 * (l3 + l2 * Mathf.Cos(q3s));
        float M55 = C11 + C10 * Mathf.Pow(l3, 2);


        double[,] MASA = new double[5, 5] {   {M11, M12, M13, M14, M15},
                                              {M21, M22, M23, M24, M25},
                                              {M31, M32, M33, M34, M35},
                                              {M41, M42, M43, M44, M45},
                                              {M51, M52, M53, M54, M55}};

        Matrix M = new Matrix(MASA);

        double[,] MGK = new double[5, 5] {    {-1,  0,  0,  0,  0},
                                              { 0, -1,  0,  0,  0},
                                              { 0,  0, -1,  0,  0},
                                              { 0,  0,  0, -1,  0},
                                              { 0,  0,  0,  0, -1}};

        Matrix GK = new Matrix(MGK);

        float Cs11 = C12;
        float Cs12 = -2 * a * C2 * ws + C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q2ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * ws - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q1ps) + C4 * (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) * q3ps + (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s)) * q2ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * q1ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws);
        float Cs13 = C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q2ps + l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q1ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * ws) + C4 * (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) * q3ps + (l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s) + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s)) * q2ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * q1ps - (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws);
        float Cs14 = C3 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * ws + l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * q1ps - l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) * q2ps) + C4 * ((l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s)) * ws + (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s)) * q1ps - (l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s)) * q2ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q3ps);
        float Cs15 = C4 * (l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * ws + l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * q1ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q2ps - l3 * Mathf.Cos(q1s) * Mathf.Cos(q2s + q3s) * q3ps);

        float Cs21 = C8 * ws + (C5 * (l2 * Mathf.Cos(q1s) * Mathf.Cos(ths) * Mathf.Cos(2 * q1s + q2s) + 2 * l2 * Mathf.Sin(q1s + ths) * Mathf.Cos(q2s) * Mathf.Sin(ths)) + C6 * (l2 * Mathf.Cos(q2s) * Mathf.Cos(ths) * Mathf.Cos(q1s - q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(ths) * Mathf.Cos(q1s - q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(ths + q1s) * Mathf.Sin(ths))) * ws;
        float Cs22 = C12 + C5 * (a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) * (ws + q1ps) + (Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * q2ps) + C6 * ((a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s)) * (ws + q1ps) + (a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) + Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s)) * (q2ps + q3ps));
        float Cs23 = -C5 * (a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) * (ws + q1ps) + (Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * q2ps) - C6 * ((a * l2 * Mathf.Cos(q2s) * Mathf.Sin(q1s) + a * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q1s)) * (ws + q1ps) + (a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) + Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s)) * (q2ps + q3ps));
        float Cs24 = -C5 * ((Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s) * (ws + q1ps) + a * l2 * Mathf.Sin(q1s) * Mathf.Cos(q2s)) * (q2ps + q3ps)) - C6 * ((Mathf.Pow(l2, 2) * Mathf.Cos(q2s) * Mathf.Sin(q2s) + Mathf.Pow(l3, 2) * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s) + l2 * l3 * Mathf.Cos(q2s + q3s) * Mathf.Sin(q2s) + l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + a * l2 * Mathf.Cos(q1s) * Mathf.Sin(q2s)) * (ws + q1ps) + (a * l2 * Mathf.Sin(q1s) * Mathf.Cos(q2s) + a * l3 * Mathf.Sin(q1s) * Mathf.Cos(q2s + q3s)) * (q2ps + q3ps));
        float Cs25 = -C6 * ((l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s) + a * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q1s) + l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s))) * (ws + q1ps) + (a * l3 * Mathf.Sin(q1s) * Mathf.Cos(q2s + q3s)) * (q2ps + q3ps);

        float Cs31 = C9 * l2 * Mathf.Cos(q2s) * Mathf.Cos(q1s) * ws + C10 * (l2 * Mathf.Cos(q1s) * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s) * Mathf.Cos(q1s)) * ws;
        float Cs32 = C9 * (-Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * q2ps + (a * l2 * Mathf.Sin(q1s + q2s) + a * l2 * Mathf.Sin(q1s - q2s)) * ws) + C10 * ((a * l2 * Mathf.Sin(q1s + q2s) + a * l2 * Mathf.Sin(q1s - q2s) - a * l3 / 4 * Mathf.Sin(q2s - q1s + q3s) + 3 * a * l3 / 4 * Mathf.Sin(q1s - q2s - q3s) + a * l3 * Mathf.Sin(q1s + q2s + q3s)) * ws - (Mathf.Pow(l2, 2) / 2 * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * q2ps - (Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 / 2 * Mathf.Sin(q3s) + l2 * l3 / 2 * Mathf.Sin(2 * q2s + q3s)) * q3ps);
        float Cs33 = C14 - C9 * Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * q2ps - C10 * ((Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * q2ps + (l2 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s) + Mathf.Pow(l3, 2) * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s)) * q3ps);
        float Cs34 = C9 * Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps) + C10 * ((Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s))) * (ws + q1ps);
        float Cs35 = C10 * ((l3 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s + q3s) + l2 * Mathf.Sin(q2s + q3s) * Mathf.Cos(q2s))) * (ws + q1ps);

        float Cs41 = -C9 * l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) * ws - C10 * (l2 * Mathf.Sin(q1s) * Mathf.Sin(q2s) + l3 * Mathf.Sin(q1s) * Mathf.Sin(q2s + q3s)) * ws;
        float Cs42 = C9 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps) + (a * l2 * Mathf.Sin(q1s + q2s) - a * l2 * Mathf.Sin(q1s - q2s)) * ws) + C10 * ((a * l2 * Mathf.Sin(q1s + q2s) - a * l2 * Mathf.Sin(q1s - q2s) + a * l3 / 4 * Mathf.Sin(q2s - q1s + q3s) - 3 * a * l3 / 4 * Mathf.Sin(q1s - q2s - q3s) + a * l3 * Mathf.Sin(q1s + q2s + q3s)) * ws + (Mathf.Pow(l2, 2) / 2 * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2 * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * (ws + q1ps));
        float Cs43 = C9 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) * (ws + q1ps)) + C10 * (Mathf.Pow(l2, 2) / 2f * Mathf.Sin(2 * q2s) + Mathf.Pow(l3, 2) / 2f * Mathf.Sin(2 * q2s + 2 * q3s) + l2 * l3 * Mathf.Sin(2 * q2s + q3s)) * (ws + q1ps);
        float Cs44 = C14 - C10 * l2 * l3 * Mathf.Sin(q3s) * q3ps;
        float Cs45 = -C10 * l2 * l3 * Mathf.Sin(q3s) * (q2ps + q3ps);

        float Cs51 = -C10 * l3 * Mathf.Sin(q2s + q3s) * Mathf.Sin(q1s) * ws;
        float Cs52 = C10 * l3 * Mathf.Sin(q2s + q3s) * ((2f * a * Mathf.Cos(q1s) + l2 * Mathf.Cos(q2s) + l3 * Mathf.Cos(q2s + q3s)) * ws + (a * Mathf.Cos(q1s) + l3 * Mathf.Cos(q2s + q3s)) * q1ps);
        float Cs53 = C10 * l3 * Mathf.Sin(q2s + q3s) * (l3 * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q2s)) * (ws + q1ps);
        float Cs54 = C10 * l2 * l3 * Mathf.Sin(q3s) * q2ps;
        float Cs55 = C14;

        double[,] CENTIPETRA = new double[5, 5] {   {Cs11, Cs12, Cs13, Cs14, Cs15},
                                              {Cs21, Cs22, Cs23, Cs24, Cs25},
                                              {Cs31, Cs32, Cs33, Cs34, Cs35},
                                              {Cs41, Cs42, Cs43, Cs44, Cs45},
                                              {Cs51, Cs52, Cs53, Cs54, Cs55}};

        Matrix C = new Matrix(CENTIPETRA);

        float G4 = C15 * l2 * Mathf.Cos(q2s) + C16 * (l3 * Mathf.Cos(q2s + q3s) + l2 * Mathf.Cos(q2s));
        float G5 = C16 * l3 * Mathf.Cos(q2s + q3s);

        double[,] GRAVITACIONAL = new double[5, 1] { { 0 }, { 0 }, { 0 }, { G4 }, { G5 } };
        Matrix G = new Matrix(GRAVITACIONAL);

        Matrix invM = M.Inverse();

        Matrix vp = invM * (velr - C * vel - G);

        us = (float)vp[0, 0] * ts + (float)vel[0, 0];
        ws = (float)vp[1, 0] * ts + (float)vel[1, 0];
        q1ps = (float)vp[2, 0] * ts + (float)vel[2, 0];
        q2ps = (float)vp[3, 0] * ts + (float)vel[3, 0];
        q3ps = (float)vp[4, 0] * ts + (float)vel[4, 0];

        ths = (float)vel[1, 0] * ts + (float)esla[1, 0];
        q1s = (float)vel[2, 0] * ts + (float)esla[2, 0];
        q2s = (float)vel[3, 0] * ts + (float)esla[3, 0];
        q3s = (float)vel[4, 0] * ts + (float)esla[4, 0];

        double[,] AKASHA = new double[10, 1] { { us }, { ws }, { q1ps }, { q2ps }, { q3ps }, { 0f }, { ths }, { q1s }, { q2s }, { q3s } };
        Matrix SAKASHA = new Matrix(AKASHA);

        AKASHA_DINAMICA = SAKASHA;

        return AKASHA_DINAMICA;

    }

    public Matrix Calculo_Cinematica(float x_1, float y_1, float phi_1, float q1_1, float q2_1, float q3_1)

    {
        Matrix Calculo_Cinematica;
        float hxc = x_1 + ar * Mathf.Cos(phi_1) + l2r * Mathf.Cos(q1_1 + phi_1) * Mathf.Cos(q2_1) + l3r * Mathf.Cos(q1_1 + phi_1) * Mathf.Cos(q2_1 + q3_1);
        float hyc = y_1 + ar * Mathf.Sin(phi_1) + l2r * Mathf.Sin(q1_1 + phi_1) * Mathf.Cos(q2_1) + l3r * Mathf.Sin(q1_1 + phi_1) * Mathf.Cos(q2_1 + q3_1);
        float hzc = altura + l1r + l2r * Mathf.Sin(q2_1) + l3r * Mathf.Sin(q2_1 + q3_1);

        double[,] SC = new double[3, 1] { { hxc }, { hyc }, { hzc } };
        Matrix C = new Matrix(SC);

        Calculo_Cinematica = C;
        return Calculo_Cinematica;
    }

    private void BRAZO(float q1m, float q2m, float q3m)
    {

        q1_eslabon.transform.localEulerAngles = new Vector3(0f, ((-q1m / 1) * Mathf.Rad2Deg), 0f); // Sentido por ley de la mano derecha
        q2_eslabon.transform.localEulerAngles = new Vector3(((q2m / 1) * Mathf.Rad2Deg), 0f, 0f);
        q3_eslabon.transform.localEulerAngles = new Vector3(((q3m / 1) * Mathf.Rad2Deg), 0f, 90f);
        // q4_eslabon.transform.localEulerAngles = new Vector3(((0) * Mathf.Rad2Deg), 180f, 0f);

    }

    private void PLATAFORMA(float ym, float xm, float phim)
    {
        //float aux = y[k];
        //Debug.Log(ym);
        plataforma.transform.localPosition = new Vector3(-ym, 0f, xm);
        plataforma.transform.localEulerAngles = new Vector3(0f, (-phim / 1) * Mathf.Rad2Deg, 0f);

    }
}
