﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
public class INTERFAZ : MonoBehaviour
{

    [SerializeField] private TMP_Text[] R1;
    [SerializeField] private TMP_Text[] R2;
    [SerializeField] private GameObject[] F;
    // Start is called before the first frame update
    void Start()
    {
        for (int i = 0; i < R1.Length; i++)
        {
            R1[i].text = System.Convert.ToString(1f);
            R2[i].text = System.Convert.ToString(0);

        }

        for (int i = 0; i < F.Length; i++)
        {
            F[i].SetActive(false);
        }
    }

    // Update is called once per frame
    void Update()
    {
        this.VENTANAS();
        Setting.setting._f1 = F[0].activeSelf;
        Setting.setting._f2 = F[1].activeSelf;


        R1[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._Q11, 2));
        R1[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._q21, 2));
        R1[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._q31, 2));
        R1[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._phi1, 2));
        R1[4].text = System.Convert.ToString(System.Math.Round(Setting.setting._u1, 2));
        R1[5].text = System.Convert.ToString(System.Math.Round(Setting.setting._w1, 2));

        R2[0].text = System.Convert.ToString(System.Math.Round(Setting.setting._q12, 2));
        R2[1].text = System.Convert.ToString(System.Math.Round(Setting.setting._q22, 2));
        R2[2].text = System.Convert.ToString(System.Math.Round(Setting.setting._q32, 2));
        R2[3].text = System.Convert.ToString(System.Math.Round(Setting.setting._phi2, 2));
        R2[4].text = System.Convert.ToString(System.Math.Round(Setting.setting._u2, 2));
        R2[5].text = System.Convert.ToString(System.Math.Round(Setting.setting._w2, 2));


    }


    private void VENTANAS()
    {
        if (Input.GetKeyDown(KeyCode.F1))
        {
            F[0].SetActive(true);
            F[1].SetActive(false);

        }

        if (Input.GetKeyDown(KeyCode.F2))
        {

            F[0].SetActive(false);
            F[1].SetActive(true);
        }

        if (Input.GetKeyDown(KeyCode.F3))
        {
            F[0].SetActive(false);
            F[1].SetActive(false);

        }
    }
}
