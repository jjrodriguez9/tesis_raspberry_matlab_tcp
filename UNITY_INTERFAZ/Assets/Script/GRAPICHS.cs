﻿using UnityEngine;
using ChartAndGraph;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.InteropServices;

public class GRAPICHS : MonoBehaviour
{
    public GraphChart[] Graph;
    float lastTime = 0f;
    private float X = 0f;
    private float ts = 0.1f;
    // Start is called before the first frame update
    void Start()
    {
        //Graph[0].DataSource.VerticalViewSize=1f;
        Graph[0].DataSource.StartBatch();
        Graph[0].DataSource.ClearCategory("hxe");
        Graph[0].DataSource.ClearCategory("hye");
        Graph[0].DataSource.ClearCategory("hze");
        Graph[0].DataSource.EndBatch();

        Graph[1].DataSource.StartBatch();
        Graph[1].DataSource.ClearCategory("Distance");
        Graph[1].DataSource.ClearCategory("Alpha");
        Graph[1].DataSource.ClearCategory("Betha");
        Graph[1].DataSource.EndBatch();

    }

    void Update()
    {

        float time = Time.time;
        if (lastTime + ts < time)
        {
            lastTime = time;
            // if (Setting.setting._f2)
            //  {
            Graph[0].DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, Setting.setting._hxe, 1);
            Graph[0].DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, Setting.setting._hye, 1);
            Graph[0].DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, Setting.setting._hze, 1);

            Graph[1].DataSource.AddPointToCategoryRealtime("Distance", System.DateTime.Now, Setting.setting._de, 1);
            Graph[1].DataSource.AddPointToCategoryRealtime("Alpha", System.DateTime.Now, Setting.setting._alphae, 1);
            Graph[1].DataSource.AddPointToCategoryRealtime("Betha", System.DateTime.Now, Setting.setting._betae, 1);
            // }



            X++;
        }
    }
}

