﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Setting : MonoBehaviour
{
    public static Setting setting;

    private float q11, q21, q31, phi1, u1, w1, q12, q22, q32, phi2, u2, w2;
    private bool f1, f2;

    private float hxe, hye, hze, de, alphae, betae, distance;

    public float _distance { set { this.distance = value; } get { return distance; } }
    public float _Q11 { set { this.q11 = value; } get { return q11; } }
    public float _q21 { set { this.q21 = value; } get { return q21; } }
    public float _q31 { set { this.q31 = value; } get { return q31; } }
    public float _phi1 { set { this.phi1 = value; } get { return phi1; } }
    public float _u1 { set { this.u1 = value; } get { return u1; } }
    public float _w1 { set { this.w1 = value; } get { return w1; } }



    public float _q12 { set { this.q12 = value; } get { return q12; } }
    public float _q22 { set { this.q22 = value; } get { return q22; } }
    public float _q32 { set { this.q32 = value; } get { return q32; } }
    public float _phi2 { set { this.phi2 = value; } get { return phi2; } }
    public float _u2 { set { this.u2 = value; } get { return u2; } }
    public float _w2 { set { this.w2 = value; } get { return w2; } }

    public float _hxe { set { this.hxe = value; } get { return hxe; } }
    public float _hye { set { this.hye = value; } get { return hye; } }
    public float _hze { set { this.hze = value; } get { return hze; } }

    public float _de { set { this.de = value; } get { return de; } }
    public float _alphae { set { this.alphae = value; } get { return alphae; } }
    public float _betae { set { this.betae = value; } get { return betae; } }

    public bool _f1 { set { this.f1 = value; } get { return f1; } }
    public bool _f2 { set { this.f2 = value; } get { return f2; } }

    private void Awake()
    {
        if (Setting.setting == null) //verifica q el objeeto sea igual a nulo
        {
            Setting.setting = this; // se asigna el objeto setting y this apunt a el mismo
        }
        else
        {
            if (Setting.setting != this) // si setting es difeereente de si mismo destruyela 
            {
                Destroy(this.gameObject);
            }
        }
        DontDestroyOnLoad(this.gameObject);
        //Debug.Log(containerVolume);
    }
}
