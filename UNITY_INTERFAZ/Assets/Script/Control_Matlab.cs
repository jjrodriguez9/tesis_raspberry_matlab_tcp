﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using MOL;
using TMPro;
using UnityEngine.SceneManagement;
using System;
using System.Text;
using System.Runtime.InteropServices;

public class Control_Matlab : MonoBehaviour
{
    [SerializeField]
    private float To;
    private float Time_sample;
    private float t_fin;
    [SerializeField]
    public static int k;
    // GameObject Brazo
    [SerializeField]
    private GameObject q1_eslabon;
    [SerializeField]
    private GameObject q2_eslabon;
    [SerializeField]
    private GameObject q3_eslabon;
    [SerializeField]
    private GameObject q4_eslabon;
    [SerializeField]
    private GameObject plataforma;

    //Gameobject grafl del efector final
    [SerializeField]
    private GameObject graf_efector_final;
    //Gameobject grafl del efector final
    [SerializeField]
    private GameObject graf_trayectoria_deseada;

    //Configuracion interna del manipulador
    float l1, l2, l3, l4, a, h;

    // Posiones del brazo
    public static float[] q1 = new float[9999];
    public static float[] q2 = new float[9999];
    public static float[] q3 = new float[9999];
    public static float[] q4 = new float[9999];

    // Posiciones de la plataforma
    private float[] x = new float[9999];
    private float[] y = new float[9999];
    private float[] th = new float[9999];

    // velocidades de la plataforma
    private float[] x_p= new float[9999];
    private float[] y_p = new float[9999];
    private float[] th_p = new float[9999];

    // Posiciones del Efector Final
    public static float[] hx = new float[9999];
    public static float[] hy = new float[9999];
    public static float[] hz = new float[9999];

    //Trayectoria
    public static float[] hxd = new float[9999];
    private float[] hyd = new float[9999];
    private float[] hzd = new float[9999];

    private float[] hxd_p = new float[9999];
    private float[] hyd_p = new float[9999];
    private float[] hzd_p = new float[9999];

    //Errores
    public static float[] hxe = new float[9999];
    public static float[] hye = new float[9999];
    public static float[] hze = new float[9999];
    private double[,] error = new double[3, 1];

    // Jacobiana
    double j11, j12, j13, j14, j15, j16, j21, j22, j23, j24, j25, j26, j31, j32, j33, j34, j35, j36;

    //Acciones de control
    private float[] u = new float[9999];
    private float[] w = new float[9999];
    private float[] q1_p = new float[9999];
    private float[] q2_p = new float[9999];
    private float[] q3_p = new float[9999];
    private float[] q4_p = new float[9999];

    // Objetivos secundarios
    private float q1dm;
    private float q2dm;
    private float q3dm;
    private float q4dm;

    public float  q1_prue;
    public float q2_prue;
    public float q3_prue;
    public float q4_prue;

    const string dllPath = "smClient64.dll";
    const string Mem_1 = "MemoriaONE";
    const string Mem_2 = "MemoriaTWO"; 
    const string Mem_3 = "ENABLE";

    [DllImport(dllPath)] // For 64 Bits System
    static extern int openMemory(String name, int type);

    [DllImport(dllPath)]
    static extern void setFloat(String memName, int position, float value);

    [DllImport(dllPath)]
    static extern void setInt(String memName, int position, int value);

    [DllImport(dllPath)]
    static extern float getFloat(String memName, int position);

    // Start is called before the first frame update
    void Start()
    {
        openMemory(Mem_1, 2);
        openMemory(Mem_2, 2);
        openMemory(Mem_3, 1);
        


        k = 0;
        t_fin = 1000;
        Time_sample = 0.1f;

        a = 0.175f;
        h = 0.375f;
        //l1 = 0f;
        l2 = 0.275f;
        l3 = 0.275f;
        l4 = 0.15f;

        // Condiciones iniciakes de la plataforma

        x[0] = 0f;
        y[0] = 0f;
        th[0] = (0f * Mathf.PI) / 180f;
        //Condiciones Iniciales del brazo
        q1[0] = (0 * Mathf.PI) / 180f;
        q2[0] = (90 * Mathf.PI) / 180f;
        q3[0] = (-30 * Mathf.PI) / 180f;
        q4[0] = (-30 * Mathf.PI) / 180f;

        hx[0] = x[0] + a * Mathf.Cos(th[0]) + Mathf.Cos(q1[0] + th[0]) * (l2 * Mathf.Cos(q2[0]) + l3 * Mathf.Cos(q2[0] + q3[0]) + l4 * Mathf.Cos(q2[0] + q3[0] + q4[0]));
        hy[0] = y[0] + a * Mathf.Sin(th[0]) + Mathf.Sin(q1[0] + th[0]) * (l2 * Mathf.Cos(q2[0]) + l3 * Mathf.Cos(q2[0] + q3[0]) + l4 * Mathf.Cos(q2[0] + q3[0] + q4[0]));
        hz[0] = h + l2 * Mathf.Sin(q2[0]) + l3 * Mathf.Sin(q2[0] + q3[0]) + l4 * Mathf.Sin(q2[0] + q3[0] + q4[0]);

        graf_trayectoria_deseada.SetActive(false); // para qeu no se grafique el primer movimietno
        graf_efector_final.SetActive(false); // para qeu no se grafique el primer movimietno

        mov_manpipulador();
        Mov_plataforma();
        //Grafico__trayectoria_efector_final();
        //Grafico__trayectoria_deseada();


        setInt(Mem_3, 0, 1);
    }

    // Update is called once per frame
    void Update()
    {
        To += Time.deltaTime;
        if (To >= Time_sample) {

            hxd[k] = getFloat(Mem_1, 7);
            hyd[k] = getFloat(Mem_1, 8);
            hzd[k] = getFloat(Mem_1, 9);

            x[k+1] = getFloat(Mem_1, 0);
            y[k+1] = getFloat(Mem_1, 1);
            th[k+1] = getFloat(Mem_1, 2);

            q1[k+1] = getFloat(Mem_1, 3);
            q2[k+1] = getFloat(Mem_1, 4);
            q3[k+1] = getFloat(Mem_1, 5);
            q4[k+1] = getFloat(Mem_1, 6);


            Cinematica_Directa();
            //Debug.Log(hx[k]);
            setFloat(Mem_2, 0, hx[k]);
            setFloat(Mem_2, 1, hy[k]);
            setFloat(Mem_2, 2, hz[k]);

            //Errores
            hxe[k] = hx[k] - hxd[k];
            hye[k] = hy[k] - hyd[k];
            hze[k] = hz[k] - hzd[k];




            Mov_plataforma();
            mov_manpipulador();
            Grafico__trayectoria_efector_final();
            Grafico__trayectoria_deseada();

            k += 1;
            To = 0;   
        }

    }


    private void Cinematica_Directa()
    {
        //Cinematica Directa
        hx[k + 1] = x[k + 1] + a * Mathf.Cos(th[k + 1]) + Mathf.Cos(q1[k + 1] + th[k + 1]) * (l2 * Mathf.Cos(q2[k + 1]) + l3 * Mathf.Cos(q2[k + 1] + q3[k + 1]) + l4 * Mathf.Cos(q2[k + 1] + q3[k + 1] + q4[k + 1]));
        hy[k + 1] = y[k + 1] + a * Mathf.Sin(th[k + 1]) + Mathf.Sin(q1[k + 1] + th[k + 1]) * (l2 * Mathf.Cos(q2[k + 1]) + l3 * Mathf.Cos(q2[k + 1] + q3[k + 1]) + l4 * Mathf.Cos(q2[k + 1] + q3[k + 1] + q4[k + 1]));
        hz[k + 1] = h + l2 * Mathf.Sin(q2[k + 1]) + l3 * Mathf.Sin(q2[k] + q3[k + 1]) + l4 * Mathf.Sin(q2[k + 1] + q3[k + 1] + q4[k + 1]);

    }


    private void mov_manpipulador()
    {
        q1_eslabon.transform.localEulerAngles = new Vector3(0f, ((-q1[k] / 1) * Mathf.Rad2Deg), 0f); // Sentido por ley de la mano derecha
        q2_eslabon.transform.localEulerAngles = new Vector3(((q2[k] / 1) * Mathf.Rad2Deg), 0f, 0f);
        q3_eslabon.transform.localEulerAngles = new Vector3(((q3[k] / 1) * Mathf.Rad2Deg), 0f, 90f);
        q4_eslabon.transform.localEulerAngles = new Vector3(((q4[k] / 1) * Mathf.Rad2Deg), 180f, 0f);

    }

    private void Mov_plataforma()
    {
        plataforma.transform.localPosition = new Vector3(-y[k], 0f, x[k]);
        plataforma.transform.localEulerAngles = new Vector3(0f, (-th[k] / 1) * Mathf.Rad2Deg, 0f);

    }

    private void Grafico__trayectoria_efector_final()
    {
        graf_efector_final.transform.position = new Vector3(-hy[k], hz[k], hx[k]); // Sentido por ley de la mano derecha
        if (!graf_efector_final.activeInHierarchy)
        {
            graf_efector_final.SetActive(true);
        }
    }

    private void Grafico__trayectoria_deseada()
    {
        graf_trayectoria_deseada.transform.position = new Vector3(-hyd[k], hzd[k], hxd[k]); // Sentido por ley de la mano derecha
        if (!graf_trayectoria_deseada.activeInHierarchy)
        {
            graf_trayectoria_deseada.SetActive(true);
        }
    }

    float Unity_HyperbolicTangent(float In)
    {
        float Out = (Mathf.Exp(2 * In) - 1)/ (Mathf.Exp(2 * In) + 1);
        return Out;       
    }

    void OnApplicationQuit()
    {

        setInt(Mem_3, 0, 0);

    }


}
