#define Graph_And_Chart_PRO
using UnityEngine;
using System.Collections;
using ChartAndGraph;

public class StreamingGraph : MonoBehaviour
{
    public GraphChart Graph;
    public int TotalPoints = 5;
    float lastTime = 0f;
    float lastX = 0f;

    void Start()
    {
        if (Graph == null) // the ChartGraph info is obtained via the inspector
            return;
        
        Graph.DataSource.StartBatch(); // calling StartBatch allows changing the graph data without redrawing the graph for every change
        Graph.DataSource.ClearCategory("hxe"); // clear the "Player 1" category. this category is defined using the GraphChart inspector
        Graph.DataSource.ClearCategory("hye"); // clear the "Player 2" category. this category is defined using the GraphChart inspector
        Graph.DataSource.ClearCategory("hze"); // clear the "Player 2" category. this category is defined using the GraphChart inspector

        Graph.DataSource.AddPointToCategory("hxe", System.DateTime.Now, 0); // each time we call AddPointToCategory 
        Graph.DataSource.AddPointToCategory("hye", System.DateTime.Now, 0); // each time we call AddPointToCategory 
        Graph.DataSource.AddPointToCategory("hze", System.DateTime.Now, 0); // each time we call AddPointToCategory 

        Graph.DataSource.EndBatch(); // finally we call EndBatch , this will cause the GraphChart to redraw itself
    }

    void Update()
    {
        float time = Time.time;
        if (lastTime + 0.1f < time) // Tiempo de ejecuciond elas graficas
        {
            lastTime = time;

            ////Graph.DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, 0.1*Random.Range(-1.0f, 1.0f)); // each time we call AddPointToCategory 
            //Graph.DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, 2*Random.Range(-1.0f, 1.0f)); // each time we call AddPointToCategory
            //Graph.DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, 0.05*Random.Range(-1.0f, 1.0f)); // each time we call AddPointToCategory

            Graph.DataSource.AddPointToCategoryRealtime("hxe", System.DateTime.Now, Control_Trayectoria.hxe[Control_Trayectoria.k - 1]); // each time we call AddPointToCategory 
            Graph.DataSource.AddPointToCategoryRealtime("hye", System.DateTime.Now, Control_Trayectoria.hye[Control_Trayectoria.k - 1]); // each time we call AddPointToCategory
            Graph.DataSource.AddPointToCategoryRealtime("hze", System.DateTime.Now, Control_Trayectoria.hze[Control_Trayectoria.k - 1]); // each time we call AddPointToCategory
        }

    }
}
